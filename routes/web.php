<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



//Send Test Email to check the email functions.  From email has to be from the same domain.
Route::get('/teapot', function(){ Mail::raw('SAVANNAH clicked on it!!!', function($message) {
    $message->to('bryanmcree@outsourcelogistics.com');
    $message->CC('davidalscher@outsourcelogistics.com');
    $message->to('chrisbryant@outsourcelogistics.com');
    $message->from('bryanmcree@outsourcelogistics.com');
});
    return view('savannah');
});


//Route::get('/teapot', function () {
//    return view('savannah');
//});

Auth::routes(['verify' => true]);

//Send Test Email to check the email functions.  From email has to be from the same domain.
Route::get('test_email', function(){ Mail::raw('This is a test email 1 ...2', function($message) {
    $message->to('davidalscher@outsourcelogistics.com');
    $message->from('davidalscher@outsourcelogistics.com');
});
return "test sent to davidalscher@outsourcelogistics.com";
});


//Home
Route::get('/test', function () {
    return view('test');
});

//Test
Route::get('/home', 'HomeController@index')->name('home');

//roles
Route::get('/roles', 'RolesController@index')->name('roles');
Route::post('/roles/add', 'RolesController@addRole')->name('add_role');

//users
Route::get('/users', 'UsersController@index')->name('roles');
Route::GET('/security/{id}', 'UsersController@user_security');
Route::POST('/users/security/add', 'UsersController@add_roles')->name('add_roles');
Route::POST('/users/security/remove', 'UsersController@remove_roles')->name('remove_roles');
Route::GET('/users/login_as/{id}','UsersController@login_as');
Route::GET('/users/edit/{id}','UsersController@edit');
Route::GET('/users/delete/{id}','UsersController@destroy'); //Delete users
Route::GET('/users/editaccount/{id}','UsersController@editaccount'); //Users edit account
Route::POST('/user/update', 'UsersController@update')->name('user_update');
Route::POST('/user/editaccount/update', 'UsersController@updateaccount')->name('user_update_account');
Route::GET('/users/id/{id}','UsersController@id'); //view/print id
Route::POST('/user/upload/image', 'UsersController@add_image')->name('user_update_account');
Route::GET('/users/id/visitor/badge','UsersController@visitor_id'); //view/print id
Route::GET('/users/id/contractor/badge','UsersController@contractor_id'); //view/print id
Route::POST('/user/add', 'UsersController@ManualAdd')->name('user_manual_add');
Route::GET('/users/id2/{id}','UsersController@id2'); //view/print id


//Client
Route::get('/clients', 'ClientsController@index')->name('clients');
Route::get('/clients/{id}', 'ClientsController@show')->name('client_detail');

//Contracts
Route::POST('/clients/contract/add', 'ContractsController@addContract')->name('contract_add');

//PTO
Route::get('/pto', 'PTOController@index')->name('pto');
Route::post('/pto/add', 'PTOController@store')->name('add_pto');
Route::get('/pto/delete/{id}', 'PTOController@destroy')->name('pto_delete');

//Expense Reports
Route::get('/expense', 'ExpenseController@index')->name('expense');
Route::post('/expense/addExpense', 'ExpenseController@create_expense')->name('add_expense');
Route::get('/expense/{id}', 'ExpenseController@detail')->name('expense_detail');
Route::post('/expense/addItem', 'ExpenseController@create_item')->name('add_item');
Route::get('/expense/print/{id}', 'ExpenseController@print_expense_report')->name('expense_print_report');
Route::get('/expense/delete/{id}', 'ExpenseController@deleteExpense')->name('expense_delete');
Route::get('/expense/complete/{id}', 'ExpenseController@markComplete')->name('expense_complete');
Route::get('/expense/delete/item/{id}', 'ExpenseController@deleteItem')->name('expense_delete_item');

//Dashboards
Route::get('/dashboard/osl', 'DashboardController@osl')->name('osl');
Route::get('/dashboard/error', 'DashboardController@error')->name('error');

//Applicant
Route::get('/applicant', 'ApplicantController@index')->name('applicant');
Route::get('/applicant/register', 'ApplicantController@register')->name('applicant_register');
Route::post('/applicant/register/add', 'ApplicantController@newRegistration')->name('applicant_add');

//Containers
Route::get('/containers', 'ContainerController@index')->name('containers');
Route::get('/containers/noCamelot', 'ContainerController@notInCamelot')->name('containers_not_in_Camelot');
Route::get('/containers/dupes', 'ContainerController@viewDuplicates')->name('containers_dupes');
Route::get('/containers/all', 'ContainerController@allContainers')->name('containers');
Route::get('/containers/add', 'ContainerController@addContainer')->name('containers_add');
Route::post('/containers/store', 'ContainerController@store')->name('add_item');
Route::get('/containers/on_vessels', 'ContainerController@on_vessels')->name('containers_on_vessels');
Route::get('/containers/{id}', 'ContainerController@viewContainer')->name('containers_view');
Route::post('/containers/search', 'ContainerController@searchContainers')->name('containers_search');
Route::post('/containers/update', 'ContainerController@update')->name('update_item');
Route::post('/containers/notes/add', 'ContainerController@notesAdd')->name('add_note');
Route::post('/containers/flag', 'ContainerController@flagReview')->name('flag_review');
Route::get('/containers/delete/{id}', 'ContainerController@deleteContainer')->name('containers_delete');
Route::post('/containers/bulk/add', 'ContainerController@bulk_add')->name('bulk_add');

//Portal
Route::get('/portal/ship/{id}', 'PortalController@ship')->name('portal_ship');
Route::POST('/portal/ship/new', 'PortalController@new_ship')->name('portal_new_ship');
Route::get('/portal/csv', 'PortalController@downloadCSV')->name('portal_csv');

//History
Route::get('/history', 'HistoryController@index')->name('history_index');

//Bins
Route::get('/bins', 'BinTransfersController@index')->name('bin_index');
Route::POST('/bins/inventory/bin', 'BinTransfersController@InventorybinScan')->name('bin_inventory_new');
Route::get('/bins/inventory', 'BinTransfersController@inventory')->name('bin_inventory');
Route::POST('/bins/inventory/lp', 'BinTransfersController@lpInventory')->name('lp_inv_new');
Route::get('/bins/inventory/{id}', 'BinTransfersController@viewBinInventory')->name('bin_scan_view');
Route::get('/bins/scan', 'BinTransfersController@scan')->name('bin_scan');
Route::get('/bins/dashboard', 'BinTransfersController@dashboard')->name('bin_dashboard');
Route::get('/bins/dashboard/complete', 'BinTransfersController@dashboardCompleted')->name('bin_dashboard_complete');
Route::POST('/bins/scan/bin', 'BinTransfersController@binScan')->name('bin_scan_new');
Route::get('/bins/{id}', 'BinTransfersController@viewBin')->name('bin_scan_view');
Route::POST('/bins/scan/lp', 'BinTransfersController@lpScan')->name('lp_scan_new');
Route::get('/bins/delete/{id}', 'BinTransfersController@destroy')->name('bin_scan_delete');
Route::get('/bins/bins/complete/{id}', 'BinTransfersController@markComplete')->name('bin_scan_complete');
Route::get('/bins/bins/pending/{id}', 'BinTransfersController@markPending')->name('bin_scan_pending');

//Barcode
Route::get('/barcode', 'BarcodeController@index')->name('barcode_index');
Route::get('/barcode/bigsingle', 'BarcodeController@bigsingle')->name('barcode_bigsingle');
Route::POST('/barcode/print2', 'BarcodeController@print2')->name('barcode_print2');
Route::get('/barcode/dualcodes', 'BarcodeController@dualcode')->name('barcode_dualcodes');
Route::POST('/barcode/print', 'BarcodeController@print')->name('barcode_print');

//Timecard
Route::get('/timeclock/admin', 'TimecardController@admin')->name('timeclock-admin')->middleware(['auth','verified']);
Route::get('/timeclock', 'TimecardController@timeclock')->name('timeclock');
Route::POST('/timeclock/punch', 'TimecardController@punch')->name('timeclock_in');
Route::POST('/timeclock/login', 'TimecardController@timeclock_login')->name('timeclock_login');
Route::get('/timeclock/users', 'TimecardController@users')->name('timeclock_users')->middleware(['auth','verified']);
Route::POST('/timeclock/newuser', 'TimecardController@newuser')->name('timeclock_newuser')->middleware(['auth','verified']);
Route::POST('/timeclock/newpunch', 'TimecardController@newpunch')->name('timeclock_newuser');
Route::get('/timeclock/dashboard/feed','TimecardController@dashboard_feed'); //JsonDump
Route::POST('/timeclock/update_punch', 'TimecardController@edit_punch')->name('timeclock_newuser');

//Yard
Route::get('/yard/switch/complete/{id}', 'YardController@CompleteAssignment')->name('switch_complete');
Route::get('/yard/switch/assign/{id}', 'YardController@RequestAssignment')->name('switch_assign');
Route::get('/yard/switch/abandon/{id}', 'YardController@AbandonAssignment')->name('switch_abandon');
Route::get('/yard', 'YardController@test')->name('yard_test');
Route::get('/yard/feed','YardController@yard_feed'); //JsonDump
Route::get('/dock/feed','YardController@dock_feed'); //JsonDump
Route::get('/yard/detail/{id}','YardController@yard_detail'); //JsonDump
Route::POST('/yard/ingate', 'YardController@ingate')->name('yard-ingate');
Route::POST('/yard/outgate', 'YardController@outgate')->name('yard-outgate');
Route::POST('/yard/search', 'YardController@SearchYard')->name('yard-SearchYard');
Route::POST('/yard/unloaded', 'YardController@unloaded')->name('yard-unloaded');
Route::get('/yard/switch', 'YardController@switching')->name('yard_test');
Route::get('/yard/switch/feed', 'YardController@switchFeed')->name('switch_feed');

//Daily Charges
Route::get('/dailycharges', 'DailyChargesController@index')->name('daily_index');

//Power Bill
Route::get('/powerbill', 'PowerBillController@index')->name('powerbille_index');
Route::POST('/powerbill/add', 'PowerBillController@add')->name('powerbille_add');
Route::GET('/powerbill/delete/{id}','PowerBillController@destroy'); //Delete bill

//Gate Controls
Route::get('/gate/drivergatedel1', 'GateControlController@DriveGateDelete1')->name('delete1-url');
Route::get('/gate', 'GateControlController@index')->name('gatecontrols-index');
Route::POST('/gate/driver', 'GateControlController@DriveGate')->name('gatecontrol-driver');
Route::get('/gate/drivergate2', 'GateControlController@DriveGateURL')->name('gatecontrols-url');

//Shipping Labels
Route::get('/ship', 'ShippingLablesController@index')->name('shipping_index');
Route::post('/ship/print', 'ShippingLablesController@printLabel')->name('powerbille_add');

//Asset Control
Route::post('/asset/new', 'AssetsController@store')->name('asset_store');
Route::get('/asset', 'AssetsController@index')->name('asset_index');
Route::get('/asset/label/{id}', 'AssetsController@label')->name('asset_label');
Route::get('/asset/add', 'AssetsController@create')->name('asset_create');
Route::get('/asset/{id}', 'AssetsController@show')->name('asset_show');
Route::get('/asset/edit/{id}', 'AssetsController@edit')->name('asset_edit');
Route::post('/asset/update', 'AssetsController@update')->name('asset_update');

//Additional Asset Control routes for App
Route::get('/asset/api/all', 'APIController@index')->name('asset_index_app'); //index
Route::post('/asset/api', 'AppController@createAsset')->name('asset_add');  // add asset
Route::get('/asset/api/{id}', 'AppController@updateAsset')->name('asset_update_app');  // update asset
Route::post('/asset/api/{id}', 'AppController@deleteAsset')->name('asset_destroy'); //delete asset

//Logs
Route::get('/logs', 'LogController@index')->name('logs');

//Firebase Routes
Route::get('/firebase', 'FirebaseController@index')->name('logs');

//Bin Creator
Route::get('/bincreator', 'BinCreatorController@index')->name('bin_creator');
Route::post('/bincreator/post', 'BinCreatorController@createBins')->name('asset_add');  // add asset
Route::post('/bincreator/3x2', 'BinCreatorController@printLabels')->name('bin_printLabels');

//Rack Bin Creator
Route::get('/bincreator/rack','BinCreatorController@rackIndex')->name('rack_bin_creator');
Route::post('/bincreator/rackpost','BinCreatorController@rackCreateBins')->name('rack_asset_add');
Route::post('/bincreator/rack3x2','BinCreatorController@rackPrintLabels')->name('rack_printLabels');

//Custom Bin Creator
Route::get('/bincreator/custom','BinCreatorController@customIndex')->name('custom_bin_creator');
Route::post('/bincreator/custompost','BinCreatorController@customCreateBins')->name('custom_asset_add');
Route::post('/bincreator/custom4x6','BinCreatorController@customPrintLabels')->name('custom_printLabels');
