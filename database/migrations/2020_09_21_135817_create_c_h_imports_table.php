<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCHImportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('c_h_imports', function (Blueprint $table) {
            $table->id();
            $table->string('status')->nullable();
            $table->string('load_no')->nullable(); //load was a protected name
            $table->string('invoice')->nullable();
            $table->string('pro')->nullable();
            $table->string('trailer')->nullable();
            $table->timestamp('ship_date')->nullable();
            $table->timestamp('est_payment_date')->nullable();
            $table->decimal('freight_rate', 8, 2)->nullable();
            $table->decimal('advanced', 8, 2)->nullable();
            $table->decimal('adjustments', 8, 2)->nullable();
            $table->decimal('balance', 8, 2)->nullable();
            $table->decimal('total_paid', 8, 2)->nullable();
            $table->string('currency')->nullable();
            $table->timestamp('check_release_date')->nullable();
            $table->timestamp('due_date')->nullable();
            $table->string('check_no')->nullable();
            $table->string('payer')->nullable();
            $table->string('t_code')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('c_h_imports');
    }
}
