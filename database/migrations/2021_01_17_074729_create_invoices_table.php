<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->id();

            $table->date('received_date')->nullable(); //Date we received the invoice
            $table->date('bill_date')->nullable(); //Date on the invoice
            $table->date('due_date')->nullable();  //date invoice is due
            $table->date('service_date')->nullable();  //date where the service was provided or goods received
            $table->string('net_days')->nullable();  // net days on invoice if no date is listed
            $table->string('warehouse')->nullable();  // warehouse group assigned to
            $table->string('assigned_manager')->nullable();  // manager who needs to review and approve
            $table->string('qb_gl_code')->nullable();  // the quickbooks gl code this transaction needs to be assigned to
            $table->string('invoice_status')->nullable(); //status of invoice, pending approval, approved ect.
            $table->string('invoice_notes')->nullable();  // any additional information for this invoice
            $table->string('file_name')->nullable();  //uploaded file name with ext
            $table->string('extension')->nullable(); //uploaded file name just EXT
            $table->string('old_filename')->nullable(); //Old uploaded file name
            $table->string('new_filename')->nullable(); //new file name after upload
            $table->string('created_by')->nullable();  // user that created the invoice
            $table->string('approved_by')->nullable();  // what user approved invoice

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
