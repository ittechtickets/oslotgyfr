<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToTimecardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('timecards', function (Blueprint $table) {
            $table->string('long_out')->nullable();
            $table->string('lat_out')->nullable();
            $table->string('exception')->nullable();
            $table->string('exception_hours')->nullable();
            $table->string('exception_by')->nullable();
            $table->string('exception_notes')->nullable();
            $table->dateTime('exception_entered_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('timecards', function (Blueprint $table) {
            $table->dropColumn('long_out');
            $table->dropColumn('lat_out');
            $table->dropColumn('exception');
            $table->dropColumn('exception_hours');
            $table->dropColumn('exception_by');
            $table->dropColumn('exception_notes');
            $table->dropColumn('exception_entered_date');
        });
    }
}
