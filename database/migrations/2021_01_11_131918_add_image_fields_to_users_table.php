<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddImageFieldsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('file_name')->nullable();
            $table->string('extension')->nullable();
            $table->string('old_filename')->nullable();
            $table->string('new_filename')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('file_name');
            $table->dropColumn('extension');
            $table->dropColumn('old_filename');
            $table->dropColumn('new_filename');
        });
    }
}
