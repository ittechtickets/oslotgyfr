<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMoreFieldsToTimecardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('timecards', function (Blueprint $table) {
            $table->string('exception_paid')->nullable();
            $table->string('correction_by')->nullable();
            $table->string('manual_approved_by')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('timecards', function (Blueprint $table) {
            $table->dropColumn('exception_paid');
            $table->dropColumn('correction_by');
            $table->dropColumn('manual_approved_by');
        });
    }
}
