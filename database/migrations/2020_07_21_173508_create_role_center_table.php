<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoleCenterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('role_center', function (Blueprint $table) {
            $table->id();
            $table->integer('AdjustIn')->nullable();
            $table->integer('AdjustQty')->nullable();
            $table->integer('AdjStatusChange')->nullable();
            $table->integer('SipmentsPending')->nullable();
            $table->integer('ShipmentsToday')->nullable();
            $table->integer('ShipmentsOverdue')->nullable();
            $table->integer('ReceiptQty')->nullable();
            $table->integer('ReceiptsToday')->nullable();
            $table->integer('ReceiptsOverdue')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('role_center');
    }
}
