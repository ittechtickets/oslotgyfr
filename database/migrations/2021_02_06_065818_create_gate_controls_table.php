<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGateControlsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gate_controls', function (Blueprint $table) {
            $table->id();

            $table->string('controller_id')->nullable();  // Controller ID
            $table->string('controller_command')->nullable();  // Code sent to controller
            $table->string('created_by')->nullable();  // Command Issued by

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gate_controls');
    }
}
