<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToYardTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('yard', function (Blueprint $table) {
            $table->string('current_id')->nullable();  // id of the CURRENT transaction in the slot
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('yard', function (Blueprint $table) {
            $table->dropColumn('current_id');
        });
    }
}
