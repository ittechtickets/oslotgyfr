<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTimecardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('timecards', function (Blueprint $table) {
            $table->id();

            $table->string('employee_id')->nullable();
            $table->string('supervisor_id')->nullable();
            $table->dateTime('in_time')->nullable();
            $table->dateTime('out_time')->nullable();
            $table->string('notes')->nullable();
            $table->string('punch_type')->nullable();
            $table->string('approved')->nullable();
            $table->string('ip_address')->nullable();
            $table->string('long')->nullable();
            $table->string('lat')->nullable();
            $table->float('total_time')->nullable();
            $table->float('total_min')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('timecards');
    }
}
