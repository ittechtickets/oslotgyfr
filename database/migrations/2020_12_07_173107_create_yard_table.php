<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateYardTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('yard', function (Blueprint $table) {
            $table->id();

            $table->string('location_bld')->nullable();
            $table->string('location_type')->nullable();
            $table->string('location_number')->nullable();
            $table->string('location_status')->nullable();
            $table->string('location_notes')->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('yard');
    }
}
