<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInventoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventories', function (Blueprint $table) {
            $table->id();
            $table->string('item_id')->nullable();
            $table->string('warehouse')->nullable();
            $table->string('unit')->nullable();
            $table->string('unit_1')->nullable();
            $table->string('client')->nullable();
            $table->string('item')->nullable();
            $table->string('orig_document')->nullable();
            $table->decimal('qty_remain', 8, 2);
            $table->decimal('qty_1_remain', 8, 2);
            $table->datetime('last_updated')->nullable();
            $table->string('description')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventories');
    }
}
