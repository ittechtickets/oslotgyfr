<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddOtherFieldsToTimecardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('timecards', function (Blueprint $table) {
            $table->string('miss_punch_by')->nullable();
            $table->string('miss_punch_status')->nullable();  //1 = ignored
            $table->integer('less_lunch_min')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('timecards', function (Blueprint $table) {
            //
        });
    }
}
