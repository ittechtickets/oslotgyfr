<?php

namespace App;

use App\Traits\Observable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Timecard extends Model
{
    use SoftDeletes;
    use Observable;

    public static function logSubject(Model $model): string
    {
        return sprintf( "User [id:%d] %s/%s",
            $model->id, $model->name, $model->email
        );
    }
    protected $guarded = [];
    protected $dates = ['in_time','out_time'];
    public function employee(){
        return $this->hasOne('\App\TimeclockUsers','id','employee_id');
    }
}
