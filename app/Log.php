<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'logs';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'models' => 'array',
    ];

    /**
     * Get the user record associated with this log
     */
    public function user()
    {
        return $this->hasOne(User::class);
    }

    public function person(){
        return $this->hasOne('\App\User','id','user_id');
    }
}
