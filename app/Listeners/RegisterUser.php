<?php

namespace App\Listeners;

use App\History;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Request;
use Illuminate\Auth\Events\Registered;

class RegisterUser
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(Registered $event)
    {
        //
        $history = new History();
        $history->action = 'Register';
        $history->user_id = 'New Account';
        $history->account_type = 'None';
        $history->search_string = 'New account created.';
        $history->user_ip = Request::getClientIp();
        $history->save();


        Mail::send([], [], function ($message)  {
            $message->to('bryanmcree@outsourcelogistics.com')
                //->CC('joloresduncan@outsourcelogistics.com')
                //->CC('maxgowan@outsourcelogistics.com')
                ->subject('New Registered User')
                ->from('no-reply@outsourcelogistics.com', 'New User')
                ->priority(1)
                ->setBody('<h1 style="color:red">A new user has registered</h1>'
                    , 'text/html'); // for HTML rich messages.
        });
    }
}
