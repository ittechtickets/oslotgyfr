<?php

namespace App\Listeners;

use App\History;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request; //Gets the client ID
use Illuminate\Auth\Events\Logout;

class LogoutUser
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(Logout $event)
    {
        //Create history record for login
        $history = new History();
        $history->action = 'Login';
        $history->user_id = Auth::user()->email;
        $history->account_type = Auth::user()->account_type;
        $history->search_string = 'User has logged out.';
        $history->user_ip = request()->ip();
        $history->save();
    }
}
