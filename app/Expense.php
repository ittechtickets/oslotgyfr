<?php

namespace App;

use App\Traits\Observable;
use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
    use Observable;

    public static function logSubject(Model $model): string
    {
        return sprintf( "User [id:%d] %s/%s",
            $model->id, $model->name, $model->email
        );
    }

    protected $table = 'expense';
    protected $guarded = [];

    public function items()
    {
        return $this->hasMany('App\ExpenseItems');
    }

    public function employee(){
        return $this->hasOne('\App\User','id','employee_id');
    }
}
