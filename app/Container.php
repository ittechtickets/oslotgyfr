<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Container extends Model
{
    protected $table = 'containers';
    protected $guarded = [];
    protected $dates = ['estimated_arrival','estimated_lfd','pickup_date','drop_date','empty_date','returned_date',
        'returned_date','scan_date','date_camelot','container_ava','container_fte','container_to_door'];

    public function employee(){
        return $this->hasOne('\App\User','id','created_by');
    }

    public function notesContainer(){
        return $this->hasMany('\App\ContainerNotes','container_id','id')->orderby('created_at','desc');
    }


    public function chImport(){
        return $this->hasMany('\App\CHImport','load_no','ch_number');
    }
}
