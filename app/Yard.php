<?php

namespace App;

use App\Traits\Observable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Yard extends Model
{
    use SoftDeletes;
    use Observable;

    public static function logSubject(Model $model): string
    {
        return sprintf( "User [id:%d] %s/%s",
            $model->id, $model->name, $model->email
        );
    }
    protected $table = 'yard';

    public function employee(){
        return $this->hasOne('\App\User','id','created_by');
    }

    public function YardTrans(){
        return $this->hasOne('\App\YardTransaction','location_id','id');
    }

    public function YardCurrent(){
        return $this->hasOne('\App\YardTransaction','id','current_id');
    }


}
