<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CHImport extends Model
{
    protected $table = 'c_h_imports';
    protected $dates = ['ship_date','est_payment_date','check_release_date','due_date'];
}
