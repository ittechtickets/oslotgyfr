<?php

namespace App\Http\Controllers;

use App\Contracts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Webpatser\Uuid\Uuid;

class ContractsController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth','verified']);
    }

    public function addContract(Request $request) {

        $contract = $request->all();

        $file = $contract['file_name'];
        $extension = $file->getClientOriginalExtension();
        $entry = new Contracts();
        $entry->file_mime = $file->getClientMimeType();
        $entry->old_filename = $file->getClientOriginalName();
        $entry->new_filename = Uuid::generate(4) .'.'.$extension;
        $entry->client_id = $contract['client_id'];
        $entry->file_extension = $extension;
        $entry->file_size = $file->getSize();
        $entry->file_description = 'Contract';
        $entry->signed_date = $contract['signed_date'];
        $entry->renewal_date = $contract['renewal_date'];
        $entry->reminder = $contract['reminder'];

        Storage::disk('local')->put("/Contracts/" .$entry->new_filename,  File::get($file));

        $entry->save();
        alert()->success('File has been uploaded.', 'File Uploaded');
        return redirect('/clients/'.$contract['client_id']);
    }

    public function downloadFile($id){

        $entry = Documents::where('id', '=', $id)->firstOrFail();
        $file = Storage::disk('local')->get($entry->new_filename);
        return (new Response($file, 200))
            ->header('Content-Type', $entry->file_mime)
            ->header('content-disposition', 'attachment; filename="'. $entry->old_filename .'"');
    }

    public function deleteFile($id, $class_id)
    {
        $entry = Documents::where('id', '=', $id)->firstOrFail();
        $file = Storage::disk('local')->get($entry->new_filename);
        File::delete($file);
        return redirect('/classes/'.$class_id);
    }
}
