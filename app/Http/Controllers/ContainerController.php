<?php

namespace App\Http\Controllers;

use App\Container;
use App\ContainerNotes;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;
use App\History;
use Illuminate\Support\Facades\Auth;

class ContainerController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth','verified']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //container dashboard
        $containers = Container::all();

        //Duplicate Containers
        $duplicates = DB::table('containers')
            ->select('container_number', DB::raw('COUNT(*) as `count`'))
            ->groupBy( 'container_number')
            ->havingRaw('COUNT(*) > 1')
            ->get();

        //Containers by customer this week
        $customer = Container::select('customer', DB::raw('COUNT(*) as `count`'))
            ->groupby('customer')
            ->get();

        //CH containers
        $chContainers = Container:://select('customer',DB::raw('COUNT(*) as `count`'))
            leftjoin('c_h_imports','containers.ch_number','=','c_h_imports.load_no')
            ->wherein('customer', ['Kroger MFG','Kroger Seasonal'])
            //->wherenotnull('c_h_imports.load_no')
            //->groupby('customer')
            ->get();

        //Containers w/I 2 days of LFD
        $lfd = Container::where('estimated_lfd', '<=',Carbon::now()->subDays(2)->toDateTimeString())
            ->wherenotnull('returned_date') //If already returned don't count!
            ->get();

        //Not in Camelot
        $noCamelot = Container::wherenull('camelot_number')
            ->wherenotnull('drop_date')
            ->get();

        //dd($lfd);

        $history = new History();
        $history->action = 'View Containers';
        $history->user_id = Auth::user()->email;
        $history->account_type = Auth::user()->account_type;
        $history->search_string = 'User has accessed the container app.';
        $history->user_ip =  request()->ip();
        $history->save();

        return view('containers.index', compact('containers','duplicates','customer','chContainers','lfd',
        'noCamelot'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function addContainer()
    {
        return view('containers.add_container');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $NewContainer = $request->except('add_another');  //collect all other fields
        $NewContainer = Container::create($NewContainer);  //Create the record

        Alert::toast('Container Created!', 'success');

        $history = new History();
        $history->action = 'Added Container';
        $history->user_id = Auth::user()->email;
        $history->account_type = Auth::user()->account_type;
        $history->search_string = 'User has added a new container.';
        $history->user_ip = request()->ip();
        $history->save();

        if($request->add_another == 1)
        {
            return redirect('/containers/add');
        }else{
            return redirect('/containers');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Container  $container
     * @return \Illuminate\Http\Response
     */
    public function viewContainer($id)
    {
        $container = Container::findorfail($id);  //pull container
        //dd($container);
        $history = new History();
        $history->action = 'Viewed Container';
        $history->user_id = Auth::user()->email;
        $history->account_type = Auth::user()->account_type;
        $history->search_string = 'User viewed container '. $container->container_number .'.';
        $history->user_ip = request()->ip();
        $history->save();

        return view('containers.edit_container', compact('container'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Container  $container
     * @return \Illuminate\Http\Response
     */
    public function edit(Container $container)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Container  $container
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $container = Container::findorfail($request->id);
        $container->update($request->all());

        //Create a note when user updates container.
        $note = new ContainerNotes();
        $note->container_id = $container->id;
        $note->created_by = Auth::user()->id;
        $note->container_notes = 'Updated container';
        $note->created_at = Carbon::now();
        $note->save();


        $history = new History();
        $history->action = 'Update Container';
        $history->user_id = Auth::user()->email;
        $history->account_type = Auth::user()->account_type;
        $history->search_string = 'User viewed container '. $container->container_number .'.';
        $history->user_ip = request()->ip();
        $history->save();

        Alert::toast('Container has been updated!', 'success');
        return redirect('/containers/'.$container->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Container  $container
     * @return \Illuminate\Http\Response
     */
    public function destroy(Container $container)
    {
        //
    }

    public function searchContainers(Request $request)
    {
        $containers = Container::where('container_number', 'LIKE', '%' . $request->search_bar . '%')
            ->orWhere('bl_number', 'LIKE', '%' . $request->search_bar . '%')
            ->orWhere('dn_number', 'LIKE', '%' . $request->search_bar . '%')
            ->orWhere('ch_number', 'LIKE', '%' . $request->search_bar . '%')
            ->orWhere('transportation_inv_number', 'LIKE', '%' . $request->search_bar . '%')
            ->orWhere('camelot_number', 'LIKE', '%' . $request->search_bar . '%')
            ->paginate(50);

        $history = new History();
        $history->action = 'Searched Container';
        $history->user_id = Auth::user()->email;
        $history->account_type = Auth::user()->account_type;
        $history->search_string = $request->search_bar;
        $history->user_ip = request()->ip();
        $history->save();

        return view('containers.containers', compact('containers'));

    }

    public function notesAdd (Request $request)
    {
        $NewContainerNote = $request->all();  //collect all other fields
        $NewContainerNote = ContainerNotes::create($NewContainerNote);  //Create the record

        $history = new History();
        $history->action = 'Added Note to Container';
        $history->user_id = Auth::user()->email;
        $history->account_type = Auth::user()->account_type;
        $history->search_string = 'User has added a note to container.';
        $history->user_ip = request()->ip();
        $history->save();

        Alert::toast('Note has been added!', 'success');
        return redirect('/containers/'. $request->container_id);
    }

    public function allContainers ()
    {
        $containers = Container::wherenull('returned_date')->paginate(50);
        return view('containers.containers', compact('containers'));
    }

    public function deleteContainer(Request $request)
    {
        $remove_container = Container::where('id', $request->id)->delete();

        $history = new History();
        $history->action = 'Delete Container';
        $history->user_id = Auth::user()->email;
        $history->account_type = Auth::user()->account_type;
        $history->search_string = 'User has deleted a container.';
        $history->user_ip = request()->ip();
        $history->save();

        Alert::toast('Container Deleted', 'success');
        return redirect('/containers/all');
    }

    public function on_vessels()
    {
        //container dashboard
        $containers = Container::where('on_vessel','Yes')
            ->get();
        //dd($containers);

        $history = new History();
        $history->action = 'Containers on Vessels';
        $history->user_id = Auth::user()->email;
        $history->account_type = Auth::user()->account_type;
        $history->search_string = 'User viewed all containers on vessels.';
        $history->user_ip =  request()->ip();
        $history->save();

        return view('containers.on_vessels', compact('containers'));
    }

    public function bulk_add(Request $request)
    {
        //dd($request->all());
        $container_numbers = preg_replace( "/\r|\n/", "", array_filter(explode("\n", trim($request->container_data)), 'trim'));
        $field_one = preg_replace( "/\r|\n/", "", array_filter(explode("\n", trim($request->field_one_data)), 'trim'));
        $field_two = preg_replace( "/\r|\n/", "", array_filter(explode("\n", trim($request->field_two_data)), 'trim'));
        $field_three = preg_replace( "/\r|\n/", "", array_filter(explode("\n", trim($request->field_three_data)), 'trim'));
        $field_four = preg_replace( "/\r|\n/", "", array_filter(explode("\n", trim($request->field_four_data)), 'trim'));
        //dd($container_numbers);

        $x = -1;
        foreach ($container_numbers as $line) {
            // processing here.
            $x = $x+1;

            $newContainer = new Container();
            $newContainer->customer = $request->bulk_customer;
            $newContainer->container_number = $line;
            $newContainer->on_vessel = $request->bulk_vessels;


            if(!empty($field_one))
            {
                $newContainer->bl_number = $field_one[$x];
            }

            if(!empty($field_two))
            {
                $newContainer->dn_number = $field_two[$x];
            }

            if(!empty($field_three))
            {
                $newContainer->ch_number = $field_three[$x];
            }

            if(!empty($field_four))
            {
                $newContainer->desc_01 = $field_four[$x];
            }

            $newContainer->created_by = $request->created_by;
            $newContainer->save();

        }

        $history = new History();
        $history->action = 'Bulk Container Add';
        $history->user_id = Auth::user()->email;
        $history->account_type = Auth::user()->account_type;
        $history->search_string = 'User added a bulk number of containers.';
        $history->user_ip =  request()->ip();
        $history->save();

        Alert::toast('All Containers Added', 'success');
        return redirect('/containers/all');
    }

    public function viewDuplicates()
    {
        $dups = DB::table('containers')
            ->select('container_number')
            ->groupBy( 'container_number')
            ->havingRaw('COUNT(*) > 1')
            ->get();

        $containers = Container::wherein('container_number', [$dups->container_number])
            ->get();


        return view('containers.duplicates', compact('containers'));
    }

    public function notInCamelot()
    {
        $containers = Container::wherenull('camelot_number')
            ->wherenotnull('drop_date')
            ->get();

        $history = new History();
        $history->action = 'Containers Not in Camelot';
        $history->user_id = Auth::user()->email;
        $history->account_type = Auth::user()->account_type;
        $history->search_string = 'User viewed containers not in Camelot.';
        $history->user_ip =  request()->ip();
        $history->save();

        return view('containers.drill_notInCamelot', compact('containers'));

    }

    public function flagReview(Request $request)
    {

        $NewContainerNote = $request->all();  //collect all other fields
        $NewContainerNote = ContainerNotes::create($NewContainerNote);  //Create the record

        $updateContainer = Container::find($request->container_id);
        $updateContainer -> container_status = 'Flagged';
        $updateContainer->save();

        $history = new History();
        $history->action = 'Flagged for Review';
        $history->user_id = Auth::user()->email;
        $history->account_type = Auth::user()->account_type;
        $history->search_string = 'User flagged container' . $updateContainer->container_number .' for review.';
        $history->user_ip =  request()->ip();
        $history->save();


        Alert::toast('Container has been flagged fo review.', 'success');
        return redirect('/containers/'.$request->container_id);
    }
}
