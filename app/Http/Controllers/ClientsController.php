<?php

namespace App\Http\Controllers;

use App\Clients;
use App\Contracts;
use App\History;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Auth;

class ClientsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $allClients = Clients::all();

        $history = new History();
        $history->action = 'View Clients';
        $history->user_id = Auth::user()->email;
        $history->account_type = Auth::user()->account_type;
        $history->search_string = 'User has accessed the client list.';
        $history->user_ip = request()->ip();
        $history->save();

        return view('clients.index', compact('allClients'));
    }

    public function show($id)
    {
        $client = Clients::findorfail($id);
        $contracts  = Contracts::where('client_id', $id)->get();

        return view('clients.detail', compact('client','contracts'));
    }
}
