<?php

namespace App\Http\Controllers;

use App\BinCreator;
use Illuminate\Http\Request;

class BinCreatorController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('bincreator.index');
    }
    
    public function rackIndex() {
    
        return view('bincreator.rackindex');
    }
    
    public function customIndex() {
        
        return view('bincreator.customindex');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BinCreator  $binCreator
     * @return \Illuminate\Http\Response
     */
    public function show(BinCreator $binCreator)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BinCreator  $binCreator
     * @return \Illuminate\Http\Response
     */
    public function edit(BinCreator $binCreator)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BinCreator  $binCreator
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BinCreator $binCreator)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BinCreator  $binCreator
     * @return \Illuminate\Http\Response
     */
    public function destroy(BinCreator $binCreator)
    {
        //
    }



    public function createBins(Request $request)
    {
         $binData = $request->all();
         $preBin = $binData['building_number'] . $binData['delimiter'] . $binData['aisle_number']. $binData['delimiter'];
         $delimiter = $binData['delimiter'];
         $building_number = $binData['building_number'];
         $aisle_number = $binData['aisle_number'];
         $start_number = $binData['start_number'];
         $end_number = $binData['end_number'];
         $end_number_length = strlen($binData['end_number']);
         return view('bincreator.view',compact('preBin','start_number','end_number','end_number_length','delimiter'
         , 'building_number','aisle_number'));
    }
    
    public function rackCreateBins(Request $request) {
    
        $binData = $request->all();
        $preBin = "R". $binData['delimiter'] . $binData['row'] . $binData['delimiter'];
        $delimiter = $binData['delimiter'];
        $row = $binData['row'];
        $start_location = $binData['start_location'];
        $end_location = $binData['end_location'];
        $sub_location = $binData['sub_location'];
        $end_location_length = strlen($binData['end_location']);
        return view('bincreator.rackview',compact('preBin','start_location','end_location','end_location_length','delimiter','row','sub_location'));
    }
    
    public function customCreateBins(Request $request) {
    
        $binData = $request->all();
        $custom = $binData['custom'];
        return view('bincreator.customview',compact('custom'));
    }

    public function printLabels(Request $request)
    {
        $binData = $request->all();
        $preBin = $binData['building_number'] . $binData['delimiter'] . $binData['aisle_number']. $binData['delimiter'];
        $start_number = $binData['start_number'];
        $end_number = $binData['end_number'];
        $end_number_length = strlen($binData['end_number']);

        if($binData['LabelSize'] =='3x2')
        {
            return view('bincreator.3x2',compact('preBin','start_number','end_number','end_number_length'));
        }elseif($binData['LabelSize'] =='4x6'){
            return view('bincreator.4x6',compact('preBin','start_number','end_number','end_number_length'));
        }
    }
        
    public function rackPrintLabels(Request $request)
    {
        $binData = $request->all();
        $preBin = "R". $binData['delimiter'] . $binData['row'] . $binData['delimiter'];
        $start_location = $binData['start_location'];
        $end_location = $binData['end_location'];
        $sub_location = $binData['sub_location'];
        $delimiter = $binData['delimiter'];
        $end_location_length = strlen($binData['end_location']);
        
        if($binData['RackLabelSize'] == '3x2') {
       
        return view('bincreator.rack3x2',compact('preBin','start_location','end_location','end_location_length','delimiter','sub_location'));
        }
        else {
        return view('bincreator.rackindex');
        }
        
       
    }
    
    public function customPrintLabels(Request $request) {
        
        $binData = $request->all();
        $custom = $binData['custom'];
        
        if($binData['customLabelSize'] == '4x6') {
        
            return view('bincreator.custom4x6',compact('custom'));
        } else {
            return view('bincreator.customindex');
        }
    }
}
