<?php

namespace App\Http\Controllers;

use App\Container;
use App\History;
use App\Timecard;
use App\TimeclockUsers;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use RealRashid\SweetAlert\Facades\Alert;

class TimecardController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Timecard  $timecard
     * @return \Illuminate\Http\Response
     */
    public function show(Timecard $timecard)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Timecard  $timecard
     * @return \Illuminate\Http\Response
     */
    public function edit(Timecard $timecard)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Timecard  $timecard
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Timecard $timecard)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Timecard  $timecard
     * @return \Illuminate\Http\Response
     */
    public function destroy(Timecard $timecard)
    {
        //
    }

    public function timeclock()
    {
        return view('timecard.timeclock');
    }

    public function timeclock_login(Request $request)
    {
        $TimeClock = $request->all();

        //check and see if there is a user with this ID
        $timeclockuser = TimeclockUsers::where('timeclock_id','=', $TimeClock['timecard_id'])
            ->first();

        if(!is_null($timeclockuser))
        {
            //User if FOUND

            //Lets check for entries for today
            $entries = Timecard::wheredate('in_time','=', Carbon::now())
                ->where('employee_id','=', $timeclockuser->id )
                ->orderby('in_time','DESC')
                ->first();

            $weekly_card = Timecard::where('employee_id','=', $timeclockuser->id )
                ->whereBetween('in_time', [
                    Carbon::parse('last monday')->startOfDay(),
                    Carbon::parse('next friday')->endOfDay(),
                ])
                ->orderby('in_time','DESC')
                ->get();

            return view('timecard.timepunch', compact('timeclockuser','entries','weekly_card'));
        }else{
            //ERRRR Send user back to login page
            Session::flash('message', 'Timeclock ID not found!');
            Session::flash('alert-class', 'alert-danger');
            //return view('timecard.timeclock');
            return redirect('/timeclock');
        }


    }

    public function punch(Request $request)
    {
        //dd($request->builiding_location);
        $punchCheck = $request->all();
        //dd($punchCheck);

        //See if there are any punches for today
        $entries = Timecard::wheredate('in_time','=', Carbon::now())
            ->where('employee_id','=', $punchCheck['id'] )
            ->orderby('in_time','DESC')
            ->first();

        //dd($entries);
        if(is_null($entries))
        {
            //No punches for today lets make one.
            $punch = new Timecard();
            $punch->employee_id = $punchCheck['id'];
            $punch->in_time = Carbon::now();
            $punch->building_location = $request->bulding_location;
            $punch->punch_type = 'Web';
            $punch->long = $punchCheck['long'];
            $punch->lat = $punchCheck['lat'];
            $punch->building_location = $punchCheck['bld'];
            $punch->ip_address = request()->ip();
            $punch->save();

            //send the user back so someone else can clock in
            Session::flash('message', 'Your punch has been recorded');
            Session::flash('alert-class', 'alert-success');
            return view('timecard.timeclock');

        //Check if there is an out time in the last punch
        }elseif(is_null($entries->out_time)){

            //Update the LAST entry adding the out time
            $entries->out_time = Carbon::now();
            $entries->total_time = (Carbon::now()->diffInMinutes($entries->in_time, true))/60;
            $entries->total_min = Carbon::now()->diffInMinutes($entries->in_time, true);
            $entries->lat_out = $punchCheck['lat'];
            $entries->long_out = $punchCheck['long'];
            $entries->save();

            //Update the total time in decimals


            //send the user back so someone else can clock in
            Session::flash('message', 'Your punch has been recorded');
            Session::flash('alert-class', 'alert-success');
            //return view('timecard.timeclock');
            return redirect('/timeclock');

        }elseif(!is_null($entries->out_time))
        {
            //No punches for today lets make one.
            $punch = new Timecard();
            $punch->employee_id =  $punchCheck['id'];
            $punch->in_time = Carbon::now();
            $punch->building_location = $request->bulding_location;
            $punch->punch_type = 'Web';
            $punch->long = $punchCheck['long'];
            $punch->lat = $punchCheck['lat'];
            $punch->building_location = $punchCheck['bld'];
            $punch->ip_address = request()->ip();
            $punch->save();

            //send the user back so someone else can clock in
            Session::flash('message', 'Your punch has been recorded');
            Session::flash('alert-class', 'alert-success');
            //return view('timecard.timeclock');
            return redirect('/timeclock');
        }

    }

    public function users()
    {
        $users = TimeclockUsers::all();
        return view('timecard.users', compact('users'));
    }

    public function createuser()
    {
        return view('timecard.createuser');
    }

    public function admin()
    {

        //View feed via dashboard_feed below
        return view('timecard.index');
    }

    public function newuser(Request $request)
    {
        $newuser = $request->all();
        $NewContainer = TimeclockUsers::create($newuser);  //Create the record
        return redirect('/timeclock/users');
    }

    public function newpunch(Request $request)
    {
        $newpunch = $request->all();
        dd($newpunch);
    }

    public function dashboard_feed()
    {
        $clockedin = Timecard::wheredate('in_time','=', Carbon::now())
            ->wherenull('out_time')
            ->get();

        $transactions = Timecard::wheredate('in_time','=', Carbon::now())
            ->orderby('updated_at','desc')
            ->get();

        $mps = Timecard::wheredate('in_time','<', Carbon::now()) //Don't include today
            ->wherenull('out_time')

            ->orWhere(function($query) {  //Where user worked over 8.5 and his time has NOT been adjusted
                $query->where('total_time', '>', 8.5)
                    ->WhereNull('less_lunch_min');
            })

            ->get();

        return view('timecard.dashboardfeed', compact('clockedin','transactions','mps'));
    }

    public function edit_punch(Request $request)
    {

        $update_punch = Timecard::findorfail($request->id);  //Find the record we want to change

        if(is_null($request->less_lunch_min))
        {
            $request->offsetSet('total_time',(Carbon::parse($request->in_time)->diffInMinutes($request->out_time, true))/60);
            $request->offsetSet('total_min',Carbon::parse($request->in_time)->diffInMinutes($request->out_time, true));
        }else{
            //dd($request->less_lunch_min);
            $request->offsetSet('total_time',((Carbon::parse($request->in_time)->diffInMinutes($request->out_time, true))-$request->less_lunch_min)/60);
            $request->offsetSet('total_min',Carbon::parse($request->in_time)->diffInMinutes($request->out_time, true)-$request->less_lunch_min);
        }



        //subtract less time and recalculate

        $update_punch->update($request->all());
        Alert::toast('Punch has been updated!', 'success');
        return redirect('/timeclock/admin');
    }


}
