<?php

namespace App\Http\Controllers;

use App\History;
use Illuminate\Http\Request;
use App\Roles;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class RolesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $allRoles = Roles::all();

        $history = new History();
        $history->action = 'View Roles';
        $history->user_id = Auth::user()->email;
        $history->account_type = Auth::user()->account_type;
        $history->search_string = 'User has viewed roles.';
        $history->user_ip =  request()->ip();
        $history->save();
        return view('roles.index', compact('allRoles'));
    }

    public function addRole(Request $request)
    {
        $role = $request->all();
        Roles::create($role);
        Alert::toast('Role Added', 'success');

        $history = new History();
        $history->action = 'Added Role';
        $history->user_id = Auth::user()->email;
        $history->account_type = Auth::user()->account_type;
        $history->search_string = $request->name;
        $history->user_ip =  request()->ip();
        $history->save();

        return redirect('/roles');
    }

    public function editRoll()
    {

    }
}
