<?php

namespace App\Http\Controllers;

use App\DailyCharges;
use App\History;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DailyChargesController extends Controller
{


    public function __construct()
    {
        $this->middleware(['auth','verified']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transactions = DailyCharges::wherein('user_id',['SAVMANAGER','SAVANNAH2','SAVANNAH5','SAVANNAH7','SAVANNAH8','SAVANNAH6'])
            ->where('Doc Date','=', Carbon::yesterday())
            ->get();
        $no_charges = DailyCharges::where('Charge amount','=<', 0)
            ->wherein('user_id',['SAVMANAGER','SAVANNAH2','SAVANNAH5','SAVANNAH7','SAVANNAH8','SAVANNAH6'])
            ->where('Doc Date','=', Carbon::yesterday())
            ->get();
        $charges = DailyCharges::where('Charge amount','>', 0)
            ->wherein('user_id',['SAVMANAGER','SAVANNAH2','SAVANNAH5','SAVANNAH7','SAVANNAH8','SAVANNAH6'])
            ->where('Doc Date','=', Carbon::yesterday())
            ->get();
        $charges_customer = DailyCharges::select(DB::raw("Client, sum(`Charge amount`) as total_charges"))
            ->where('Charge amount','>',0)
            ->where('Doc Date','=', Carbon::yesterday())
            ->wherein('user_id',['SAVMANAGER','SAVANNAH2','SAVANNAH5','SAVANNAH7','SAVANNAH8','SAVANNAH6'])
            ->groupby('Client')
            ->orderby('total_charges','desc')
            ->get();
        $no_charges_user = DailyCharges::select(DB::raw("user_id, count(*) as total_transactions"))
            ->where('Charge amount','=<',0)
            ->where('Doc Date','=', Carbon::yesterday())
            ->wherein('user_id',['SAVMANAGER','SAVANNAH2','SAVANNAH5','SAVANNAH7','SAVANNAH8','SAVANNAH6'])
            ->groupby('user_id')
            ->orderby('total_transactions','desc')
            ->get();

        $history = new History();
        $history->action = 'Dashboard - Daily Charges';
        $history->user_id = Auth::user()->email;
        $history->account_type = Auth::user()->account_type;
        $history->search_string = 'None';
        $history->user_ip = request()->ip();
        $history->save();


        return view('dailycharges.index',compact('transactions','no_charges','charges','charges_customer',
        'no_charges_user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DailyCharges  $dailyCharges
     * @return \Illuminate\Http\Response
     */
    public function show(DailyCharges $dailyCharges)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DailyCharges  $dailyCharges
     * @return \Illuminate\Http\Response
     */
    public function edit(DailyCharges $dailyCharges)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DailyCharges  $dailyCharges
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DailyCharges $dailyCharges)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DailyCharges  $dailyCharges
     * @return \Illuminate\Http\Response
     */
    public function destroy(DailyCharges $dailyCharges)
    {
        //
    }
}
