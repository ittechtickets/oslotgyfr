<?php

namespace App\Http\Controllers;

use App\YardTransfer;
use Illuminate\Http\Request;

class YardTransferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\YardTransfer  $yardTransfer
     * @return \Illuminate\Http\Response
     */
    public function show(YardTransfer $yardTransfer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\YardTransfer  $yardTransfer
     * @return \Illuminate\Http\Response
     */
    public function edit(YardTransfer $yardTransfer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\YardTransfer  $yardTransfer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, YardTransfer $yardTransfer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\YardTransfer  $yardTransfer
     * @return \Illuminate\Http\Response
     */
    public function destroy(YardTransfer $yardTransfer)
    {
        //
    }
}
