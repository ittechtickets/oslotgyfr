<?php

namespace App\Http\Controllers;

use App\History;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ShippingLablesController extends Controller
{
    public function index()
    {
        return view('label.index');
    }

    public function printLabel(Request $request)
    {
        $fields = $request->all();
        $plt = $request->number_plt;

        $history = new History();
        $history->action = 'Created Shipping Labels';
        $history->user_id = Auth::user()->email;
        $history->account_type = Auth::user()->account_type;
        $history->search_string = 'None';
        $history->user_ip = request()->ip();
        $history->save();

        return view('label.print', compact('fields','plt'));
    }
}
