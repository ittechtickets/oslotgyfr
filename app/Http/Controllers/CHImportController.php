<?php

namespace App\Http\Controllers;

use App\CHImport;
use Illuminate\Http\Request;

class CHImportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CHImport  $cHImport
     * @return \Illuminate\Http\Response
     */
    public function show(CHImport $cHImport)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CHImport  $cHImport
     * @return \Illuminate\Http\Response
     */
    public function edit(CHImport $cHImport)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CHImport  $cHImport
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CHImport $cHImport)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CHImport  $cHImport
     * @return \Illuminate\Http\Response
     */
    public function destroy(CHImport $cHImport)
    {
        //
    }
}
