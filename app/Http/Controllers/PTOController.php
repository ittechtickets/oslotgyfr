<?php

namespace App\Http\Controllers;

use App\PTO;
use App\Roles;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use RealRashid\SweetAlert\Facades\Alert;

class PTOController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth','verified']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allUsers = User::orderby('last_name')->get();
        $ptoRequest = PTO::where('employee_id','=',Auth::user()->id)->get();
        $pendingPTO = PTO::where('supervisor_id','=',Auth::user()->id)->where('request_status',0)->get();

        return view('pto.index', compact('allUsers','ptoRequest','pendingPTO'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pto = $request->all();
        $approve_email = User::where('id','=', $pto['Supervisor_id'])->first();
        $requester_email = User::where('id','=', $pto['employee_id'])->first();
        //dd($requester_email);

        PTO::create($pto);

        //New request has been submitted we need to email the supervisor and send a notification to the requester.


        //Send notification to the supervisor.
        Mail::send([], [], function ($message) use ($approve_email, $pto) {
            $message->to($approve_email['email'])
                ->subject('PTO Request')
                ->from('no-reply@outsourcelogistics.com', 'OL Employee Warehouse')
                ->setBody('<h1>'. $approve_email['first_name'] .', <br> You have a PTO request to review!</h1>
                                ', 'text/html'); // for HTML rich messages.
        });

        //Send Notification to the requester

        Mail::send([], [], function ($message) use ($requester_email, $pto) {
            $message->to($requester_email['email'])
                    ->subject('PTO Request')
                    ->from('no-reply@outsourcelogistics.com', 'OL Employee Warehouse')
                    ->setBody('<h1>'. $requester_email['first_name'] .', <br> Your PTO request has been received!</h1>
                                <br> <b>Start:</b> '. Carbon::parse($pto['start_date'])->format('m-d-y H:i').'
                                <br> <b>End:</b> '. Carbon::parse($pto['end_date'])->format('m-d-y H:i'), 'text/html'); // for HTML rich messages.
            });


        Alert::toast('PTO Request Sent', 'success');
        return redirect('/pto');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PTO  $pTO
     * @return \Illuminate\Http\Response
     */
    public function show(PTO $pTO)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PTO  $pTO
     * @return \Illuminate\Http\Response
     */
    public function edit(PTO $pTO)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PTO  $pTO
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PTO $pTO)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PTO  $pTO
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $remove_item = PTO::where('id', $request->id)->delete();
        Alert::toast('Request Deleted', 'success');
        return redirect('/pto');
    }
}
