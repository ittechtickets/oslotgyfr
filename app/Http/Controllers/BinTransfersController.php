<?php

namespace App\Http\Controllers;

use App\BinTransfers;
use App\History;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class BinTransfersController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth','verified']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allBins = BinTransfers::all();

        return view('bins.index', compact('allBins'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BinTransfers  $binTransfers
     * @return \Illuminate\Http\Response
     */
    public function show(BinTransfers $binTransfers)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BinTransfers  $binTransfers
     * @return \Illuminate\Http\Response
     */
    public function edit(BinTransfers $binTransfers)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BinTransfers  $binTransfers
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BinTransfers $binTransfers)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BinTransfers  $binTransfers
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $removeBin = BinTransfers::where('id', $id)->first();
        $binLocation = $removeBin->bin_location;
        $removeBin->delete();
        Alert::toast('License Plate Deleted', 'success');
        return redirect('/bins/'. $binLocation);
    }

    public function scan()
    {
        return view('bins.scan');
    }

    public function binScan(Request $request)
    {

        $bin = $request->bin_location;
        $bins = BinTransfers::where('bin_location','=', $request->bin_location)
            ->get();
        //dd($bin);

        return view('bins.bin', compact('bins','bin'));
    }

    public function lpScan(Request $request)
    {
        $newlp = $request->all();

        $dup = BinTransfers::where('bin_location', '=', $newlp['bin_location'])
            ->where('license_plate','=', $newlp['license_plate'])
            ->first();
        //dd($dup);

        if(is_null($dup))
        {
            $newlp = BinTransfers::create($newlp);

            Alert::toast('LP added to Bin', 'success');
            return redirect('/bins/'. $newlp['bin_location']);
        }else{
            //$newlp = BinTransfers::create($newlp);

            Alert::toast('LP already in bin', 'warning');
            return redirect('/bins/'. $newlp['bin_location']);
        }
    }

    public function viewBin($id)
    {
        $bins = BinTransfers::where('bin_location','=', $id)
            ->orderby('license_plate')
            ->get();

        if($bins->count() > 0)
        {
            $bin = $bins->first()->bin_location;
            return view('bins.bin', compact('bins','bin'));
        }else{
            return view('bins.scan');
        }
    }

    public function dashboard()
    {
        $allBins = BinTransfers::where('bin_status','<>','Complete')
            ->orderby('created_at','DESC')
            ->get();
        return view('bins.dashboard',compact('allBins'));
    }

    public function dashboardCompleted()
    {
        $allBins = BinTransfers::where('bin_status','=','Complete')
            ->get();
        return view('bins.dashboard_complete',compact('allBins'));
    }

    public function markComplete($id)
    {
        $bin = BinTransfers::findorfail($id);

        $bin->bin_status = 'Complete';
        $bin->save();
        Alert::toast('Bin Marked as COMPLETE', 'success');
        return redirect('/bins/dashboard');
    }

    public function markPending($id)
    {
        $bin = BinTransfers::findorfail($id);

        $bin->bin_status = 'Pending';
        $bin->save();
        Alert::toast('Bin Marked as PENDING', 'success');
        return redirect('/bins/dashboard/complete');
    }

    public function inventory()
    {
        return view('bins.inventory');
    }

    public function InventorybinScan(Request $request)
    {

        $bin = $request->bin_location;
        $bins = BinTransfers::where('bin_location','=', $request->bin_location)
            ->where('scan_type','=','Inventory Scan')
            ->get();
        //dd($bin);

        return view('bins.inventorybins', compact('bins','bin'));
    }

    public function lpInventory(Request $request)
    {
        $newlp = $request->all();

        //dd($newlp);
        //dd('Hitting here');

        $dup = BinTransfers::where('bin_location', '=', $newlp['bin_location'])
            ->where('license_plate','=', $newlp['license_plate'])
            ->where('scan_type','=','Inventory Scan')
            ->first();
        //dd($dup);

        if(is_null($dup))
        {
            $newlp = BinTransfers::create($newlp);

            Alert::toast('LP added to Bin', 'success');
            return redirect('/bins/inventory/'. $newlp['bin_location']);
        }else{
            //$newlp = BinTransfers::create($newlp);

            Alert::toast('LP already in bin', 'warning');
            return redirect('/bins/inventory/'. $newlp['bin_location']);
        }

    }
    public function viewBinInventory($id)
    {
        $bins = BinTransfers::where('bin_location','=', $id)
            ->orderby('license_plate')
            ->get();

        if($bins->count() > 0)
        {
            $bin = $bins->first()->bin_location;
            return view('bins.inventorybins', compact('bins','bin'));
        }else{
            return view('bins.inventory');
        }
    }
}
