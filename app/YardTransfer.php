<?php

namespace App;

use App\Traits\Observable;
use Illuminate\Database\Eloquent\Model;

class YardTransfer extends Model
{

    use Observable;

    public static function logSubject(Model $model): string
    {
        return sprintf( "User [id:%d] %s/%s",
            $model->id, $model->name, $model->email
        );
    }

    public function CurrentLOC(){
        return $this->hasOne('\App\Yard','id','current_location_id');
    }

    public function NewLOC(){
        return $this->hasOne('\App\Yard','id','new_location_id');
    }

    public function TransDetail(){
        return $this->hasOne('\App\YardTransaction','id','detail_id');
    }

    public function createdBy(){
        return $this->hasOne('\App\User','id','created_by');
    }

    public function assignedTo(){
        return $this->hasOne('\App\User','id','assigned_to');
    }
}
