<?php

namespace App;

use App\Traits\Observable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ExpenseItems extends Model

{
    use SoftDeletes;
    use Observable;

    public static function logSubject(Model $model): string
    {
        return sprintf( "User [id:%d] %s/%s",
            $model->id, $model->name, $model->email
        );
    }

    protected $table = 'expense_items';
    protected $guarded = [];


    public function Buildings(){
        return $this->hasOne('\App\buildings','id','building_number');
    }
}
