<?php

namespace App;

use App\Traits\Observable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BinTransfers extends Model
{
    use SoftDeletes;
    protected $table = 'bin_transfers';
    protected $guarded = [];

    use Observable;

    public static function logSubject(Model $model): string
    {
        return sprintf( "User [id:%d] %s/%s",
            $model->id, $model->name, $model->email
        );
    }

    public function employee(){
        return $this->hasOne('\App\User','id','created_by');
    }
}
