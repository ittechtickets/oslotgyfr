{{--New file Template--}}

{{--Add Security for this page below--}}


@extends('layouts.dashboard')
{{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')

    @if( Auth::user()->hasRole('Daily Charges') == FALSE)
        @include('layouts.unauthorized')

    @Else

        <style>
            .modal-lg {
                max-width: 80%;
            }
        </style>


        <div class="card mb-4 mt-2 bg-dark text-white">
            <div class="card-header">
                <b>Camelot Daily Charges</b> - [DATA DATE]
                <div class="float-right"><a href="#" class="btn btn-sm btn-light disabled">Download CSV</a> </div>
            </div>
            <div class="card-body">
                <div class="card-deck">
                    <div class="card bg-dark">
                        <div class="card-body">
                            <b>Total Charges</b>
                            <div class="float-right" style="font-weight: bold; font-size: 30px;">${{number_format($transactions->sum("Charge amount"),2)}}</div>
                        </div>
                    </div>
                    <div class="card bg-dark">
                        <div class="card-body">
                            <b>Transactions</b>
                            <div class="float-right" style="font-weight: bold; font-size: 30px;">{{$transactions->count()}}</div>
                        </div>
                    </div>
                    <div class="card bg-dark" data-toggle="modal" data-target="#exampleModal">
                        <div class="card-body">
                            <b>Transactions without Charges</b>
                            <div class="float-right" style="font-weight: bold; font-size: 30px;">{{$no_charges->count()}}</div>
                        </div>
                    </div>
                    <div class="card bg-dark">
                        <div class="card-body">
                            <b>Transactions with Charges</b>
                            <div class="float-right" style="font-weight: bold; font-size: 30px;">{{$charges->count()}}</div>
                        </div>
                    </div>
                </div>
                <div class="card-deck mt-2">
                    <div class="card">
                        <div class="card-body bg-dark">
                            <canvas id="bar-chart-horizontal" height="100"></canvas>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body bg-dark">
                            <canvas id="no_charges_user" height="100"></canvas>
                        </div>
                    </div>
                </div>
                <div class="card-deck" mt-2>
                    <div class="card-body bg-dark">
                        Transactions by TYPE
                    </div>
                    <div class="card-body bg-dark">
                        Charges Breakdown
                    </div>
                </div>
            </div>
        </div>
    @endif

@include('dailycharges.modals.no_charge')

@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="text/javascript">

        $(document).ready( function () {
            $('#no_charges').DataTable();
        } );

        //Charges by customer
        new Chart(document.getElementById("bar-chart-horizontal"), {
            type: 'horizontalBar',
            data: {
                labels: [
                    @foreach($charges_customer as $charges)
                    "{{$charges->Client}}",
                    @endforeach
                ],
                datasets: [
                    {
                        label: "Charges (Dollars)",
                        backgroundColor: ["#5899DA", "#E8743B","#19A979","#ED4A7B","#945ECF","#13A4B4","#525DF4","#BF399E","#6C8893","#EE6868","#2F6497"],
                        data: [
                            @foreach($charges_customer as $charges)
                                {{round($charges->total_charges, 2)}},
                            @endforeach
                        ]
                    }
                ]
            },
            options: {
                legend: { display: false },
                title: {
                    display: true,
                    text: 'Charges by Client'
                }
            }
        });

        //NO Charges by User
        new Chart(document.getElementById("no_charges_user"), {
            type: 'horizontalBar',
            data: {
                labels: [
                    @foreach($no_charges_user as $charges)
                        "{{$charges->user_id}}",
                    @endforeach
                ],
                datasets: [
                    {
                        label: "Transactions",
                        backgroundColor: ["#5899DA", "#E8743B","#19A979","#ED4A7B","#945ECF","#13A4B4","#525DF4","#BF399E","#6C8893","#EE6868","#2F6497"],
                        data: [
                            @foreach($no_charges_user as $charges)
                            {{$charges->total_transactions}},
                            @endforeach
                        ]
                    }
                ]
            },
            options: {
                legend: { display: false },
                title: {
                    display: true,
                    text: 'Transactions by User W/O Charges'
                }
            }
        });

    </script>

@endsection
