<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><b>No Charge Transactions</b></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table" id="no_charges" width="100%">
                    <thead>
                        <tr>
                            <td><b>Document</b></td>
                            <td><b>Client</b></td>
                            <td><b>Hr Reference</b></td>
                            <td><b>Warehouse</b></td>
                            <td><b>User</b></td>
                            <td><b>Status</b></td>
                            <td><b>Charge</b></td>
                            <td><b>Description</b></td>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($no_charges as $no_charge)
                        <tr>
                            <td>{{$no_charge->Document}}</td>
                            <td>{{$no_charge->Client}}</td>
                            <td>{{$no_charge->{"Header reference"} }}</td>
                            <td>{{$no_charge->Warehouse}}</td>
                            <td>{{$no_charge->user_id}}</td>
                            <td>{{$no_charge->{"Workflow Status"} }}</td>
                            <td>{{$no_charge->{"Charge amount"} }}</td>
                            <td>{{$no_charge->description}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
