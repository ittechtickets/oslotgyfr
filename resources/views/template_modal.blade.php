<!-- Modal Template -->
<div class="modal fade" id="NewExpense" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form method="post" action="">
                @csrf
                <input type="hidden" name="client_id" value="">
                <div class="modal-header text-white"  style="background-color: #1a32ba;">
                    <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
                    <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
                    <input  type="submit" class="btn btn-sm btn-primary" value="Submit Form">
                </div>
            </form>
        </div>
    </div>
</div>
