{{--New file Template--}}

{{--Add Security for this page below--}}


@extends('layouts.validating')
{{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')




    <div class="card mt-4">
        <div class="card-header" style="background-color: #91bcff;">
            <b>Pending Account Validation</b>

            <a class="float-right btn btn-dark btn-sm" href="{{ route('logout') }}"
               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                <i class="fad fa-sign-out-alt"></i> {{ __('Logout') }}
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </div>
        <div class="card-body">
            This account has been disabled.
        </div>
    </div>


@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="text/javascript">

        $(document).ready( function () {
            $('#clients').DataTable();
        } );

    </script>

@endsection
