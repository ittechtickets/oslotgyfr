{{--New file Template--}}

{{--Add Security for this page below--}}


@extends('layouts.print')
{{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')

    @if( Auth::user()->hasRole('Admin') == FALSE)
        @include('layouts.unauthorized')

    @Else
        <br>
        <table width="100%"  border="0" cellspacing="0" cellpadding="3">
            <tr>
                <td align="center">
                    <img src="{{url('img/Outsource Logistics_500.png')}}" class="">
                </td>
            </tr>
            <tr>
                <td WIDTH="50%" align="center"><img src="{{asset('storage/users/'. $user->new_filename)}}" height="600" ></td>
            </tr>
            <tr>
                <td align="center" colspan="2" style="font-size: 90px;"><b>{{$user->first_name}} {{$user->last_name}}</b></td>
            </tr>
            <tr>
                <td align="center" colspan="2"  style="font-size: 80px;"><b>{{$user->title}}</b></td>
            </tr>
        </table>

    @endif

@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="text/javascript">

        $(document).ready( function () {
            $('#clients').DataTable();
        } );

    </script>

@endsection
