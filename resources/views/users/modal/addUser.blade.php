<!-- Modal Template -->
<div class="modal fade" id="AddUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
        <div class="modal-content">
            <form method="post" action="/user/add" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="client_id" value="">
                <div class="modal-header bg-warning">
                    <h5 class="modal-title" id="exampleModalLongTitle"><b>Add User</b></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="exampleFormControlInput1"><b>First Name</b></label>
                        <input type="text" name="first_name" class="form-control" id="exampleFormControlInput1" placeholder="First Name" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlInput1"><b>Last Name</b></label>
                        <input type="text" name="last_name" class="form-control" id="exampleFormControlInput1" placeholder="Last Name" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlInput1"><b>Title</b></label>
                        <input type="text" name="title" class="form-control" id="exampleFormControlInput1" placeholder="Title">
                    </div>
                    <div class="form-group">
                        <label for="validationCustom09"><b>Employee Status</b></label>
                        <select class="form-control" id="validationCustom09" name="employee_status" >
                            <option selected value="Full Time">Full Time</option>
                            <option value="Part Time">Part Time</option>
                            <option value="Temp">Temp</option>
                            <option value="FLMA">FLMA</option>
                            <option value="Workers Comp">Workers Comp</option>
                            <option value="IFLMA">IFLMA</option>
                            <option value="Terminated">Terminated</option>
                            <option value="Severance">Severance</option>
                            <option value="Not Employed">Not Employed</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="validationCustom09"><b>Account Type</b></label>
                        <select class="form-control" id="validationCustom09" name="account_type" >
                            <option value="Applicant">Applicant</option>
                            <option value="Vendor">Vendor</option>
                            <option selected value="Employee">Employee</option>
                            <option value="Temp Employee">Temp Employee</option>
                            <option value="Customer">Customer</option>
                            <option value="Disabled">Disabled</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlFile1"><b>Select Image</b></label>
                        <input type="file" name="file_name" class="form-control-file" id="exampleFormControlFile1">
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlInput1"><b>Email</b></label>
                        <input type="email" name="email" class="form-control" id="exampleFormControlInput1" placeholder="Email Address" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlInput1"><b>Password</b></label>
                        <input type="password" name="password" class="form-control" id="exampleFormControlInput1" value="Oslog123!" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
                    <input  type="submit" class="btn btn-sm btn-primary" value="Add User">
                </div>
            </form>
        </div>
    </div>
</div>
