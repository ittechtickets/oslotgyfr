{{--New file Template--}}

{{--Add Security for this page below--}}



{{--Updated 12/6/2018 for Bootstrap 4.1--}}

<style>
    p{
        white-space:pre-wrap;
    }
</style>

<?php

for ($x = 1; $x <= $plt; $x++) {
?>
<table width="100%">
    <tr>
        <td style="font-size: 25px;" align="right">
            <b>Ship From:</b>
        </td>
    </tr>
    <tr>
        <td style="font-size: 18px;" align="right">
            <p>{{$fields['ship_from']}}</p>
        </td>
    </tr>
</table>

<table width="100%" border="1">
    <tr>
        <td style="font-size: 25px;">
            <b>Ship TO:</b>
        </td>
    </tr>
    <tr>
        <td style="font-size: 18px;">
            <p>{{$fields['ship_to']}}</p>
        </td>
    </tr>
</table>

<table width="100%">
    <tr>
        <td width="50%">PO Number: {{$fields['po_number']}}</td>
        <td align="right">Pallet {{$x}} of {{$fields['number_plt']}}</td>
    </tr>
</table>
<div style="page-break-after: always;"></div>
<?php
}
?>






{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="text/javascript">

        $(document).ready( function () {
            $('#clients').DataTable();
        } );

    </script>

@endsection
