{{--New file Template--}}

{{--Add Security for this page below--}}


@extends('layouts.app')
{{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')




        <div class="card mb-4">
            <div class="card-header text-white" style="background-color: #413c69;">
                <b>Shipping Label</b>
            </div>
            <div class="card-body">
                <form action="/ship/print" method="post" target="_blank">
                    @csrf
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1"><b>Ship From</b></label>
                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="ship_from"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1"><b>Ship To</b></label>
                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="ship_to"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlInput1"><b>PO Number</b></label>
                        <input type="input" name="po_number" class="form-control" id="exampleFormControlInput1" >
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlInput1"><b>Number of Pallets</b></label>
                        <input type="input" name="number_plt" class="form-control" id="exampleFormControlInput1">
                    </div>
                    <input type="submit" value="Print" class="btn btn-primary">
                </form>
            </div>
        </div>


@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="text/javascript">

        $(document).ready( function () {
            $('#clients').DataTable();
        } );

    </script>

@endsection
