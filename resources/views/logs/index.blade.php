{{--New file Template--}}

{{--Add Security for this page below--}}


@extends('layouts.app')
{{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')

    @if( Auth::user()->hasRole('Admin') == FALSE)
        @include('layouts.unauthorized')

    @Else


        <div class="card mb-4">
            <div class="card-header text-white" style="background-color: #413c69;">
                <b>Logs</b>
            </div>
            <div class="card-body">
                <table class="table table-sm table-hover" id="logs">
                    <thead>
                    <tr>
                        <td><b>User ID</b></td>
                        <td><b>Model</b></td>
                        <td><b>Action</b></td>
                        <td><b>Message</b></td>
                        <td><b>Created At</b></td>
                        <td></td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($logs as $log)
                        <tr>
                            <td>@if(is_null($log->user_id)) --- @else {{$log->person->first_name}} {{$log->person->last_name}} @endif</td>
                            <td>{{$log->model}}</td>
                            <td>{{$log->action}}</td>
                            <td>{{$log->message}}</td>
                            <td>{{$log->created_at}}</td>
                            <td align="right"><button class="btn btn-sm btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                                    Details
                                </button></td>
                        </tr>
                        <tr class="collapse" id="collapseExample">
                            <td colspan="6"><pre>{{json_encode($log->models, JSON_PRETTY_PRINT)}}</pre></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @endif

@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="text/javascript">

        $(document).ready( function () {
            $('#clients').DataTable();
        } );

    </script>

@endsection
