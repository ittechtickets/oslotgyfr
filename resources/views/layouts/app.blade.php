<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<?php
$moose_talk = array('meooow','bggrrwww','rawwww','Purrrrrr','MEOW!','Buuuggguu','miaow','mrruh','prrrup','mrow','yowl','mrrrrrr','groooour');
$randomNumber = rand(0, (count($moose_talk) - 1));
?>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>



    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- Site Variables -->
    <link src="/css/osl.css">
    <!-- Datatables-->
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">

    <script src="/js/fontawesome.js"></script>
    <style>
        body {
            background-color: #709fb0;
        }

        #mybutton {
            position: fixed;
            bottom: -4px;
            right: 10px;
            opacity: 0.5;
            filter: alpha(opacity=50); /* For IE8 and earlier */
        }
    </style>
</head>
<body>

<div id="mybutton">
<img src="/img/Cat-icon_35.png" title="{{$moose_talk[rand(0, count($moose_talk) - 1)]}}" data-toggle="modal" data-target="#mooseSupport"></img>
</div>

@include('navbar.default', ['emps' => $emps = App\User::where('account_type','=','Employee')->where('employee_status','<>','Terminated')->get()])



<div class="container-fluid">
    @yield('content')


</div>
@include('sweetalert::alert')
    <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script type="text/javascript" src="//cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>

@yield('scripts')
</body>
</html>
<script type="text/javascript">

    $(document).ready( function () {
        $('#employees').DataTable({
            "pageLength": 10
        });
        $('div.dataTables_filter input').focus()
    } );

</script>
@include('help.moose')
