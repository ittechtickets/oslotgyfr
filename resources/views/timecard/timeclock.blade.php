{{--New file Template--}}

{{--Add Security for this page below--}}


@extends('layouts.timeclock')
{{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')

<div class="col-lg-3 offset-lg-4">
    <div class="card mt-5">
        <div class="card-header text-center">
            <img src="{{url('img/Outsource Logistics_Transparent_300.png')}}" class="">
        </div>
        <div class="card-body">
            <form method="post" action="/timeclock/login">
                @csrf

                @if(Session::has('message'))
                    <p class="alert {{ Session::get('alert-class', 'alert-info') }} alert-message">{{ Session::get('message') }}</p>
                @endif

                <div class="form-row">
                    <label><b>Enter Timecard ID</b></label>
                    <input type="text" name="timecard_id" class="form-control" required>
                </div>
                <div class="form-row">
                    <input type="submit" name="" value="Enter" class="btn btn-lg btn-primary btn-block mt-2" onclick="getLocation()">
                </div>
            </form>
        </div>
    </div>
</div>



@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="text/javascript">

        //Flash alert fade away
        $(".alert-message").alert();
        window.setTimeout(function() { $(".alert-message").alert('close'); }, 5000);
        //END Flash alert fade away



    </script>

@endsection
