{{--New file Template--}}

{{--Add Security for this page below--}}


@extends('layouts.app')
{{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')

    @if( Auth::user()->hasRole('Timeclock Admin') == FALSE)
        @include('layouts.unauthorized')

    @Else


        <div class="card mb-4">
            <div class="card-header text-white" style="background-color: #413c69;">
                <b>Users</b>
                <div class="btn-group btn-group-sm float-right">
                    <a href="/timeclock/admin" class="btn btn-sm btn-secondary">Back to Admin</a>
                    <a href="#" class="btn btn-sm btn-secondary" data-toggle="modal" data-target="#newuser">Add User</a>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-hover" id="users">
                    <thead>
                        <tr>
                            <td><b>Name</b></td>
                            <td><b>Company</b></td>
                            <td><b>Time Clock ID</b></td>
                            <td><b>LDW</b></td>
                            <td><b>MP 30-Days</b></td>
                            <td><b>Hrs</b></td>
                            <td></td>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>{{$user->last_name}}, {{$user->first_name}}</td>
                            <td>{{$user->company}}</td>
                            <td>{{$user->timeclock_id}}</td>
                            <td>LDW</td>
                            <td>MP</td>
                            <td>Hrs</td>
                            <td align="right">[Function]</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @endif
@include("timecard.modal.newuser")
@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="text/javascript">

        $(document).ready( function () {
            $('#users').DataTable();
        } );

    </script>

@endsection
