{{--New file Template--}}

{{--Add Security for this page below--}}


@extends('layouts.app')
{{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')

    @if( Auth::user()->hasRole('Bin Transfers') == FALSE)
        @include('layouts.unauthorized')

    @Else


        <div class="card mb-4">
            <div class="card-header" style="background-color: #91bcff;">
                <span style="font-weight: bold; font-size: 20px;">COMPLETE ({{$allBins->count()}})</span>

                <div class="btn-group btn-sm float-right" role="group" aria-label="Button group with nested dropdown">
                    <a href="" class="btn btn-sm btn-dark" onClick="history.go(0);"><i class="fad fa-retweet-alt"></i></a>
                    <div class="btn-group" role="group">
                        <button id="btnGroupDrop1" type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            View
                        </button>
                        <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                            <a class="dropdown-item" href="/bins/dashboard/complete">View Completed</a>
                            <a class="dropdown-item" href="/bins/dashboard">View Pending</a>
                        </div>
                    </div>
                    <a href="/bins" class="btn btn-sm btn-primary"><i class="fad fa-home"></i></a>
                </div>

            </div>
            <form method="post" action="/bins/scan/bin">
                @csrf
                <div class="card-body">
                    <table class="table table-striped table-hover" id="pending">
                        <thead>
                        <tr>
                            <td align="left"><b>View</b></td>
                            <td align="center"><b>Bin</b></td>
                            <td align="center"><b>LP</b></td>
                            <td align="right"><b>UnDo</b></td>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($allBins as $bin)
                            <tr>
                                <td align="left">
                                    <a href="#" class="btn btn-sm btn-secondary ViewBinDetail"
                                       data-bin_location="{{$bin->bin_location}}"
                                       data-license_plate="{{$bin->license_plate}}"
                                       data-created_by="{{$bin->employee->last_name}}, {{$bin->employee->first_name}}"
                                       data-created_at="{{$bin->created_at}}"
                                       data-bin_status="{{$bin->bin_status}}"
                                    ><i class="fad fa-info-square"></i></a>
                                </td>
                                <td align="center">{{$bin->bin_location}}</td>
                                <td align="center">{{$bin->license_plate}}</td>
                                <td align="right"><a href="/bins/bins/pending/{{$bin->id}}" class="btn btn-sm btn-info"><i class="fad fa-check-square"></i></a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </form>
        </div>
    @endif
    @include('bins.modals.detail')
@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="text/javascript">

        $(document).ready( function () {
            $('#pending').DataTable();
        } );

        $(document).on("click", ".ViewBinDetail", function () {
            $('.bin_location').html($(this).attr("data-bin_location"));
            $('.license_plate').html($(this).attr("data-license_plate"));
            $('.created_by').html($(this).attr("data-created_by"));
            $('.created_at').html($(this).attr("data-created_at"));
            $('.bin_status').html($(this).attr("data-bin_status"));
            $('#BinDetail').modal('show');
        });


    </script>

@endsection
