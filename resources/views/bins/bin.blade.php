{{--New file Template--}}

{{--Add Security for this page below--}}


@extends('layouts.app')
{{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')

    @if( Auth::user()->hasRole('Bin Transfers') == FALSE)
        @include('layouts.unauthorized')

    @Else


        <div class="card mb-4">
            <div class="card-header" style="background-color: #91bcff;">
                <b>@if($bin) {{$bin}}  @else {{$bins->first()->bin_location}} @endif</b> | Total LP: {{$bins->count()}}
                <a href="/bins/scan" class="btn btn-sm btn-primary float-right"><i class="fad fa-home"></i></a>
            </div>
            <form method="post" action="/bins/scan/lp">
                @csrf
                <input type="hidden" value="@if($bin) {{$bin}}  @else {{$bins->first()->bin_location}} @endif" name="bin_location">
                <input type="hidden" value="{{ Auth::user()->id }}" name="created_by">
                <input type="hidden" value="Pending" name="bin_status">
                <div class="card-body">
                    <input class="form-control form-control-lg " name="license_plate" type="text" placeholder="Scan License Plate" autofocus required>

                    @if($bins->isEmpty())
                        <div class="alert alert-info mt-3">No LP's</div>
                    @else
                    <table class="table table-sm mt-3">
                        <thead>
                            <tr>
                                <td><b>LP</b></td>
                                <td align="center"><b>View</b></td>
                                <td align="right"><b>Del</b></td>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($bins as $lp)
                            <tr>
                                <td>{{$lp->license_plate}}</td>
                                <td align="center">
                                    <a href="#" class="btn btn-sm btn-secondary ViewBinDetail"
                                       data-bin_location="{{$lp->bin_location}}"
                                       data-license_plate="{{$lp->license_plate}}"
                                       data-created_by="{{$lp->employee->last_name}}, {{$lp->employee->first_name}}"
                                       data-created_at="{{$lp->created_at}}"
                                       data-bin_status="{{$lp->bin_status}}"
                                    ><i class="fad fa-info-square"></i></a>
                                </td>
                                <td align="right"><a href="/bins/delete/{{$lp->id}}" class="btn btn-sm btn-danger"><i class="fad fa-trash-alt"></i></a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    @endif
                </div>
                <div class="card-footer">
                    <i>Pull down to refresh data.</i>
                </div>
            </form>
        </div>
    @endif
@include('bins.modals.detail')
@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="text/javascript">

        $(document).ready( function () {
            $('#clients').DataTable();
        } );

        $(document).on("click", ".ViewBinDetail", function () {
            $('.bin_location').html($(this).attr("data-bin_location"));
            $('.license_plate').html($(this).attr("data-license_plate"));
            $('.created_by').html($(this).attr("data-created_by"));
            $('.created_at').html($(this).attr("data-created_at"));
            $('.bin_status').html($(this).attr("data-bin_status"));
            $('#BinDetail').modal('show');
        });
    </script>

@endsection
