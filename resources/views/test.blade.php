<!DOCTYPE html>
<html>
<body>

<h1>My First Google Map</h1>

<div id="googleMap" style="width:100%;height:800px;"></div>

<script>
    function myMap() {
        const clockedIn = { lat: 31.847510999999994, lng: -83.2196372 };
        const clockedOut = { lat: 30.847510999999994, lng: -83.2196372 };
        const map = new google.maps.Map(document.getElementById("googleMap"), {
            zoom: 14,
            center: clockedIn,
        });
        new google.maps.Marker({
            position: clockedIn,
            map,
            title: "Clocked In",
        });
        new google.maps.Marker({
            position: clockedOut,
            map,
            title: "Clocked In",
        });
    }
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAlapyIgcGKgH0lX89IN1UXGTEK37ZZuCs&callback=myMap"></script>

</body>
</html>
