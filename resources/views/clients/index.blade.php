{{--New file Template--}}

{{--Add Security for this page below--}}


@extends('layouts.app')
{{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')

    @if( Auth::user()->hasRole('Clients') == FALSE)
       @include('layouts.unauthorized')

    @Else
        <div class="card mb-4">
            <div class="card-header" style="background-color: #91bcff;">
                <b>OL Clients</b>
            </div>
            <div class="card-body">
                <table class="table table-hover table-sm" id="clients" width="100%">
                    <thead>
                    <tr>
                        <td><b>No.</b></td>
                        <td><b>Name</b></td>
                        <td><b>Phone</b></td>
                        <td><b>Contact</b></td>
                        <td><b>Address</b></td>
                        <td><b>State</b></td>
                        <td></td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($allClients as $client)
                        <tr>
                            <td>{{$client->number}}</td>
                            <td>{{$client->name}}</td>
                            <td>{{$client->phone_number}}</td>
                            <td>{{$client->contact}}</td>
                            <td>{{$client->address}}</td>
                            <td>{{$client->city}}</td>
                            <td align="right"><a href="/clients/{{$client->id}}" class="btn btn-sm btn-primary"><i class="fad fa-arrow-circle-right"></i></a> </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @endif

@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="text/javascript">

        $(document).ready( function () {
            $('#clients').DataTable();
        } );

    </script>

@endsection



