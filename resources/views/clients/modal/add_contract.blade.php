<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <form method="post" action="/clients/contract/add" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="client_id" value="{{$client->id}}">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Add Contract</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-8">
                            <div class="form-group">
                                <label for="exampleInputPassword1"><b>Select File</b></label>
                                <input type="file" class="form-control-file" id="exampleFormControlFile1" name="file_name" required>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group">
                                <label for="exampleInputPassword1"><b>Reminder Days</b></label>
                                <input type="int" class="form-control" name="reminder" value="30" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="exampleInputPassword1"><b>Date Signed</b></label>
                                <input type="date" class="form-control" name="signed_date">
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="exampleInputPassword1"><b>Renewal Date</b></label>
                                <input type="date" class="form-control" name="renewal_date" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary btn-sm" value="Save Contract">
                </div>
            </form>
        </div>
    </div>
</div>
