{{--New file Template--}}

{{--Add Security for this page below--}}


@extends('layouts.yard')
{{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')

    @if( Auth::user()->hasRole('Yard') == FALSE)
        @include('layouts.unauthorized')

    @Else


        <div class="card mb-1 mt-2">
            <div class="card-header text-white" style="background-color: #413c69;">
                <b>Switching Dashboard</b>
            </div>
            <div class="card-body">
                <!-- Ajax loads data below -->
                <div class="" id="SwitchLoad"></div>
                <!-- Ajax Done -->
            </div>
        </div>
    @endif

@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="text/javascript">

        //Auto Update
        $(document).ready(function() {
            $('#SwitchLoad').load('/yard/switch/feed');
            var refreshId2 = setInterval(function() {
                $("#SwitchLoad").load('/yard/switch/feed');
                //alert('reloaded')
            }, 1000);
            $.ajaxSetup({ cache: false });
        });
        //END Auto Update

        $(document).ready( function () {
            $('#clients').DataTable();
        } );

    </script>

@endsection
