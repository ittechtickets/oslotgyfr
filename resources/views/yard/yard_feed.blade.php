<?PHP
$cellwidth = 55;
$rowheight = 73;

$yard_cellwidth = 55;
$yard_rowheight = 73;
?>

<table class="table table-bordered table-responsive text-white" width="100%">

    <tr height="{{$rowheight}}">
        @foreach($first_row as $dock)
            <td align="center" style="vertical-align:middle;" class="
                                                            @if($dock->location_status == 'Empty') bg-primary
                                                            @elseif($dock->location_status == 'Damaged') bg-dark
                                                            @elseif($dock->location_status == 'Equipment') bg-dark
                                                            @elseif($dock->location_status == 'Occupied') bg-danger
                                                            @elseif($dock->location_status == 'Transfer') bg-warning
                                                            @elseif($dock->location_status == 'Complete') bg-success
                                                            @endif LocationDetail" width="{{$cellwidth}}"
                data-location_id = "{{$dock->id}}"
                data-location_type="{{$dock->location_type}}"
                data-location_number="{{$dock->location_number}}"
                data-location_status="{{$dock->location_status}}"
            >
                <b>{{$dock->location_number}}</b>
                <br>

                @include('yard.inc.yard_icons')


            </td>
        @endforeach
    </tr>

    <tr height="{{$rowheight}}">
        @foreach($second_row as $dock)
            <td align="center" style="vertical-align:middle;" class="
                                                            @if($dock->location_status == 'Empty') bg-primary
                                                            @elseif($dock->location_status == 'Damaged') bg-dark
                                                            @elseif($dock->location_status == 'Equipment') bg-dark
                                                            @elseif($dock->location_status == 'Occupied') bg-danger
                                                            @elseif($dock->location_status == 'Transfer') bg-warning
                                                            @elseif($dock->location_status == 'Complete') bg-success
                                                            @endif LocationDetail" width="{{$cellwidth}}"
                width="{{$cellwidth}}"
                data-location_id = "{{$dock->id}}"
                data-location_type="{{$dock->location_type}}"
                data-location_number="{{$dock->location_number}}"
                data-location_status="{{$dock->location_status}}"
            >
                <b>{{$dock->location_number}}</b>
                <br>

                @include('yard.inc.yard_icons')

            </td>
        @endforeach
    </tr>

</table>

<table class="table table-bordered table-responsive text-white" width="100%">

    <tr height="{{$yard_rowheight}}">
        @foreach($yard_first_row as $dock)
            <td align="center" style="vertical-align:middle;" class="
                                                            @if($dock->location_status == 'Empty') bg-primary
                                                            @elseif($dock->location_status == 'Damaged') bg-dark
                                                            @elseif($dock->location_status == 'Equipment') bg-dark
                                                            @elseif($dock->location_status == 'Occupied') bg-danger
                                                            @elseif($dock->location_status == 'Transfer') bg-warning
                                                            @elseif($dock->location_status == 'Complete') bg-success
                                                            @endif LocationDetail"
                width="{{$yard_cellwidth}}"
                data-location_id = "{{$dock->id}}"
                data-location_type="{{$dock->location_type}}"
                data-location_number="{{$dock->location_number}}"
                data-location_status="{{$dock->location_status}}"
            >
                <b>{{$dock->location_number}}</b>
                <br>

                @include('yard.inc.yard_icons')

            </td>
        @endforeach
    </tr>

    <tr height="{{$yard_rowheight}}">
        @foreach($yard_second_row as $dock)
            <td align="center" style="vertical-align:middle;" class="
                                                            @if($dock->location_status == 'Empty') bg-primary
                                                            @elseif($dock->location_status == 'Damaged') bg-dark
                                                            @elseif($dock->location_status == 'Equipment') bg-dark
                                                            @elseif($dock->location_status == 'Occupied') bg-danger
                                                            @elseif($dock->location_status == 'Transfer') bg-warning
                                                            @elseif($dock->location_status == 'Complete') bg-success
                                                            @endif LocationDetail" width="{{$yard_cellwidth}}"
                width="{{$yard_cellwidth}}"
                data-location_id = "{{$dock->id}}"
                data-location_type="{{$dock->location_type}}"
                data-location_number="{{$dock->location_number}}"
                data-location_status="{{$dock->location_status}}"
            >
                <b>{{$dock->location_number}}</b>
                <br>

                @include('yard.inc.yard_icons')

            </td>
        @endforeach
    </tr>

    <tr height="{{$yard_rowheight}}">
        @foreach($yard_third_row as $dock)
            <td align="center" style="vertical-align:middle;" class="
                                                            @if($dock->location_status == 'Empty') bg-primary
                                                            @elseif($dock->location_status == 'Damaged') bg-dark
                                                            @elseif($dock->location_status == 'Equipment') bg-dark
                                                            @elseif($dock->location_status == 'Occupied') bg-danger
                                                            @elseif($dock->location_status == 'Transfer') bg-warning
                                                            @elseif($dock->location_status == 'Complete') bg-success
                                                            @endif LocationDetail" width="{{$yard_cellwidth}}"
                width="{{$yard_cellwidth}}"
                data-location_id = "{{$dock->id}}"
                data-location_type="{{$dock->location_type}}"
                data-location_number="{{$dock->location_number}}"
                data-location_status="{{$dock->location_status}}"
            >
                <b>{{$dock->location_number}}</b>
                <br>

                @include('yard.inc.yard_icons')

            </td>
        @endforeach
    </tr>

    <tr height="{{$yard_rowheight}}">
        @foreach($yard_forth_row as $dock)
            <td align="center" style="vertical-align:middle;" class="
                                                            @if($dock->location_status == 'Empty') bg-primary
                                                            @elseif($dock->location_status == 'Damaged') bg-dark
                                                            @elseif($dock->location_status == 'Equipment') bg-dark
                                                            @elseif($dock->location_status == 'Occupied') bg-danger
                                                            @elseif($dock->location_status == 'Transfer') bg-warning
                                                            @elseif($dock->location_status == 'Complete') bg-success
                                                            @endif LocationDetail" width="{{$yard_cellwidth}}"
                width="{{$yard_cellwidth}}"
                data-location_id = "{{$dock->id}}"
                data-location_type="{{$dock->location_type}}"
                data-location_number="{{$dock->location_number}}"
                data-location_status="{{$dock->location_status}}"
            >
                <b>{{$dock->location_number}}</b>
                <br>

                @include('yard.inc.yard_icons')

            </td>
        @endforeach
    </tr>

    <tr height="{{$yard_rowheight}}">
        @foreach($yard_fifth_row as $dock)
            <td align="center" style="vertical-align:middle;" class="
                                                            @if($dock->location_status == 'Empty') bg-primary
                                                            @elseif($dock->location_status == 'Damaged') bg-dark
                                                            @elseif($dock->location_status == 'Equipment') bg-dark
                                                            @elseif($dock->location_status == 'Occupied') bg-danger
                                                            @elseif($dock->location_status == 'Transfer') bg-warning
                                                            @elseif($dock->location_status == 'Complete') bg-success
                                                            @endif LocationDetail" width="{{$yard_cellwidth}}"
                width="{{$yard_cellwidth}}"
                data-location_id = "{{$dock->id}}"
                data-location_type="{{$dock->location_type}}"
                data-location_number="{{$dock->location_number}}"
                data-location_status="{{$dock->location_status}}"
            >
                <b>{{$dock->location_number}}</b>
                <br>

                @include('yard.inc.yard_icons')

            </td>
        @endforeach
    </tr>

    <tr height="{{$yard_rowheight}}">
        @foreach($yard_sixth_row as $dock)
            <td align="center" style="vertical-align:middle;" class="
                                                            @if($dock->location_status == 'Empty') bg-primary
                                                            @elseif($dock->location_status == 'Damaged') bg-dark
                                                            @elseif($dock->location_status == 'Equipment') bg-dark
                                                            @elseif($dock->location_status == 'Occupied') bg-danger
                                                            @elseif($dock->location_status == 'Transfer') bg-warning
                                                            @elseif($dock->location_status == 'Complete') bg-success
                                                            @endif LocationDetail" width="{{$yard_cellwidth}}"
                width="{{$yard_cellwidth}}"
                data-location_id = "{{$dock->id}}"
                data-location_type="{{$dock->location_type}}"
                data-location_number="{{$dock->location_number}}"
                data-location_status="{{$dock->location_status}}"
            >
                <b>{{$dock->location_number}}</b>
                <br>

                @include('yard.inc.yard_icons')

            </td>
        @endforeach
    </tr>

    <tr height="{{$yard_rowheight}}">
        @foreach($yard_seventh_row as $dock)
            <td align="center" style="vertical-align:middle;" class="
                                                            @if($dock->location_status == 'Empty') bg-primary
                                                            @elseif($dock->location_status == 'Damaged') bg-dark
                                                            @elseif($dock->location_status == 'Equipment') bg-dark
                                                            @elseif($dock->location_status == 'Occupied') bg-danger
                                                            @elseif($dock->location_status == 'Transfer') bg-warning
                                                            @elseif($dock->location_status == 'Complete') bg-success
                                                            @endif LocationDetail" width="{{$yard_cellwidth}}"
                width="{{$yard_cellwidth}}"
                data-location_id = "{{$dock->id}}"
                data-location_type="{{$dock->location_type}}"
                data-location_number="{{$dock->location_number}}"
                data-location_status="{{$dock->location_status}}"
            >
                <b>{{$dock->location_number}}</b>
                <br>

                @include('yard.inc.yard_icons')

            </td>
        @endforeach
    </tr>

</table>
