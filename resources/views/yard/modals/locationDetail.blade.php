<!-- Modal -->
<div class="modal fade" id="location_detail" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header text-white"  style="background-color: #1a32ba;">
                <h5 class="modal-title Location" id="exampleModalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col-md-12">
                    <div class="accordion" id="accordionExample">
                        <div class="card mb-2" id="Ingate">
                            <div class="card-header" id="headingOne">
                                <h2 class="mb-2">
                                    <button style="font-size: 25px;" class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                        <i class="fad fa-arrow-square-right fa-lg"></i> <b>Ingate</b>
                                    </button>
                                </h2>
                            </div>
                            <!-- Ingate Form -->
                            <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                                <form method="POST" action="/yard/ingate">
                                    @csrf
                                    <input type="hidden" name="created_by" value="{{ Auth::user()->id }}">
                                    <input type="hidden" class="location_id" name="location_id">
                                    <input type="hidden" name="current_status" value="On the yard">
                                    <div class="card-body">
                                        <div class="form-row">
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label for="exampleFormControlSelect1"><b>Type</b></label>
                                                    <select class="form-control" id="exampleFormControlSelect1" name="trailer_type" required>
                                                        <option value="">[Select]</option>
                                                        <option value="Running Refer">Running Refer</option>
                                                        <option value="Container">Container</option>
                                                        <option value="Trailer">Trailer</option>
                                                        <option value="Other">Other</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label for="exampleFormControlInput1"><b>Container/Trailer #</b></label>
                                                    <input type="input" name="trailer_number" class="form-control" id="exampleFormControlInput1" placeholder="Container/Trailer" required>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label for="exampleFormControlSelect1"><b>Empty/Loaded</b></label>
                                                    <select class="form-control" id="exampleFormControlSelect1" name="loaded" required>
                                                        <option value="">[Select]</option>
                                                        <option value="Empty">Empty</option>
                                                        <option value="Loaded">Loaded</option>
                                                        <option value="other">Other</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label for="exampleFormControlInput1"><b>Driver Name</b></label>
                                                    <input type="input" class="form-control" id="exampleFormControlInput1" placeholder="Driver Name" name="driver_name" required>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label for="exampleFormControlInput1"><b>Driver Phone #</b></label>
                                                    <input type="input" class="form-control" id="exampleFormControlInput1" placeholder="Driver Number" name="driver_phone" required>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label for="exampleFormControlInput1"><b>PU Number</b></label>
                                                    <input type="input" class="form-control" id="exampleFormControlInput1" placeholder="Pickup Number" name="pickup_number" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label for="exampleFormControlInput1"><b>Carrier</b></label>
                                                    <input type="input" class="form-control" id="exampleFormControlInput1" placeholder="Carrier" name="trailer_owner" required>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="exampleFormControlInput1"><b>Customer</b></label>
                                                    <input type="input" class="form-control" id="exampleFormControlInput1" placeholder="Customer Name" name="customer_name" required>
                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                                <div class="form-group">
                                                    <label for="exampleFormControlSelect1"><b>Open Gate</b></label>
                                                    <select class="form-control" id="exampleFormControlSelect1" name="open_gate">
                                                        <option value="Yes" selected>Yes</option>
                                                        <option value="No">NO</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="col-sm-12">
                                                <div class="RecButton"></div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- Ingate END -->

                            <!-- Transfer Request - START -->
                        </div>
                        <div class="card mb-2" id="RequestTransfer">
                            <div class="card-header" id="headingTwo">
                                <h2 class="mb-0">
                                    <button style="font-size: 25px;" class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        <i class="fad fa-exchange"></i> <b>Request Transfer</b>
                                    </button>
                                </h2>
                            </div>
                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                <div class="card-body">
                                    <form>
                                        @csrf
                                        <input type="hidden" name="created_by" value="{{ Auth::user()->id }}">
                                        <input type="hidden" class="location_id" name="location_id">
                                        <input type="hidden" name="current_status" value="Unloaded">
                                        <input type="hidden" class="trailer_number" name="trailer_number">
                                        <input type="hidden" class="trailer_type" name="trailer_type">
                                        <input type="hidden" name="loaded" value="Unloaded">
                                        <input type="hidden" class="driver_name" name="driver_name">
                                        <input type="hidden" class="driver_phone" name="driver_phone">
                                        <input type="hidden" class="trailer_owner" name="trailer_owner">
                                        <input type="hidden" class="customer_name" name="customer_name">
                                        <input type="hidden" class="pickup_number" name="pickup_number">
                                        <input type="hidden" name="open_gate" value="No">

                                        <div class="form-row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="exampleFormControlSelect1"><b>Select Move:</b></label>
                                                    <select class="form-control" id="exampleFormControlSelect1" name="open_gate">
                                                        <option value="anyDock" selected>Move to any open Dock location.</option>
                                                        <option value="anyYard">Move to any Yard location</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="exampleFormControlSelect1"><b>Top Priority?</b></label>
                                                    <select class="form-control" name="priority" id="exampleFormControlSelect1" required>
                                                        <option selected value="Normal">No</option>
                                                        <option value="High">Yes</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <input type="submit" class="btn btn-primary btn-block" value="Request Transfer">
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                        <!-- Transfer Request - END -->

                        <div class="card mb-2" id="Unloaded">
                            <div class="card-header" id="headingTwo">
                                <h2 class="mb-0">
                                    <button style="font-size: 25px;" class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#unloaded" aria-expanded="false" aria-controls="collapseTwo">
                                        <i class="fad fa-person-dolly-empty"></i> <b>Mark as Unloaded</b>
                                    </button>
                                </h2>
                            </div>
                            <div id="unloaded" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                <div class="card-body">

                                    <form method="post" action="/yard/unloaded">
                                        @csrf
                                        <input type="hidden" name="created_by" value="{{ Auth::user()->id }}">
                                        <input type="hidden" class="location_id" name="location_id">
                                        <input type="hidden" name="current_status" value="Unloaded">
                                        <input type="hidden" class="trailer_number" name="trailer_number">
                                        <input type="hidden" class="trailer_type" name="trailer_type">
                                        <input type="hidden" name="loaded" value="Unloaded">
                                        <input type="hidden" class="driver_name" name="driver_name">
                                        <input type="hidden" class="driver_phone" name="driver_phone">
                                        <input type="hidden" class="trailer_owner" name="trailer_owner">
                                        <input type="hidden" class="customer_name" name="customer_name">
                                        <input type="hidden" class="pickup_number" name="pickup_number">
                                        <input type="hidden" name="open_gate" value="No">

                                        <div class="form-group">
                                            <label for="exampleFormControlSelect1"><b>Mark Container / Trailer as Empty and ready to be pulled away from dock.</b></label>
                                            <select class="form-control" id="exampleFormControlSelect1" required>
                                                <option selected value="">[Select One]</option>
                                                <option value="Yes">Yes</option>
                                            </select>
                                        </div>
                                        <div class="form-row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="exampleFormControlSelect1"><b>Initiate transfer?</b></label>
                                                    <select class="form-control" name="CreateTransfer" id="exampleFormControlSelect1" required>
                                                        <option selected value="No">No</option>
                                                        <option value="Yes">Yes</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="exampleFormControlSelect1"><b>Top Priority?</b></label>
                                                    <select class="form-control" name="priority" id="exampleFormControlSelect1" required>
                                                        <option selected value="Normal">No</option>
                                                        <option value="High">Yes</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <input type="submit" class="btn btn-primary btn-block" value="Mark is Unloaded">
                                    </form>
                                </div>
                            </div>
                        </div>

                        <!-- Outgate START -->
                        <div class="card mb-2" id="Outgate">
                            <div class="card-header" id="headingThree">
                                <h2 class="mb-0">
                                    <button style="font-size: 25px;" class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        <i class="fad fa-arrow-square-left"></i> <b>Outgate</b>
                                    </button>
                                </h2>
                            </div>
                            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                <div class="card-body">
                                    <form method="POST" action="/yard/outgate">
                                        @csrf
                                        <input type="hidden" name="created_by" value="{{ Auth::user()->id }}">
                                        <input type="hidden" class="location_id" name="location_id">
                                        <input type="hidden" name="current_status" value="Off the yard">
                                        <div class="card-body">
                                            <div class="form-row">
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="exampleFormControlSelect1"><b>Type</b></label>
                                                        <select class="form-control" id="exampleFormControlSelect1" name="trailer_type" required>
                                                            <option value="">[Select]</option>
                                                            <option value="Running Refer">Running Refer</option>
                                                            <option value="Container">Container</option>
                                                            <option value="Trailer">Trailer</option>
                                                            <option value="Other">Other</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="exampleFormControlInput1"><b>Container/Trailer #</b></label>
                                                        <input type="input" name="trailer_number" class="form-control trailer_number" id="exampleFormControlInput1" placeholder="Container/Trailer" required>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="exampleFormControlSelect1"><b>Empty/Loaded</b></label>
                                                        <select class="form-control" id="exampleFormControlSelect1" name="loaded" required>
                                                            <option value="">[Select]</option>
                                                            <option value="Empty">Empty</option>
                                                            <option value="Loaded">Loaded</option>
                                                            <option value="other">Other</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="exampleFormControlInput1"><b>Driver Name</b></label>
                                                        <input type="input" class="form-control driver_name" id="exampleFormControlInput1" placeholder="Driver Name" name="driver_name" required>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="exampleFormControlInput1"><b>Driver Phone #</b></label>
                                                        <input type="input" class="form-control driver_phone" id="exampleFormControlInput1" placeholder="Driver Number" name="driver_phone" required>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="exampleFormControlInput1"><b>PU Number</b></label>
                                                        <input type="input" class="form-control" id="exampleFormControlInput1" placeholder="Pickup Number" name="pickup_number" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <div class="form-group">
                                                            <label for="exampleFormControlInput1"><b>Carrier</b></label>
                                                            <input type="input" class="form-control trailer_owner" id="exampleFormControlInput1" placeholder="Carrier" name="trailer_owner" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label for="exampleFormControlInput1"><b>Customer</b></label>
                                                        <input type="input" class="form-control customer_name" id="exampleFormControlInput1" placeholder="Customer Name" name="customer_name" required>
                                                    </div>
                                                </div>
                                                <div class="col-sm-2">
                                                    <div class="form-group">
                                                        <label for="exampleFormControlSelect1"><b>Open Gate</b></label>
                                                        <select class="form-control" id="exampleFormControlSelect1" name="open_gate" required>
                                                            <option value="Yes" selected>Yes</option>
                                                            <option value="No">NO</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="col-sm-12">
                                                    <div class="OGButton"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- Outgate END -->
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
