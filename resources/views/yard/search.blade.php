{{--New file Template--}}

{{--Add Security for this page below--}}


@extends('layouts.app')
{{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')

    @if( Auth::user()->hasRole('Admin') == FALSE)
        @include('layouts.unauthorized')

    @Else


        <div class="card mb-4">
            <div class="card-header text-white" style="background-color: #413c69;">
                <b>Search Yard</b>

                <a href="/yard" class="btn btn-sm btn-secondary float-right">Return to Yard</a>
            </div>
            <div class="card-body">
                <table class="table table-hover table-sm">
                    <thead>
                        <tr>
                            <td><b>Bld</b></td>
                            <td><b>Status</b></td>
                            <td><b>Type</b></td>
                            <td><b>Number</b></td>
                            <td><b>Loaded</b></td>
                            <td><b>Driver</b></td>
                            <td><b>Phone</b></td>
                            <td><b>PU #</b></td>
                            <td><b>Carrier</b></td>
                            <td><b>Customer</b></td>
                            <td><b>Created By</b></td>
                            <td><b>Created At</b></td>
                            <td></td>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($yardSearch as $results)
                        <tr>
                            <td>{{$results->YardSlot->location_bld}}</td>
                            <td>{{$results->current_status}}</td>
                            <td>{{$results->trailer_type}}</td>
                            <td>{{$results->trailer_number}}</td>
                            <td>{{$results->loaded}}</td>
                            <td>{{$results->driver_name}}</td>
                            <td>{{$results->driver_phone}}</td>
                            <td>{{$results->pickup_number}}</td>
                            <td>{{$results->trailer_owner}}</td>
                            <td>{{$results->customer_name}}</td>
                            <td>{{$results->employee->last_name}}, {{$results->employee->first_name}}</td>
                            <td>{{$results->created_at}}</td>
                            <td></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @endif

@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="text/javascript">

        $(document).ready( function () {
            $('#clients').DataTable();
        } );

    </script>

@endsection
