@extends('layouts.app')

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card-deck">
                <div class="card">
                    <div class="card-header text-white" style="height: 56px; background-color: #413c69"><b>Outsource Logistics News</b></div>
                    <div class="card-body" style="height: 225px;">
                        <b>{{\Carbon\Carbon::now()->format('m-d-Y')}}</b> - <i>It is a BEAUTIFUL day at Outsource Logistics!</i>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')



@endsection
