{{--New file Template--}}

{{--Add Security for this page below--}}


@extends('layouts.portal')
{{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')




    <div class="card mb-4">
        <div class="card-header" style="background-color: #7bc26c">
            <b>Create Shipment ({{$item->item}} - {{$item->description}})</b>
            <a href="/home" class="btn btn-secondary btn-sm float-right">Cancel and Return</a>
        </div>
        <div class="card-body">
            <i>*Fields in RED are required.</i>
            <form method="post" action="/portal/ship/new">
                @csrf
                <input type=hidden name="record_type" value="BC">
                <input type="hidden" name="depositor_code" value="{{$item->client}}">
                <input type="hidden" name="item_number" value="{{$item->item}}">
                <input type="hidden" name="uom" value="{{$item->unit}}">
                <input type="hidden" name="warehouse" value="{{$item->warehouse}}">

                <div class="card">
                    <div class="card-header">
                        <b>Basic Information</b>
                    </div>
                    <div class="card-body">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <b>How many of these would you like to ship?  There are <u>{{$item->qty_remain}} {{$item->unit}}(S)</u> in {{$item->warehouse}}. </b>
                            </div>
                            <div class="form-group col-md-6">
                                <input type="number" class="form-control is-invalid" name="qty_ordered" max="{{number_format($item->qty_remain)}}" min="0" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <b>What refrence number would you like to use for this shipment?</b>
                            </div>
                            <div class="form-group col-md-6">
                                <input type="text" class="form-control is-invalid" name="order_ref_number" value="TEST PORTAL DELETE" required>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card mt-3">
                    <div class="card-header">
                        <b>Shipment Dates</b>
                    </div>
                    <div class="card-body">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <b>We have set the order date to today, however if you would like to change it your can do that here.</b>
                            </div>
                            <div class="form-group col-md-6">
                                <input type="date" class="form-control is-invalid" name="order_date"  value="{{\Carbon\Carbon::now()->format('Y-m-d')}}" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <b>When do you want this order to ship?</b>
                            </div>
                            <div class="form-group col-md-6">
                                <input type="date" class="form-control is-invalid" name="ship_date" required>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card mt-3">
                    <div class="card-header">
                        <b>Shipping Address</b>
                    </div>
                    <div class="card-body">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <b>Name</b>
                            </div>
                            <div class="form-group col-md-6">
                                <input type="text" class="form-control is-invalid" name="ship_to_name" placeholder="Receiver's Complete Name" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <b>Address</b>
                            </div>
                            <div class="form-group col-md-6">
                                <input type="text" class="form-control is-invalid" name="ship_to_address" placeholder="Receiver's Address" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <b>Suite / APT</b>
                            </div>
                            <div class="form-group col-md-6">
                                <input type="text" class="form-control" name="ship_to_address_2" placeholder="Receiver's Suite or Apt number">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <b>City</b>
                            </div>
                            <div class="form-group col-md-6">
                                <input type="text" class="form-control is-invalid" name="ship_to_city" placeholder="Receiver's City" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <b>State</b>
                            </div>
                            <div class="form-group col-md-6">
                                <select class="form-control is-invalid" id="validationCustom04" name="ship_to_state" required>
                                    <option selected value="">[Select State]</option>
                                    @foreach($states as $state)
                                        <option value="{{$state->code}}">{{$state->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <b>Zip</b>
                            </div>
                            <div class="form-group col-md-6">
                                <input type="text" class="form-control is-invalid" name="ship_to_zip" placeholder="Receiver's Zip" required>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="card mt-3">
                    <div class="card-header">
                        <b>Schedule Pickup / Delivery</b>
                    </div>
                    <div class="card-body">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <b>When would you like to schedule your pick-up?</b>
                            </div>
                            <div class="form-group col-md-6">
                                <input type="datetime-local" class="form-control is-invalid" name="pickup_date" >
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <b>When would you like this order to be delivered?  This may effect the above pickup-date.</b>
                            </div>
                            <div class="form-group col-md-6">
                                <input type="datetime-local" class="form-control is-invalid" name="delivery_date" >
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <b>What carrier would you like to use?</b>
                            </div>
                            <div class="form-group col-md-6">
                                <select class="form-control is-invalid" id="validationCustom04" name="carrier" required>
                                    <option selected value="">[Select Carrier]</option>
                                        <option value="OSL Trucking">Outsource Trucking</option>
                                    <option value="FedEx">FedEx</option>
                                    <option value="Other">Other (Specify in Notes)</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <b>Please provide us with any special instructions for this shippment if needed.</b>
                            </div>
                            <div class="form-group col-md-6">
                                <textarea class="form-control" name="shipping_notes"></textarea>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card mt-3">
                    <div class="card-header">
                        <b>Complete</b>
                    </div>
                    <div class="card-body">
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <b>Indicate if you are done are have more lines/orders to enter.</b>
                            </div>
                            <div class="form-group col-md-4">
                                <select class="form-control is-invalid" id="validationCustom04" name="complete" required>
                                    <option selected value="">[Select]</option>
                                    <option value="OSL Trucking">I have entered all my lines and orders.  Please submit my shipment.</option>
                                    <option value="FedEx">I have more lines or orders to enter.</option>
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                <input class="btn btn-primary float-right" type="submit" value="Complete Shipment">
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>


@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="text/javascript">

        $(document).ready( function () {
            $('#items').DataTable();
        } );

    </script>

@endsection
