{{--New file Template--}}

{{--Add Security for this page below--}}


@extends('layouts.app')
{{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')



        <div class="card mb-4">
            <div class="card-header text-white" style="background-color: #413c69;">
                <b>{{$expense->trip_title}}</b>
                <div class="btn-group float-right">
                    <a href="/expense" class="btn btn-sm btn-primary float-right d-none d-lg-block"><i class="fad fa-arrow-square-left"></i> Back to Expenses</a>
                    <a href="/expense" class="btn btn-sm btn-primary float-right d-lg-none"><i class="fad fa-backspace"></i></a>
                    <a href="/expense/print/{{$expense->id}}" class="btn btn-sm btn-primary float-right d-none d-lg-block"><i class="fad fa-print"></i> Print Report</a>
                </div>

            </div>
            <div class="card-body">
                @if($expense->items->isEmpty())
                    <div class="alert alert-info text-center"><b>No items yet</b>
                        <a href="#" class="btn btn-sm btn-primary float-right" data-toggle="modal" data-target="#NewItem">Add Expense</a>

                        @if(is_null($expense->complete))
                        <a href="/expense/delete/{{$expense->id}}" class="btn btn-sm btn-danger float-left" >Delete Expense</a>
                        @endif
                    </div>
                @else
                    <table class="table table-sm table-hover table-bordered">
                        <thead>
                            <tr>
                                <td><b>Category</b></td>
                                <td class="d-none d-sm-table-cell"><b>Vendor</b></td>
                                <td class="d-none d-sm-table-cell"><b>Building</b></td>
                                <td align="center"><b>Receipt</b></td>
                                <td align="center" class="d-none d-sm-table-cell"><b>Image</b></td>
                                <td class="d-none d-sm-table-cell"><b>Created At</b></td>
                                <td><b>Amount</b></td>
                                @if(is_null($expense->complete))
                                <td>
                                    <a href="/expense/delete/{{$expense->id}}" class="btn btn-sm btn-danger float-left d-none d-lg-block" >Delete Expense</a>
                                    <a href="#" class="btn btn-sm btn-primary float-right d-none d-lg-block" data-toggle="modal" data-target="#NewItem">Add Expense</a>
                                    <a href="/expense/delete/{{$expense->id}}" class="btn btn-sm btn-danger float-left d-lg-none" ><i class="fad fa-trash"></i></a>
                                    <a href="#" class="btn btn-sm btn-primary float-right d-lg-none" data-toggle="modal" data-target="#NewItem"><i class="fad fa-plus-square"></i></a>
                                </td>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($expense->items as $items)
                            <tr>
                                <td style="vertical-align:middle;">{{$items->category}}</td>
                                <td style="vertical-align:middle;" class="d-none d-sm-table-cell">{{$items->vendor}}</td>
                                <td style="vertical-align:middle;" class="d-none d-sm-table-cell">@if(!is_null($items->Buildings)) {{$items->Buildings->building_name}} @endif</td>
                                <td style="vertical-align:middle;" align="center">
                                    @if(!is_null($items->file_name))
                                        <i class="fad fa-check" style="--fa-primary-color: green; --fa-secondary-color: green; --fa-secondary-opacity: 1.0"></i>
                                    @else
                                        <i class="fad fa-times"  style="--fa-primary-color: red; --fa-secondary-color: red; --fa-secondary-opacity: 1.0"></i>
                                    @endif
                                </td>
                                <td style="vertical-align:middle;" align="center" class="d-none d-sm-table-cell">
                                    @if(!is_null($items->file_name))
                                        <a href="{{asset('storage/receipts/'. $items->new_filename)}}"><img src="{{asset('storage/receipts/'. $items->new_filename)}}" height="150"></a>
                                    @else

                                    @endif
                                </td>
                                <td  style="vertical-align:middle;" class="d-none d-sm-table-cell">{{$items->created_at}}</td>
                                <td style="vertical-align:middle;">${{$items->amount}}</td>
                                @if(is_null($expense->complete))
                                <td style="vertical-align:middle;" align="right"><a href="/expense/delete/item/{{$items->id}}" class="btn btn-sm btn-danger"><i class="fad fa-info-circle" title="Delete Item"></i></a></td>
                                @endif
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <td></td>
                            <td class="d-none d-sm-table-cell"></td>
                            <td class="d-none d-sm-table-cell"></td>
                            <td class="d-none d-sm-table-cell"></td>
                            <td class="d-none d-sm-table-cell"></td>
                            <td align="right"><b>Total:</b></td>
                            <td><b>${{number_format($expense->items->sum('amount'),2)}}</b></td>
                            @if(is_null($expense->complete))
                                <td align="right">
                                    <a href="/expense/complete/{{$expense->id}}" class="btn btn-sm btn-success d-none d-lg-block"><i class="fad fa-check-square "></i> Mark as Complete</a>
                                    <a href="/expense/complete/{{$expense->id}}" class="btn btn-sm btn-success d-lg-none"><i class="fad fa-check-square "></i></a>
                                </td>
                            @endif
                        </tr>
                        </tfoot>

                    </table>
                @endif
            </div>
        </div>

@include('expense.modal.new_item')
@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="text/javascript">

        $(document).ready( function () {
            $('#clients').DataTable();
        } );

    </script>

@endsection
