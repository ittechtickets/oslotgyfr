<!-- Modal Template -->
<div class="modal fade" id="NewItem" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form method="post" action="/expense/addItem" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="employee_id" value="{{Auth::user()->id}}">
                <input type="hidden" name="expense_id" value="{{$expense->id}}">
                <div class="modal-header text-white" style="background-color: #1a32ba;">
                    <h5 class="modal-title" id="exampleModalLongTitle">Create New Item</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="exampleFormControlSelect1"><b>Category</b></label>
                        <select class="form-control" name="category" id="exampleFormControlSelect1" required>
                            <option value="" selected>[Select One]</option>
                            <option value="Mileage">Mileage</option>
                            <option value="Meals">Meals</option>
                            <option value="Lodging">Lodging</option>
                            <option value="Training">Training</option>
                            <option value="Equipment">Equipment</option>
                            <option value="Training">Training</option>
                            <option value="Software">Software</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlSelect1"><b>Building</b></label>
                        <select class="form-control" name="building_number" id="exampleFormControlSelect1">
                            <option value="" selected>[Select One]</option>
                            @foreach($buildings as $building)
                                <option value="{{$building->id}}">{{$building->building_name}}</option>
                            @endforeach
                            <option value="Transportation">Transportation</option>
                        </select>
                        <small id="emailHelp" class="form-text text-muted">Assign this expense to the above building. Not sure leave blank.</small>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlInput1"><b>Vendor</b></label>
                        <input type="text" name="vendor" class="form-control" id="exampleFormControlInput1">
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlInput1"><b>Amount</b></label>
                        <input type="number" name="amount" class="form-control" id="exampleFormControlInput1" step="0.01" min="0.00" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1"><b>Attach Receipt</b></label>
                        <input type="file" class="form-control-file" id="exampleFormControlFile1" name="file_name">
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1"><b>Notes</b></label>
                        <textarea class="form-control" name="notes" id="exampleFormControlTextarea1" rows="3"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
                    <input  type="submit" class="btn btn-sm btn-primary" value="Create Item">
                </div>
            </form>
        </div>
    </div>
</div>
