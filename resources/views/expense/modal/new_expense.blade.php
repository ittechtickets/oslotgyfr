<!-- Modal Template -->
<div class="modal fade" id="NewExpense" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form method="post" action="/expense/addExpense">
                @csrf
                <input type="hidden" name="employee_id" value="{{Auth::user()->id}}">
                <div class="modal-header text-white" style="background-color: #1a32ba;">
                    <h5 class="modal-title" id="exampleModalLongTitle">Create New Expense</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1"><b>Expense Title</b></label>
                        <input type="text" class="form-control" name="trip_title" aria-describedby="emailHelp" placeholder="Enter Title" required>
                        <small id="emailHelp" class="form-text text-muted">A title for this expense. "Trip to Savannah" or "Parts from Walmart"</small>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
                    <input  type="submit" class="btn btn-sm btn-primary" value="Submit Form">
                </div>
            </form>
        </div>
    </div>
</div>
