{{--New file Template--}}

{{--Add Security for this page below--}}


@extends('layouts.dashboard')
{{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')

    @if( Auth::user()->hasRole('Power Bill Dashboard') == FALSE)
        @include('layouts.unauthorized')

    @Else
        <div class="card-deck mt-2">
            <div class="card bg-dark text-white">
                <div class="card-header">
                    <b>Power Cost by Building by Month</b>
                    <div class="btn-group float-right">
                        <a href="#" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#exampleModal"><i class="fad fa-plus"></i> Add Data</a>
                        <a href="#" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#dataView"><i class="fad fa-info-square"></i> View Data</a>
                    </div>
                </div>
                <div class="card-body">
                    <canvas id="line-chart" width="800" height="200"></canvas>
                </div>
            </div>
        </div>
    @endif

@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="text/javascript">

        $(document).ready( function () {
            $('#viewData').DataTable();
        } );

        new Chart(document.getElementById("line-chart"), {
            type: 'line',
            data: {
                labels: [

                    @foreach($lables as $lable)
                    '{{$lable->bill_date->format('F-Y')}}',
                    @endforeach
                ],
                datasets: [{
                    data: [
                        @foreach($useage->where('building','Building 105') as $data)
                        {{$data->bill_cost}},
                        @endforeach
                    ],
                    label: "Building 105",
                    borderColor: "#e67f83",
                    fill: false
                }, {
                    data: [
                        @foreach($useage->where('building','Building 101') as $data)
                        {{$data->bill_cost}},
                        @endforeach
                    ],
                    label: "Building 101",
                    borderColor: "#f0b8b8",
                    fill: false
                }, {
                    data: [
                        @foreach($useage->where('building','Building 23') as $data)
                        {{$data->bill_cost}},
                        @endforeach
                    ],
                    label: "Building 23",
                    borderColor: "#f1f1f1",
                    fill: false
                }, {
                    data: [
                        @foreach($useage->where('building','Building 11') as $data)
                        {{$data->bill_cost}},
                        @endforeach
                    ],
                    label: "Building 11",
                    borderColor: "#bad0af",
                    fill: false
                }, {
                    data: [
                        @foreach($useage->where('building','Building 10') as $data)
                        {{$data->bill_cost}},
                        @endforeach
                    ],
                    label: "Building 10",
                    borderColor: "#83af70",
                    fill: false
                },
                    {
                        data: [
                            @foreach($useage->where('building','Building 16') as $data)
                            {{$data->bill_cost}},
                            @endforeach
                        ],
                        label: "Building 16",
                        borderColor: "#488f31",
                        fill: false
                    }
                ]
            },
            options: {
                title: {
                    display: false,
                    text: 'Power Bills By Month'
                }
            }
        });
        //Global Chart Color
        Chart.defaults.global.defaultFontColor = "#fff";
    </script>

@endsection

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="POST" action="/powerbill/add">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><b>Enter Data</b></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="exampleFormControlSelect1"><b>Building</b></label>
                        <select class="form-control" name="building" id="exampleFormControlSelect1" required>
                            <option selected value="">[Select Building]</option>
                            @foreach($loc as $bld)
                                <option value="{{$bld->building}}">{{$bld->building}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlSelect1"><b>Month</b></label>
                        <select class="form-control" name="month" id="exampleFormControlSelect1" required>
                            <option value="">[Select Month]</option>
                            <option value="1">January</option>
                            <option value="2">February</option>
                            <option value="3">March</option>
                            <option value="4">April</option>
                            <option value="5">May</option>
                            <option value="6">June</option>
                            <option value="7">July</option>
                            <option value="8">August</option>
                            <option value="9">September</option>
                            <option value="10">October</option>
                            <option value="11">November</option>
                            <option value="12">December</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlInput1"><b>Year</b></label>
                        <input type="text" class="form-control" name="year" maxlength="4" id="exampleFormControlInput1" value="{{\Carbon\Carbon::now()->format('Y')}}" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlInput1"><b>Cost</b></label>
                        <input type="text" class="form-control" name="bill_cost" id="exampleFormControlInput1" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <input type="submit" value="Add Data" class="btn btn-primary">
                </div>
            </form>
        </div>
    </div>
</div>



<!-- Data View -->
<div class="modal fade" id="dataView" tabindex="-1" aria-labelledby="dataView2" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-warning">
                <h5 class="modal-title" id="exampleModalLabel">Data View</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-sm table-striped" id="viewData" width="100%">
                    <thead>
                        <tr>
                            <td><b>Building</b></td>
                            <td><b>Bill Date</b></td>
                            <td><b>Cost</b></td>
                            <td><b>Entered</b></td>
                            <td></td>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($useage as $use)
                        <tr>
                            <td>{{$use->building}}</td>
                            <td>{{$use->bill_date->format('m-d-Y')}}</td>
                            <td>${{$use->bill_cost}}</td>
                            <td>{{$use->created_at}}</td>
                            <td align="center"><a href="powerbill/delete/{{$use->id}}" class="btn btn-sm btn-danger"><i class="fad fa-trash" title="Delete Entry"></i></a> </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
