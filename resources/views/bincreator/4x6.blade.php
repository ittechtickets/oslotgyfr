{{--New file Template--}}

{{--Add Security for this page below--}}


@extends('layouts.print')
{{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')

    @if( Auth::user()->hasRole('Admin') == FALSE)
        @include('layouts.unauthorized')

    @Else
        <style>
            .table {
                display: table;
                width: 100%;
            }
            .tr {
                display: table-row;
            }
            .td {
                display: table-cell;
                text-align: center;
            }

            @media print {
                .pagebreak { page-break-after: always; } /* page-break-after works, as well */
            }
        </style>


        @for($i = $start_number; $i <= $end_number; $i++)

            <div class="table">
                <div class="tr">
                    <div class="td" style="vertical-align: middle; text-align: center">{!! QrCode::size(1500)->generate($preBin . sprintf("%0". $end_number_length."d", $i)); !!}</div>
                </div>
                <div class="tr">
                    <div class="td" style="font-weight: bold; font-size: 300px; vertical-align: middle;">{{$preBin . sprintf("%0". $end_number_length."d", $i)}}</div>
                </div>
            </div>
            <div class="pagebreak"> </div>
        @endfor

    @endif

@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="text/javascript">

        $(document).ready( function () {
            $('#clients').DataTable();
        } );

    </script>

@endsection
