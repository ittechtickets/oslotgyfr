{{--New file Template--}}

{{--Add Security for this page below--}}


@extends('layouts.app')
{{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')

    @if( Auth::user()->hasRole('Admin') == FALSE)
        @include('layouts.unauthorized')

    @Else


        <div class="card mb-4">
            <div class="card-header text-white" style="background-color: #413c69;">
                <b>Bins</b>
                <div class="btn-group float-right">
                    <a href="/bincreator" class="btn btn-sm btn-primary">New List</a>
                    <form action="/bincreator/rack3x2" method="POST" target="_blank" >
                        @csrf
                        <input type="hidden" name="delimiter" value="{{$delimiter}}">
                        <input type="hidden" name="row" value="{{$row}}">
                        <input type="hidden" name="start_location" value="{{$start_location}}">
                        <input type="hidden" name="end_location" value="{{$end_location}}">
                        <input type="hidden" name="sub_location" value="{{$sub_location}}">
                        <input type="submit" name="RackLabelSize" value="3x2" class="btn btn-sm btn-primary">
                    </form>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-sm">
                    <thead>
                        <tr>
                            <td><b>Bin Number</b></td>
                            <td><b>Length</b></td>
                            <td><b>Pass</b></td>
                            <td><b>Human Read</b></td>
                            <td><b>Barcode Read</b></td>
                            <td><b>Code</b></td>
                        </tr>
                    </thead>
                    <tbody>
                    @for ($i = $start_location; $i <= $end_location; $i++)
                        <tr>
                            <td>{{$preBin . sprintf("%0". $end_location_length."d", $i) . $delimiter . $sub_location}}</td>
                            <td>{{strlen($preBin . sprintf("%0". $end_location_length."d", $i) . $delimiter . $sub_location)}}</td>
                            <td>
                                @if(strlen($preBin . sprintf("%0". $end_location_length."d", $i) . $delimiter . $sub_location) <= 10)
                                    <i class="far fa-check" style="color: green;"></i>
                                @else
                                    <i class="fad fa-times-circle" style="color: red;"></i> FAIL - Too Long!
                                @endif
                            </td>
                            <td>{{$preBin . sprintf("%0". $end_location_length."d", $i) . $delimiter . $sub_location}}</td>
                            <td>{{$preBin . sprintf("%0". $end_location_length."d", $i) . $delimiter . $sub_location}}</td>
                            <td>{!! QrCode::size(50)->generate($preBin . sprintf("%0". $end_location_length."d", $i) . $delimiter . $sub_location); !!}
                            </td>
                        </tr>
                    @endfor
                    </tbody>
                </table>
            </div>
        </div>
    @endif

@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="text/javascript">

        $(document).ready( function () {
            $('#clients').DataTable();
        } );

    </script>

@endsection