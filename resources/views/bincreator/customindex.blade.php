@extends('layouts.app')

@section('content')

    @if( Auth::user()->hasRole('Admin') == FALSE)
        @include('layouts.unauthorized')

    @Else


        <div class="card mb-4">
            <div class="card-header text-white" style="background-color: #413c69;">
                <b>Rack Bin Creator</b>
            </div>
            <div class="card-body">
                <form method="post" action="/bincreator/custompost">
                    @csrf
                    <ul class="list-group">
                        <li class="list-group-item">Total length for bin locations is 10 characters.  This includes dashes.</li>
                        <li class="list-group-item">A delimiter is required as this number will be printed on the pick sheets.</li>
                    </ul>
                    <div class="text-center" style="font-size: 25px; font-weight: bold;">
                        You can use any bin location style here.
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1"><b>custom</b></label>
                        <input type="text" name="custom" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="bin" required>
                        <small id="emailHelp" class="form-text text-muted">Input bin location here.</small>
                    </div>
                    <input type="submit" class="btn btn-sm btn-primary">
                </form>
            </div>
        </div>
    @endif

@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="text/javascript">

        $(document).ready( function () {
            $('#clients').DataTable();
        } );

    </script>

@endsection