
<style>


    @media print {
        body {margin-top: 2mm; margin-bottom: 2mm;
            margin-left: 3mm; margin-right: 2mm}

        @page {
            margin-top: 1cm;
            margin-bottom: 1cm;
        }
    }

</style>

{!! QrCode::size(68)->generate('https://oslotgyfr.com/asset/'. $asset->id); !!}
<br>
<span style="font-size: 25px; font-weight: bold; font-family: Arial">{{sprintf("%05d", $asset->id)}}</span>


