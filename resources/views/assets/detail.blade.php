{{--New file Template--}}

{{--Add Security for this page below--}}


@extends('layouts.app')
{{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')

    @if( Auth::user()->hasRole('Admin') == FALSE)
        @include('layouts.unauthorized')

    @Else


        <div class="card mb-4">
            <div class="card-header text-white" style="background-color: #413c69;">
                <b>Asset Control - Detail</b>

                <div class="btn-group float-right" role="group" aria-label="Basic example">
                    <a href="/asset" class="btn btn-sm btn-primary"><i class="fad fa-caret-circle-left"></i> Back to Assets</a>
                    <a href="/asset/edit/{{$asset->id}}" class="btn btn-sm btn-primary "><i class="fad fa-edit"></i> Edit</a>
                    <a href="/asset/label/{{$asset->id}}" target="_blank" class="btn btn-sm btn-primary"><i class="fad fa-print"></i> Label</a>
                </div>

            </div>
            <div class="card-body">
                <! --Main Card Start-->






                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#purchase" role="tab" aria-controls="purchase" aria-selected="false">Purchase</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="contact-tab" data-toggle="tab" href="#technical" role="tab" aria-controls="technical" aria-selected="false">Technical</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="contact-tab" data-toggle="tab" href="#inventory" role="tab" aria-controls="inventory" aria-selected="false">Inventory</a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <div class="row mt-2">
                            <div class="col-md-2 mb-2">
                                <div class="card">
                                    <div class="card-header"  style="background-color: #a7c5eb;">
                                        <b>QR Code</b>
                                    </div>
                                    <div class="card-body">
                                        {!! QrCode::size(68)->generate('https://oslotgyfr.com/asset/'. $asset->id); !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8 mb-2">
                                <div class="card">
                                    <div class="card-header" style="background-color: #a7c5eb;">
                                        <b>Notes</b>
                                    </div>
                                    <div class="card-body">
                                        {{$asset->notes}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 mb-2">
                                <div class="card">
                                    <div class="card-header" style="background-color: #a7c5eb;">
                                        <b>Issued TO</b>
                                    </div>
                                    <div class="card-body">
                                        {{$asset->assigned_to_name}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-2 mb-2">
                            <div class="col-md-2 mb-2">
                                <div class="card">
                                    <div class="card-header" style="background-color: #a7c5eb;">
                                        <b>Name</b>
                                    </div>
                                    <div class="card-body">
                                        {{$asset->name}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 mb-2">
                                <div class="card">
                                    <div class="card-header" style="background-color: #a7c5eb;">
                                        <b>Description</b>
                                    </div>
                                    <div class="card-body">
                                        {{$asset->description}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 mb-2">
                                <div class="card">
                                    <div class="card-header" style="background-color: #a7c5eb;">
                                        <b>Department</b>
                                    </div>
                                    <div class="card-body">
                                        {{$asset->department}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 mb-2">
                                <div class="card">
                                    <div class="card-header" style="background-color: #a7c5eb;">
                                        <b>Building</b>
                                    </div>
                                    <div class="card-body">
                                        {{$asset->building}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 mb-2">
                                <div class="card">
                                    <div class="card-header" style="background-color: #a7c5eb;">
                                        <b>Location</b>
                                    </div>
                                    <div class="card-body">
                                        {{$asset->location}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="purchase" role="tabpanel" aria-labelledby="profile-tab">
                        <div class="row mt-2">
                            <div class="col-md-2 mb-2">
                                <div class="card">
                                    <div class="card-header" style="background-color: #a7c5eb;">
                                        <b>Purchase Date</b>
                                    </div>
                                    <div class="card-body">
                                        @if(!is_null($asset->purchase_date))
                                            {{$asset->purchase_date->format('m-d-Y')}}
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 mb-2">
                                <div class="card">
                                    <div class="card-header" style="background-color: #a7c5eb;">
                                        <b>Purchase Price</b>
                                    </div>
                                    <div class="card-body">
                                        ${{number_format($asset->purchase_price, 2)}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 mb-2">
                                <div class="card">
                                    <div class="card-header" style="background-color: #a7c5eb;">
                                        <b>Unit Price</b>
                                    </div>
                                    <div class="card-body">
                                        ${{number_format($asset->unit_price, 2)}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 mb-2">
                                <div class="card">
                                    <div class="card-header" style="background-color: #a7c5eb;">
                                        <b>Qty</b>
                                    </div>
                                    <div class="card-body">
                                        {{$asset->unit_qty}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 mb-2">
                                <div class="card">
                                    <div class="card-header" style="background-color: #a7c5eb;">
                                        <b>Extended Price</b>
                                    </div>
                                    <div class="card-body">
                                        ${{number_format($asset->unit_price * $asset->unit_qty, 2)}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 mb-2">
                                <div class="card">
                                    <div class="card-header" style="background-color: #a7c5eb;">
                                        <b>Condition</b>
                                    </div>
                                    <div class="card-body">
                                        {{$asset->condition}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row ">
                            <div class="col-md-2 mb-2">
                                <div class="card">
                                    <div class="card-header" style="background-color: #a7c5eb;">
                                        <b>Vendor</b>
                                    </div>
                                    <div class="card-body">
                                        {{$asset->vendor}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 mb-2">
                                <div class="card">
                                    <div class="card-header" style="background-color: #a7c5eb;">
                                        <b>Model Number</b>
                                    </div>
                                    <div class="card-body">
                                        {{$asset->model_number}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 mb-2">
                                <div class="card">
                                    <div class="card-header" style="background-color: #a7c5eb;">
                                        <b>Serial Number</b>
                                    </div>
                                    <div class="card-body">
                                        {{$asset->serial_number}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 mb-2">
                                <div class="card">
                                    <div class="card-header" style="background-color: #a7c5eb;">
                                        <b>Service Tag</b>
                                    </div>
                                    <div class="card-body">
                                        {{$asset->service_tag}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 mb-2">
                                <div class="card">
                                    <div class="card-header" style="background-color: #a7c5eb;">
                                        <b>Category</b>
                                    </div>
                                    <div class="card-body">
                                        {{$asset->category}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 mb-2">
                                <div class="card">
                                    <div class="card-header" style="background-color: #a7c5eb;">
                                        <b>Version</b>
                                    </div>
                                    <div class="card-body">
                                        {{$asset->version}}
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="tab-pane fade" id="technical" role="tabpanel" aria-labelledby="contact-tab">
                        <div class="row mt-2 ">
                            <div class="col-md-2 mb-2">
                                <div class="card">
                                    <div class="card-header" style="background-color: #a7c5eb;">
                                        <b>MAC Address</b>
                                    </div>
                                    <div class="card-body">
                                        {{$asset->mac_address}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 mb-2">
                                <div class="card">
                                    <div class="card-header" style="background-color: #a7c5eb;">
                                        <b>Static IP</b>
                                    </div>
                                    <div class="card-body">
                                        {{$asset->static_ip}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 mb-2">
                                <div class="card">
                                    <div class="card-header" style="background-color: #a7c5eb;">
                                        <b>Computer Name</b>
                                    </div>
                                    <div class="card-body">
                                        {{$asset->computer_name}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 mb-2">
                                <div class="card">
                                    <div class="card-header" style="background-color: #a7c5eb;">
                                        <b>GeoTab ID</b>
                                    </div>
                                    <div class="card-body">
                                        {{$asset->geo_tab_id}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 mb-2">
                                <div class="card">
                                    <div class="card-header" style="background-color: #a7c5eb;">
                                        <b>Phone Number</b>
                                    </div>
                                    <div class="card-body">
                                        {{$asset->phone_number}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 mb-2">
                                <div class="card">
                                    <div class="card-header" style="background-color: #a7c5eb;">
                                        <b>IMEI</b>
                                    </div>
                                    <div class="card-body">
                                        {{$asset->imei}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="inventory" role="tabpanel" aria-labelledby="contact-tab">

                    </div>
                </div>


                <! --Main Card END-->
            </div>
        </div>
    @endif

@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="text/javascript">

        $(document).ready( function () {
            $('#clients').DataTable();
        } );

    </script>

@endsection
