
<style>
    .center {
        display: block;
        margin-left: auto;
        margin-right: auto;
        width: 50%;
    }
</style>

<body>

<table width="100%" border="1" cellpadding="0" cellspacing="0">
    <tr>
        <td align="center" colspan="2">
            <span style="font-size: 75px; font-weight: bold">{{strtoupper($barcode_right)}}</span>
        </td>
    </tr>
    <tr>
        <td align="center" width="50%">
            <span style="font-size: 250px; font-family: 'BC C39 3 to 1 HD Medium'">*{{strtoupper($barcode_right)}}*</span>
        </td>
        <td align="center">
            <span style="font-size: 250px; font-weight: bold;">&rarr;</span>
        </td>
    </tr>
</table>


<table height="200" width="100%">
    <tr>
        <td align="center"><img src="/img/Outsource Logistics_Transparent_300.png"> </td>
    </tr>
</table>

<table width="100%" border="1" cellpadding="0" cellspacing="0">
    <tr>
        <td align="center" colspan="2">
            <span style="font-size: 75px; font-weight: bold">{{strtoupper($barcode_left)}}</span>
        </td>
    </tr>
    <tr>
        <td align="center" width="50%">
            <span style="font-size: 250px; font-weight: bold;">&larr;</span>
        </td>
        <td align="center">
            <span style="font-size: 250px; font-family: 'BC C39 3 to 1 HD Medium'">*{{strtoupper($barcode_left)}}*</span>
        </td>
    </tr>
</table>

</body>

