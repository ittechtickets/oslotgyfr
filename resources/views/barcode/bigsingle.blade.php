{{--New file Template--}}

{{--Add Security for this page below--}}


@extends('layouts.app')
{{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')

    @if( Auth::user()->hasRole('Bin Transfers') == FALSE)
        @include('layouts.unauthorized')

    @Else


        <div class="card mb-4">
            <div class="card-header text-white" style="background-color: #413c69;">
                <b>Enter Data For Big Single Barcode</b>
            </div>
            <form method="post" action="/barcode/print" target="_blank">
                @csrf
                <div class="card-body">
                    <input type="text" class="form-control" name="barcode" required>
                    <div class="card-deck mt-3">
                        <div class="card">
                            <input type="submit" value="Print" class="btn btn-info">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    @endif

@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="text/javascript">

        $(document).ready( function () {
            $('#clients').DataTable();
        } );

    </script>

@endsection
