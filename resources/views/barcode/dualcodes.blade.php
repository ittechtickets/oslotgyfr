{{--New file Template--}}

{{--Add Security for this page below--}}


@extends('layouts.app')
{{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')

    @if( Auth::user()->hasRole('Bin Transfers') == FALSE)
        @include('layouts.unauthorized')

    @Else


        <div class="card mb-4">
            <div class="card-header text-white" style="background-color: #413c69;">
                <b>Enter Data For Dual Code Barcode</b>
            </div>
            <form method="post" action="/barcode/print2" target="_blank">
                @csrf
                <div class="card-body">
                    <div class="card-deck mt-3">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group">
                                    <label><b>Left Side</b></label>
                                    <input type="text" class="form-control" name="barcode_left" required>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group">
                                    <label><b>Right Side</b></label>
                                    <input type="text" class="form-control" name="barcode_right" required>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1"><b>Row Side</b></label>
                                    <select class="form-control" name="rowside" id="exampleFormControlSelect1" required>
                                        <option value="">[Select]</option>
                                        <option value="Right">Right</option>
                                        <option value="Left">Left</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="submit" value="Print" class="btn btn-block btn-info mt-3">
                </div>
            </form>
        </div>
    @endif

@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="text/javascript">

        $(document).ready( function () {
            $('#clients').DataTable();
        } );

    </script>

@endsection

