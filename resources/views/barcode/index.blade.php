{{--New file Template--}}

{{--Add Security for this page below--}}


@extends('layouts.app')
{{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')

    @if( Auth::user()->hasRole('Bin Transfers') == FALSE)
        @include('layouts.unauthorized')

    @Else


        <div class="card mb-4">
            <div class="card-header text-white" style="background-color: #413c69;">
                <b>Select Barcode Format</b>
            </div>
            <div class="card-body">
                <div class="card-deck">
                    <div class="card">
                        <div class="card-body">
                            <b><a href="/barcode/bigsingle">Big Single</a> </b>
                            <a href="/barcode/bigsingle">
                            <img src="img/single_barcode.png">
                            </a>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <b><a href="/barcode/dualcodes">Double Code</a> </b>
                            <a href="/barcode/dualcodes">
                            <img src="img/double_barcode.png"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif

@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="text/javascript">

        $(document).ready( function () {
            $('#clients').DataTable();
        } );

    </script>

@endsection
