{{--New file Template--}}

{{--Add Security for this page below--}}


@extends('layouts.dashboard')
{{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')




        <div class="text-white text-center">
            <img src="{{URL::asset('img/Cat-icon.png')}}" class="" style="display: block; margin-left: auto; margin-right: auto;">
            <h2>Looks like there is no data for this dashboard.... Yet</h2>
            <h4>Let me go to the back and see if I can find some..</h4>
        </div>


@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="text/javascript">

        $(document).ready( function () {
            $('#clients').DataTable();
        } );

    </script>

@endsection
