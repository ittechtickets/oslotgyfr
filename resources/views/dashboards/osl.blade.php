{{--New file Template--}}

{{--Add Security for this page below--}}


@extends('layouts.dashboard')
{{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')

    <style>
        .myAlert-bottom{
            position: fixed;
            bottom: 5px;
            left:2%;
            width: 96%;
        }

        #QrCode {
            position: fixed;
            bottom: 5px;
            left: 10px;
            opacity: 1;
            filter: alpha(opacity=50); /* For IE8 and earlier */
        }
    </style>

        <meta http-equiv="refresh" content="300">

        <nav class="navbar navbar-dark bg-dark mt-1">
            <a class="navbar-brand" href="{{ url('/') }}">
                <img src="../img/logo-icon-white.png" height="25">
            </a>
            <a class="navbar-brand" href="#"><b>Camelot - Dashboard</b></a>

            <span class="navbar-text">
      <i>Last Updated: {{$dashboard->last()->created_at->format('m-d-Y H:i')}}</i>
    </span>
        </nav>

        <div class="row mt-3">
            <div class="@if($timeCustomer->isEmpty()) col-md-8 @else col-md-2 @endif">
                <div class="card bg-dark text-white h-100">
                    <div class="card-header">
                        <b>End of the Month</b>
                    </div>
                    <div class="card-body">
                        <h1>
                            @if(Carbon\Carbon::parse(\Carbon\Carbon::now())->endOfMonth()->diffInDays() == 1)
                                <div style="color:red">Tomorrow</div>
                            @elseif(Carbon\Carbon::parse(\Carbon\Carbon::now())->endOfMonth()->diffInDays() == 0)
                                <div style="color:red">Today</div>
                            @elseif(Carbon\Carbon::parse(\Carbon\Carbon::now())->endOfMonth()->diffInDays() >= 5)
                                <div style="color:white">{{Carbon\Carbon::parse(\Carbon\Carbon::now())->endOfMonth()->diffInDays()}} Days</div>
                            @elseif(Carbon\Carbon::parse(\Carbon\Carbon::now())->endOfMonth()->diffInDays() < 5)
                                <div style="color:yellow">{{Carbon\Carbon::parse(\Carbon\Carbon::now())->endOfMonth()->diffInDays()}} Days</div>
                            @endif
                        </h1>
                    </div>
                </div>
            </div>
            @if(!$timeCustomer->isEmpty())
                <div class="col-md-6">
                    <div class="card bg-danger text-white">
                        <div class="card-header">
                            <b>Time Sensitive Transactions</b> ({{$timeCustomer->count()}})
                        </div>
                        <div class="card-body" style="height: 116px; overflow: auto;">
                            <table class="table table-sm">
                                <tbody>
                                @foreach($timeCustomer as $customer)
                                    <tr>
                                        <td>{{$customer->document}}</td>
                                        <td>{{$customer->client}}</td>
                                        <td>{{$customer->header_reference}}</td>
                                        <td>{{$customer->user_id}}</td>
                                        <td align="right"><b>Time Sensitive Customer</b></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            @endif
            <div class="col-md-2">
                <div class="card bg-dark text-white">
                    <div class="card-header">
                        <b>Shipments with Documents</b>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-4">
                                <h2>{{$dashboard->last()->shipments_docs}}</h2>
                            </div>
                            <div class="col-8">
                                <canvas id="shipments_docs" width="100%" height=""></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="card bg-dark text-white">
                    <div class="card-header">
                        <b>Receipts with Documents</b>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-4">
                                <h2>{{$dashboard->last()->receipts_docs}}</h2>
                            </div>
                            <div class="col-8">
                                <canvas id="receipts_docs" width="100%"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="card-deck mb-1 mt-3">
            <div class="card bg-dark text-white">
                <div class="card-header">
                    <b>Shipments Pending</b>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-4">
                            <h2>{{$dashboard->last()->ShipmentsPending}}</h2>
                        </div>
                        <div class="col-8">
                            <canvas id="shipments_pending" width="100%"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card bg-dark text-white">
                <div class="card-header">
                    <b>Shipments Today</b>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-4">
                            <h2>{{$dashboard->last()->ShipmentsToday}}</h2>
                        </div>
                        <div class="col-8">
                            <canvas id="shipments_today" width="100%"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card bg-dark text-white">
                <div class="card-header">
                    <b>Shipments Overdue</b>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-4">
                            <h2>{{$dashboard->last()->ShipmentsOverdue}}</h2>
                        </div>
                        <div class="col-8">
                            <canvas id="shipments_overdue" width="100%"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card bg-dark text-white">
                <div class="card-header">
                    <b>Receipts Pending</b>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-4">
                            <h2>{{$dashboard->last()->ReceiptQty}}</h2>
                        </div>
                        <div class="col-8">
                            <canvas id="receipts_pending" width="100%"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card bg-dark text-white">
                <div class="card-header">
                    <b>Receipts Today</b>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-4">
                            <h2>{{$dashboard->last()->ReceiptsToday}}</h2>
                        </div>
                        <div class="col-8">
                            <canvas id="receipts_today" width="100%"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card bg-dark text-white">
                <div class="card-header">
                    <b>Receipts Overdue</b>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-4">
                            <h2>{{$dashboard->last()->ReceiptsOverdue}}</h2>
                        </div>
                        <div class="col-8">
                            <canvas id="receipts_overdue" width="100%"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card bg-dark text-white mt-3">
            <div class="card-header">
                <b>Bin Usage by Location</b>
            </div>
            <div class="card-body">
                <div class="card-deck">
                    <div class="card bg-dark text-white">
                        <div class="card-body">
                            <canvas id="savannah_bin" width="100%"></canvas>
                        </div>
                    </div>
                    <div class="card bg-dark text-white">
                        <div class="card-body">
                            <canvas id="valdosta_bin" width="100%"></canvas>
                        </div>
                    </div>
                    <div class="card bg-dark text-white">
                        <div class="card-body">
                            <canvas id="tifton_bin" width="100%"></canvas>
                        </div>
                    </div>
                    <div class="card bg-dark text-white">
                        <div class="card-body">
                            <canvas id="clay_bin" width="100%"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    @if(!$execError->isEmpty())
        <div class="card bg-danger text-white mt-3 col-8 offset-2">
            <div class="card-body">
                <table class="table table-sm">
                    <tbody>
                    @foreach($execError as $error)
                        <tr>
                            <td>{{$error->Customer}}</td>
                            <td>{{$error->{'Exception Description'} }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @endif


    <div class="d-none d-lg-block" id="QrCode">{!! QrCode::size(90)->generate('https://oslotgyfr.com/dashboard/osl'); !!}</div>

@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="text/javascript">


        new Chart(document.getElementById("shipments_pending"), {
            type: 'line',
            data: {
                labels: [
                    @foreach($dashboard as $data)
                    '{{$data->created_at->format('h:i')}}',
                    @endforeach

                ],
                datasets: [{
                    data: [
                        @foreach($dashboard as $data)
                        {{$data->ShipmentsPending}},
                        @endforeach
                    ],
                    label: "Shipments",
                    borderColor: "#3e95cd",
                    fill: true,
                    borderWidth: 1,
                    backgroundColor: "rgba(110, 110, 245, 0.1)",
                    pointRadius: 0
                }
                ]
            },
            options: {
                title: {
                    display: false,
                    text: ''
                },
                legend: {
                    display: false
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true,
                            display: false,
                            fontColor: '#0efa0a'
                        },
                        gridLines: {
                            display: false
                        }
                    }],
                    xAxes: [{
                        ticks: {
                            display: false,
                            fontColor: 'green'
                        },
                        gridLines: {
                            display: false
                        }
                    }]
                }
            }
        });


        new Chart(document.getElementById("shipments_today"), {
            type: 'line',
            data: {
                labels: [
                    @foreach($dashboard as $data)
                        '{{$data->created_at->format('h:i')}}',
                    @endforeach

                ],
                datasets: [{
                    data: [
                        @foreach($dashboard as $data)
                        {{$data->ShipmentsToday}},
                        @endforeach
                    ],
                    label: "Shipments",
                    borderColor: "#3e95cd",
                    fill: true,
                    borderWidth: 1,
                    backgroundColor: "rgba(110, 110, 245, 0.1)",
                    pointRadius: 0
                }
                ]
            },
            options: {
                title: {
                    display: false,
                    text: ''
                },
                legend: {
                    display: false
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true,
                            display: false,
                            fontColor: '#ffdef3'
                        },
                        gridLines: {
                            display: false
                        }
                    }],
                    xAxes: [{
                        ticks: {
                            display: false,
                            fontColor: 'green'
                        },
                        gridLines: {
                            display: false
                        }
                    }]
                }
            }
        });

        new Chart(document.getElementById("shipments_overdue"), {
            type: 'line',
            data: {
                labels: [
                    @foreach($dashboard as $data)
                        '{{$data->created_at->format('h:i')}}',
                    @endforeach

                ],
                datasets: [{
                    data: [
                        @foreach($dashboard as $data)
                        {{$data->ShipmentsOverdue}},
                        @endforeach
                    ],
                    label: "Shipments",
                    borderColor: "#3e95cd",
                    fill: true,
                    borderWidth: 1,
                    backgroundColor: "rgba(110, 110, 245, 0.1)",
                    pointRadius: 0
                }
                ]
            },
            options: {
                title: {
                    display: false,
                    text: ''
                },
                legend: {
                    display: false
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true,
                            display: false,
                            fontColor: '#3b27ba'
                        },
                        gridLines: {
                            display: false
                        }
                    }],
                    xAxes: [{
                        ticks: {
                            display: false,
                            fontColor: 'green'
                        },
                        gridLines: {
                            display: false
                        }
                    }]
                }
            }
        });



        new Chart(document.getElementById("receipts_pending"), {
            type: 'line',
            data: {
                labels: [
                    @foreach($dashboard as $data)
                        '{{$data->created_at->format('h:i')}}',
                    @endforeach

                ],
                datasets: [{
                    data: [
                        @foreach($dashboard as $data)
                        {{$data->ReceiptQty}},
                        @endforeach
                    ],
                    label: "Receipt",
                    borderColor: "#3e95cd",
                    fill: true,
                    borderWidth: 1,
                    backgroundColor: "rgba(110, 110, 245, 0.1)",
                    pointRadius: 0
                }
                ]
            },
            options: {
                title: {
                    display: false,
                    text: ''
                },
                legend: {
                    display: false
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true,
                            display: false,
                            fontColor: '#e847ae'
                        },
                        gridLines: {
                            display: false
                        }
                    }],
                    xAxes: [{
                        ticks: {
                            display: false,
                            fontColor: 'green'
                        },
                        gridLines: {
                            display: false
                        }
                    }]
                }
            }
        });


        new Chart(document.getElementById("receipts_today"), {
            type: 'line',
            data: {
                labels: [
                    @foreach($dashboard as $data)
                        '{{$data->created_at->format('h:i')}}',
                    @endforeach

                ],
                datasets: [{
                    data: [
                        @foreach($dashboard as $data)
                        {{$data->ReceiptsToday}},
                        @endforeach
                    ],
                    label: "Receipt",
                    borderColor: "#3e95cd",
                    fill: true,
                    borderWidth: 1,
                    backgroundColor: "rgba(110, 110, 245, 0.1)",
                    pointRadius: 0
                }
                ]
            },
            options: {
                title: {
                    display: false,
                    text: ''
                },
                legend: {
                    display: false
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true,
                            display: false,
                            fontColor: '#13ca91'
                        },
                        gridLines: {
                            display: false
                        }
                    }],
                    xAxes: [{
                        ticks: {
                            display: false,
                            fontColor: 'green'
                        },
                        gridLines: {
                            display: false
                        }
                    }]
                }
            }
        });


        new Chart(document.getElementById("receipts_overdue"), {
            type: 'line',
            data: {
                labels: [
                    @foreach($dashboard as $data)
                        '{{$data->created_at->format('h:i')}}',
                    @endforeach

                ],
                datasets: [{
                    data: [
                        @foreach($dashboard as $data)
                        {{$data->ReceiptsOverdue}},
                        @endforeach
                    ],
                    label: "Receipt",
                    borderColor: "#3e95cd",
                    fill: true,
                    borderWidth: 1,
                    backgroundColor: "rgba(110, 110, 245, 0.1)",
                    pointRadius: 0
                }
                ]
            },
            options: {
                title: {
                    display: false,
                    text: ''
                },
                legend: {
                    display: false
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true,
                            display: false,
                            fontColor: '#ff9472'
                        },
                        gridLines: {
                            display: false
                        }
                    }],
                    xAxes: [{
                        ticks: {
                            display: false,
                            fontColor: 'green'
                        },
                        gridLines: {
                            display: false
                        }
                    }]
                }
            }
        });



        new Chart(document.getElementById("receipts_docs"), {
            type: 'line',
            data: {
                labels: [
                    @foreach($dashboard as $data)
                        '{{$data->created_at->format('h:i')}}',
                    @endforeach

                ],
                datasets: [{
                    data: [
                        @foreach($dashboard as $data)
                        {{$data->receipts_docs}},
                        @endforeach
                    ],
                    label: "Receipt",

                    @if($dashboard->last()->receipts_docs >= 80)
                    borderColor: "#cd3e5d",
                    @else
                    borderColor: "#3e95cd",
                    @endif

                    fill: true,
                    borderWidth: 1,
                    backgroundColor: "rgba(110, 110, 245, 0.1)",
                    pointRadius: 0
                }
                ]
            },
            options: {
                title: {
                    display: false,
                    text: ''
                },
                legend: {
                    display: false
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true,
                            display: false,
                            fontColor: '#ff9472'
                        },
                        gridLines: {
                            display: false
                        }
                    }],
                    xAxes: [{
                        ticks: {
                            display: false,
                            fontColor: 'green'
                        },
                        gridLines: {
                            display: false
                        }
                    }]
                }
            }
        });


        new Chart(document.getElementById("shipments_docs"), {
            type: 'line',
            data: {
                labels: [
                    @foreach($dashboard as $data)
                        '{{$data->created_at->format('h:i')}}',
                    @endforeach

                ],
                datasets: [{
                    data: [
                        @foreach($dashboard as $data)
                        {{$data->shipments_docs}},
                        @endforeach
                    ],
                    label: "Receipt",
                    //borderColor: "#3e95cd",

                    @if($dashboard->last()->shipments_docs >= 80)
                    borderColor: "#cd3e5d",
                    @else
                    borderColor: "#3e95cd",
                    @endif

                    fill: true,
                    borderWidth: 1,
                    backgroundColor: "rgba(110, 110, 245, 0.1)",
                    pointRadius: 0
                }
                ]
            },
            options: {
                title: {
                    display: false,
                    text: ''
                },
                legend: {
                    display: false
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true,
                            display: false,
                            fontColor: '#ff9472'
                        },
                        gridLines: {
                            display: false
                        }
                    }],
                    xAxes: [{
                        ticks: {
                            display: false,
                            fontColor: 'green'
                        },
                        gridLines: {
                            display: false
                        }
                    }]
                }
            }
        });

        //Doughnut

        new Chart(document.getElementById("savannah_bin"), {
            type: 'doughnut',
            data: {
                labels: [ {{$binuseage->savannah_used_bins}} + " Used Bins", {{$binuseage->savannah_Total_Bins - $binuseage->savannah_used_bins}} + " Empty Bins"],
                datasets: [
                    {
                        label: "Savannah Bin Usage",

                        @if($binuseage->savannah_per >= 85)
                        backgroundColor: ["#cd3e5d", "rgba(110, 110, 245, 0.1)"],
                        @else
                        backgroundColor: ["#3e95cd", "rgba(110, 110, 245, 0.1)"],
                        @endif

                        data: [{{$binuseage->savannah_per}},{{100-$binuseage->savannah_per}}]
                    }
                ]
            },
            options: {
                title: {
                    display: true,
                    text: 'Savannah',
                    fontColor: 'white',
                    fontSize: 20
                },
                legend: {
                    display: true,
                    position: 'left',
                    labels: {
                        fontColor: 'white',
                    }
                }
            }
        });

        new Chart(document.getElementById("tifton_bin"), {
            type: 'doughnut',
            data: {
                labels: [ {{$binuseage->tifton_used_bins}} + " Used Bins", {{$binuseage->tifton_Total_Bins - $binuseage->tifton_used_bins}} + " Empty Bins"],
                datasets: [
                    {
                        label: "Savannah Bin Usage",

                        @if($binuseage->tifton_per >= 85)
                        backgroundColor: ["#cd3e5d", "rgba(110, 110, 245, 0.1)"],
                        @else
                        backgroundColor: ["#3e95cd", "rgba(110, 110, 245, 0.1)"],
                        @endif

                        data: [{{$binuseage->tifton_per}},{{100-$binuseage->tifton_per}}]
                    }
                ]
            },
            options: {
                title: {
                    display: true,
                    text: 'Tifton',
                    fontColor: 'white',
                    fontSize: 20
                },
                legend: {
                    display: true,
                    position: 'left',
                    labels: {
                        fontColor: 'white',
                    }
                }
            }
        });

        new Chart(document.getElementById("clay_bin"), {
            type: 'doughnut',
            data: {
                labels: [ {{$binuseage->Clay_used_bins}} + " Used Bins", {{$binuseage->Clay_Total_Bins - $binuseage->Clay_used_bins}} + " Empty Bins"],
                datasets: [
                    {
                        label: "Savannah Bin Usage",

                        @if($binuseage->Clay_per >= 85)
                        backgroundColor: ["#cd3e5d", "rgba(110, 110, 245, 0.1)"],
                        @else
                        backgroundColor: ["#3e95cd", "rgba(110, 110, 245, 0.1)"],
                        @endif

                        data: [{{$binuseage->Clay_per}},{{100-$binuseage->Clay_per}}]
                    }
                ]
            },
            options: {
                title: {
                    display: true,
                    text: 'Clay Rd',
                    fontColor: 'white',
                    fontSize: 20
                },
                legend: {
                    display: true,
                    position: 'left',
                    labels: {
                        fontColor: 'white',
                    }
                }
            }
        });

        new Chart(document.getElementById("valdosta_bin"), {
            type: 'doughnut',
            data: {
                labels: [ {{$binuseage->valdosta_used_bins}} + " Used Bins", {{$binuseage->valdosta_Total_Bins - $binuseage->valdosta_used_bins}} + " Empty Bins"],
                datasets: [
                    {
                        label: "Savannah Bin Usage",

                        @if($binuseage->valdosta_per >= 85)
                        backgroundColor: ["#cd3e5d", "rgba(110, 110, 245, 0.1)"],
                        @else
                        backgroundColor: ["#3e95cd", "rgba(110, 110, 245, 0.1)"],
                        @endif

                        data: [{{$binuseage->valdosta_per}},{{100-$binuseage->valdosta_per}}]
                    }
                ]
            },
            options: {
                title: {
                    display: true,
                    text: 'Valdosta',
                    fontColor: 'white',
                    fontSize: 20
                },
                legend: {
                    display: true,
                    position: 'left',
                    labels: {
                        fontColor: 'white',
                    }
                }
            }
        });


    </script>

@endsection
