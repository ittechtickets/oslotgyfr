<div class="modal fade" id="mooseSupport" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content text-white bg-dark">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Moose Support</h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <img src="{{URL::asset('img/Cat-icon.png')}}" class="" style="display: block; margin-left: auto; margin-right: auto;">

                <p>Greetings Humon!</p>
                <p style="text-align:justify">My name is Moose! When I am all grown up I will be your Support Solution Specialist.
                    I will help you find what you are looking for but until then a support ticket is the way to go! You can either send an email to ittechtickets@outsourcelogistics.com or
                <a href="https://outsourcelogistics.on.spiceworks.com/portal/tickets" target="_blank">click here!</a> </p>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
