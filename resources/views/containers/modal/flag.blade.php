<!-- Modal -->
<div class="modal fade" id="flagforreview" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form method="post" action="/containers/flag">
                @csrf
                <input type="hidden" name="created_by" value="{{Auth::user()->id}}">
                <input type="hidden" name="container_id" value="{{$container->id}}">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Flag For Review</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1"><b>Note Required</b></label>
                        <textarea class="form-control" name="container_notes" aria-describedby="emailHelp" placeholder="Enter Note REQUIRED" required></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
                    <input  type="submit" class="btn btn-sm btn-primary" value="Submit Form">
                </div>
            </form>
        </div>
    </div>
</div>
