{{--New file Template--}}

{{--Add Security for this page below--}}


@extends('layouts.app')
{{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')

    @if( Auth::user()->hasRole('Container') == FALSE)
        @include('layouts.unauthorized')

    @Else

        <meta http-equiv="refresh" content="300">

        <div class="row">
            <div class="col-md-4">
                <b>Containers Dashboard</b>
            </div>
            <div class="col-md-8">
                <form method="post" action="/containers/search">
                    @csrf
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text"><i class="fad fa-search"></i></div>
                        </div>
                        <input type="text" class="form-control" id="inlineFormInputGroupUsername" name="search_bar" placeholder="Search...">
                        <div class="btn-group">
                            <input type="submit" class="btn btn-dark" value="Search">
                            <a href="/containers/all" class="btn  btn-dark">All Containers</a>
                            <a href="/containers/add" class="btn  btn-dark">Add Container</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="card-deck mt-3">
            <div class="card" title="Containers that have been reported shipped but are still on Vessels.">
                <div class="card-header">
                    <b>On Vessels</b>
                    <a href="/containers/on_vessels" class="btn-sm btn btn-outline-dark float-right"><i class="fad fa-info-square" title="Detail"></i></a>
                </div>
                <div class="card-body p-1 text-right" style="font-size: 50px; font-weight: bolder">
                    {{$containers->where('on_vessel','Yes')->count()}}
                </div>
            </div>
            <div class="card" title="All containers that do not have a IN GATE date.">
                <div class="card-header">
                    <b>W/O IN GATE</b>
                </div>
                <div class="card-body p-1 text-right" style="font-size: 50px; font-weight: bolder">
                    {{$containers->wherenull('returned_date')->count()}}
                </div>
            </div>
            <div class="card" title="Containers that have been returned to the port.">
                <div class="card-header">
                    <b>Closed</b>
                </div>
                <div class="card-body p-1 text-right" style="font-size: 50px; font-weight: bolder">
                    {{$containers->wherenotnull('returned_date')->count()}}
                </div>
            </div>
            <div class="card" title="Containers within 2 days of LFD">
                <div class="card-header">
                    <b>ALFD</b>
                </div>
                <div class="card-body p-1 text-right" style="font-size: 50px; font-weight: bolder">
                    {{$lfd->count()}}
                </div>
            </div>
            <div class="card" title="Containers that have NOT been entered into Camelot.  No camelot transaction number, and 2 days past drop date.">
                <div class="card-header">
                    <b>No Camelot</b>
                    <a href="/containers/noCamelot" class="btn-sm btn btn-outline-dark float-right"><i class="fad fa-info-square" title="Detail"></i></a>
                </div>
                <div class="card-body p-1 text-right" style="font-size: 50px; font-weight: bolder">
                    {{$noCamelot->count()}}
                </div>
            </div>
            <div class="card" title="These containers have been flagged for review by a user and need attention.">
                <div class="card-header">
                    <b>Flagged</b>
                    <a href="/containers/noCamelot" class="btn-sm btn btn-outline-dark float-right disabled"><i class="fad fa-info-square" title="Detail"></i></a>
                </div>
                <div class="card-body p-1 text-right" style="font-size: 50px; font-weight: bolder">
                    {{$containers->where('container_status','Flagged')->count()}}
                </div>
            </div>
            <div class="card @if(!$duplicates->isEmpty()) bg-danger text-white @endif" title="Containers that have the SAME container number.">
                <div class="card-header">
                    <b>Duplicates</b>
                    @if(!$duplicates->isEmpty())<a href="/containers/dupes" class="btn-sm btn btn-outline-light float-right disabled"><i class="fad fa-exclamation-triangle" style="--fa-secondary-color: gold;"></i></a>@endif
                </div>
                <div class="card-body p-1 text-right" style="font-size: 50px; font-weight: bolder">
                    @if($duplicates->isEmpty())
                        0
                    @else
                    {{$duplicates->first()->count}}
                    @endif
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <div class="card mt-3" title="Containers broken down by customer for all time.">
                    <div class="card-header">
                        <b>Containers by Customer</b>
                    </div>
                    <div class="card-body">
                        <canvas id="bar-chart-horizontal" height="65"></canvas>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="card mt-3" title="Kroger transactions with and without CH Robinson billing info.">
                    <div class="card-header">
                        <b>CH Robinson Accounts</b>
                    </div>
                    <div class="card-body">
                        <canvas id="pie-chart" height="65"></canvas>
                    </div>
                </div>
            </div>
        </div>

        <div class="row mt-3">
            <div class="col-sm-6">
                <div class="card">
                    <div class="card-body">
                        <ul class="list-group">
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                Containers with no vessel ETA date
                                <span class="badge badge-primary badge-pill">{{$containers->wherenull('estimated_arrival')->count()}}</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                Containers with no available at port date
                                <span class="badge badge-primary badge-pill">{{$containers->wherenull('container_ava')->count()}}</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                Containers with no port LFD date
                                <span class="badge badge-primary badge-pill">{{$containers->wherenull('estimated_lfd')->count()}}</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                Containers with no out gate date
                                <span class="badge badge-primary badge-pill">{{$containers->wherenull('pickup_date')->count()}}</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                Containers with no yard arrival date
                                <span class="badge badge-primary badge-pill">{{$containers->wherenull('drop_date')->count()}}</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                Containers with no container FTE date
                                <span class="badge badge-primary badge-pill">{{$containers->wherenull('container_fte')->count()}}</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="card">
                    <div class="card-body">
                        <ul class="list-group">
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                Containers with no container to door date
                                <span class="badge badge-primary badge-pill">{{$containers->wherenull('container_to_door')->count()}}</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                Containers with no Ppwk scan date
                                <span class="badge badge-primary badge-pill">{{$containers->wherenull('scan_date')->count()}}</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                Containers with no empty notify date
                                <span class="badge badge-primary badge-pill">{{$containers->wherenull('empty_date')->count()}}</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                Containers with no in gate date
                                <span class="badge badge-primary badge-pill">{{$containers->wherenull('returned_date')->count()}}</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                Containers with no in camelot date
                                <span class="badge badge-primary badge-pill">{{$containers->wherenull('date_camelot')->count()}}</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>


    @endif

@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="text/javascript">

        $(document).ready( function () {
            $('#clients').DataTable();
        } );

        //all customers chart
        new Chart(document.getElementById("bar-chart-horizontal"), {
            type: 'horizontalBar',
            data: {
                labels: [
                    @foreach($customer as $data)
                    "{{$data->customer}}",
                    @endforeach
                ],
                datasets: [
                    {
                        //label: "Population (millions)",
                        backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
                        data: [

                            @foreach($customer as $data)
                            {{$data->count}},
                            @endforeach

                        ]
                    }
                ]
            },
            options: {
                legend: { display: false },
                title: {
                    display: false,
                    text: 'Predicted world population (millions) in 2050'
                }
            }
        });


        //CH acocunting paperwork

        new Chart(document.getElementById("pie-chart"), {
            type: 'pie',
            data: {
                labels: [
                    "Transactions WITHOUT Account Info","Transactions With Account Info"
                ],
                datasets: [{
                    label: "Population (millions)",
                    backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
                    data: [
                        {{$chContainers->wherenotnull('load_no')->count()}},
                        {{$chContainers->wherenull('load_no')->count()}}
                    ]
                }]
            },
            options: {
                title: {
                    display: false,
                    text: 'Predicted world population (millions) in 2050'
                },
                legend: {
                    display: true,
                    position: 'left',
                    labels: {
                        fontColor: 'rgb(255, 99, 132)'
                    }
                }
            }
        });

    </script>

@endsection
