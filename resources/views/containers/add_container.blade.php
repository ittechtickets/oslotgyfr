{{--New file Template--}}

{{--Add Security for this page below--}}


@extends('layouts.app')
{{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')

    @if( Auth::user()->hasRole('Container') == FALSE)
        @include('layouts.unauthorized')

    @Else


        <div class="card mb-4">
            <div class="card-header" style="background-color: #91bcff;">
                <b>Add Container</b>
                <a href="/containers/all" class="btn btn-sm btn-secondary float-right ">Back to All Containers</a>
            </div>
            <div class="card-body">
                <form method="post" action="/containers/store">
                    @csrf
                    <input type="hidden" name="created_by" value="{{Auth::user()->id}}">
                    <div class="form-row">
                        <div class="col-md-2 mb-3">
                            <label for="validationDefault01"><b>Customer</b></label>
                            <select class="custom-select mr-sm-2 is-invalid" id="inlineFormCustomSelect" name="customer" required>
                                <option selected value="">[Select Customer]</option>
                                <option value="Kronos">Kronos</option>
                                <option value="Kroger MFG">Kroger MFG</option>
                                <option value="Kroger Seasonal">Kroger Seasonal</option>
                                <option value="Schneider">Schneider</option>
                            </select>
                        </div>
                        <div class="col-md-2 mb-3">
                            <label for="validationDefault02"><b>Container #</b></label>
                            <input type="text" class="form-control  is-invalid" id="validationDefault02" name="container_number" placeholder="Container #" required>
                        </div>
                        <div class="col-md-2 mb-3">
                            <label for="validationDefault01"><b>Container on Vessel?</b></label>
                            <select class="custom-select mr-sm-2 is-invalid" id="inlineFormCustomSelect" name="on_vessel" required>
                                <option selected value="">[Select]</option>
                                <option value="Yes">Yes</option>
                                <option value="No">No</option>
                            </select>
                        </div>
                        <div class="col-md-2 mb-3">
                            <label for="validationDefault02"><b>BL #</b></label>
                            <input type="text" class="form-control" id="validationDefault02" name="bl_number" placeholder="BL #" >
                        </div>
                        <div class="col-md-2 mb-3">
                            <label for="validationDefault02"><b>DN #</b></label>
                            <input type="text" class="form-control" id="validationDefault02" name="dn_number" placeholder="DN #" >
                        </div>
                        <div class="col-md-2 mb-3">
                            <label for="validationDefault02"><b>CH Robinson #</b></label>
                            <input type="text" class="form-control" id="validationDefault02" name="ch_number" placeholder="CH Robinson" >
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-2 mb-3">
                            <label for="validationDefault02" title="When is the vessel estimated to be in the port?"><b>Vessel ETA</b></label>
                            <input type="date" class="form-control" id="validationDefault02" name="estimated_arrival" >
                        </div>
                        <div class="col-md-2 mb-3">
                            <label for="validationDefault02" title="When is the container available to be picked up from the port?"><b>Available at Port</b></label>
                            <input type="date" class="form-control" id="validationDefault02"  name="container_ava">
                        </div>
                        <div class="col-md-2 mb-3">
                            <label for="validationDefault02" title="When is the estimated last free day for this container?"><b>Port LFD</b></label>
                            <input type="date" class="form-control" id="validationDefault02" name="estimated_lfd" >
                        </div>
                        <div class="col-md-2 mb-3">
                            <label for="validationDefault02" title="When was this container picked up from the port."><b>Out Gate</b></label>
                            <input type="date" class="form-control" id="validationDefault02"   name="pickup_date">
                        </div>
                        <div class="col-md-2 mb-3">
                            <label for="validationDefault02" title="When did this container get to the yard?"><b>Yard Arrival</b></label>
                            <input type="date" class="form-control" id="validationDefault02" name="drop_date" >
                        </div>
                        <div class="col-md-2 mb-3">
                            <label for="validationDefault02" title="When does this containers free time expire?"><b>Container FTE</b></label>
                            <input type="date" class="form-control" id="validationDefault02"   name="container_fte" >
                        </div>
                        <div class="col-md-2 mb-3">
                            <label for="validationDefault02" title="When was this container pulled to the dock door?"><b>Container to Door</b></label>
                            <input type="date" class="form-control" id="validationDefault02"   name="container_to_door" >
                        </div>
                        <div class="col-md-2 mb-3">
                            <label for="validationDefault02" title="When was this containers paperwork scanned to the CSR?"><b>Ppwk Scan</b></label>
                            <input type="date" class="form-control" id="validationDefault02"  name="scan_date" >
                        </div>
                        <div class="col-md-2 mb-3">
                            <label for="validationDefault02" title="When were you notified that this container was empty?"><b>Empty Notify</b></label>
                            <input type="date" class="form-control" id="validationDefault02"   name="empty_date" >
                        </div>
                        <div class="col-md-2 mb-3">
                            <label for="validationDefault02" title="When was this container returned to the port?"><b>In Gate</b></label>
                            <input type="date" class="form-control" id="validationDefault02"  name="returned_date" >
                        </div>
                        <div class="col-md-2 mb-3">
                            <label for="validationDefault02"><b>In Camelot Date</b></label>
                            <input type="date" class="form-control" id="validationDefault02"  name="date_camelot" >
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-2 mb-3">
                            <label for="validationDefault02"><b>Camelot Number</b></label>
                            <input type="text" class="form-control" id="validationDefault02" placeholder="Camelot Receipt Number"  name="camelot_number">
                        </div>
                        <div class="col-md-2 mb-3">
                            <label for="validationDefault02"><b>Trans Inv Number</b></label>
                            <input type="text" class="form-control" id="validationDefault02" placeholder="Trans Inv Number"  name="transportation_inv_number">
                        </div>
                        <div class="col-md-2 mb-3">
                            <label for="validationDefault02"><b>Drop Location</b></label>
                            <input type="text" class="form-control" id="validationDefault02" placeholder="Drop Location"  name="drop_location">
                        </div>
                        <div class="col-md-2 mb-3">
                            <label for="validationDefault02"><b>Commodity / Desc</b></label>
                            <input type="text" class="form-control" id="validationDefault02" name="desc_01" placeholder="Commodity / Desc" >
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="1" id="invalidCheck2" name="add_another">
                            <label class="form-check-label" for="invalidCheck2">
                                Keep adding containers
                            </label>
                        </div>
                    </div>
                    <button class="btn btn-primary" type="submit">Add Container</button>
                </form>
            </div>
        </div>

        <div class="card mb-5">
            <div class="card-header" style="background-color: #91bcff;">
                <b>Add Multiple Containers</b>
            </div>
            <div class="card-body">
                <form method="post" action="/containers/bulk/add">
                    @csrf
                    <input type="hidden" name="created_by" value="{{Auth::user()->id}}">
                    <div class="form-row">
                        <div class="col-md-6 mb-3">
                            <label for="validationDefault01"><b>Select customer.  ALL CONTAINERS WILL BE ASSIGNED THIS CUSTOMER</b></label>
                            <select class="custom-select mr-sm-2 is-invalid" id="inlineFormCustomSelect" name="bulk_customer" required>
                                <option selected value="">[Select Customer]</option>
                                <option value="Kronos">Kronos</option>
                                <option value="Kroger MFG">Kroger MFG</option>
                                <option value="Kroger Seasonal">Kroger Seasonal</option>
                                <option value="Schneider">Schneider</option>
                            </select>
                        </div>
                        <div class="col-md-3 mb-3">
                            <label for="validationDefault01"><b>Are ALL these containers still on vessels?</b></label>
                            <select class="custom-select mr-sm-2  is-invalid" id="inlineFormCustomSelect" name="bulk_vessels" required>
                                <option selected value="No">No</option>
                                <option value="Yes">Yes</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-row card-deck">
                        <div class="card-body" >
                            <label for="validationDefault01"><b>Paste Container numbers, one per line</b></label>
                            <textarea class="form-control mt-2 is-invalid" rows="10" name="container_data" onKeyUp="countLines1(this)" placeholder="Must have the same number of rows!" required></textarea>
                            <input type=text class="form-control mt-2" name="lineCount1" size="2" value="0" disabled>
                        </div>
                        <div class="card-body" >
                            <label for="validationDefault01"><b>BL Nmuber, one per line.</b></label>
                            <textarea class="form-control mt-2" rows="10" name="field_one_data" onKeyUp="countLines2(this)" placeholder="Must have the same number of rows!"></textarea>
                            <input type=text class="form-control mt-2" name="lineCount2" size="2" value="0" disabled>
                        </div>
                        <div class="card-body" >
                            <label for="validationDefault01"><b>DN Number, one per line.</b></label>
                            <textarea class="form-control mt-2" rows="10"  name="field_two_data"  onKeyUp="countLines3(this)" placeholder="Must have the same number of rows!"></textarea>
                            <input type=text class="form-control mt-2" name="lineCount3" size="2" value="0" disabled>
                        </div>
                        <div class="card-body" >
                            <label for="validationDefault01"><b>CH Robinson Number, one per line.</b></label>
                            <textarea class="form-control mt-2" rows="10"  name="field_three_data" onKeyUp="countLines4(this)" placeholder="Must have the same number of rows!"></textarea>
                            <input type=text class="form-control mt-2" name="lineCount4" size="2" value="0" disabled>
                        </div>
                        <div class="card-body" >
                            <label for="validationDefault01"><b>Commodity / DESC</b></label>
                            <textarea class="form-control mt-2" rows="10"  name="field_four_data" onKeyUp="countLines5(this)" placeholder="Must have the same number of rows!"></textarea>
                            <input type=text class="form-control mt-2" name="lineCount5" size="2" value="0" disabled>
                        </div>
                    </div>
                        <button class="btn btn-primary" type="submit">Add All Containers</button>
                </form>
            </div>
        </div>
    @endif

@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="text/javascript">

        $(document).ready( function () {
            $('#clients').DataTable();
        } );


        function countLines1(theArea){
            var theLines = theArea.value.replace((new RegExp(".{"+theArea.cols+"}","g")),"\n").split("\n");
            if(theLines[theLines.length-1]=="") theLines.length--;
            theArea.form.lineCount1.value = theLines.length;
        }

        function countLines2(theArea){
            var theLines = theArea.value.replace((new RegExp(".{"+theArea.cols+"}","g")),"\n").split("\n");
            if(theLines[theLines.length-1]=="") theLines.length--;
            theArea.form.lineCount2.value = theLines.length;
        }

        function countLines3(theArea){
            var theLines = theArea.value.replace((new RegExp(".{"+theArea.cols+"}","g")),"\n").split("\n");
            if(theLines[theLines.length-1]=="") theLines.length--;
            theArea.form.lineCount3.value = theLines.length;
        }

        function countLines4(theArea){
            var theLines = theArea.value.replace((new RegExp(".{"+theArea.cols+"}","g")),"\n").split("\n");
            if(theLines[theLines.length-1]=="") theLines.length--;
            theArea.form.lineCount4.value = theLines.length;
        }

        function countLines5(theArea){
            var theLines = theArea.value.replace((new RegExp(".{"+theArea.cols+"}","g")),"\n").split("\n");
            if(theLines[theLines.length-1]=="") theLines.length--;
            theArea.form.lineCount5.value = theLines.length;
        }

    </script>

@endsection
