{{--New file Template--}}

{{--Add Security for this page below--}}


@extends('layouts.app')
{{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')

    @if( Auth::user()->hasRole('Container') == FALSE)
        @include('layouts.unauthorized')

    @Else

        <div class="card mb-4">
            <div class="card-header" style="background-color: #91bcff;">
                <div class="row">
                    <div class="col-sm-9">
                        <span style="font-size:25px;font-weight: bold">Containers NOT in Camelot</span>
                    </div>
                    <div class="col-sm-3">
                        <form method="post" action="/containers/search">
                            @csrf
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text"><i class="fad fa-search"></i></div>
                                </div>
                                <input type="text" class="form-control" id="inlineFormInputGroupUsername" name="search_bar" placeholder="Search...">
                                <input type="submit" class="btn btn-dark" value="Search">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-sm table-hover" id="containers">
                    <thead>
                    <tr>
                        <td><b>Customer</b></td>
                        <td><b>Container</b></td>
                        <td><b>BL #</b></td>
                        <td><b>DN #</b></td>
                        <td><b>Ch Rob #</b></td>
                        <td><b>Ship ETA</b></td>
                        <td><b>LFD</b></td>
                        <td><b>Out Gate</b></td>
                        <td><b>Drop</b></td>
                        <td><b>Empty</b></td>
                        <td><b>In Gate</b></td>
                        <td align="center"><b>Camelot</b></td>
                        <td align="right"></td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($containers as $container)
                        <tr
                            @if(!is_null($container->estimated_lfd) AND \Carbon\Carbon::now()->diffInDays($container->estimated_lfd) < 2)
                            class="bg-danger text-white"
                            @elseif($container->container_status == 'Flagged')
                            class="bg-warning text-white"
                            @endif
                        >
                            <td>{{$container->customer}}</td>
                            <td>{{$container->container_number}}</td>

                            <td>{{$container->bl_number}}</td>
                            <td>{{$container->dn_number}}</td>
                            <td>{{$container->ch_number}}</td>
                            <td>@if(is_null($container->estimated_arrival))-- @else {{$container->estimated_arrival->format('m-d-Y')}} @endif</td>
                            <td>

                                @if(is_null($container->estimated_lfd))-- @else {{$container->estimated_lfd->format('m-d-Y')}} ({{\Carbon\Carbon::now()->diffInDays($container->estimated_lfd)}}) @endif
                                @if(!is_null($container->estimated_lfd) AND \Carbon\Carbon::now()->diffInDays($container->estimated_lfd) < 2) <i class="fad fa-exclamation-triangle" style="--fa-secondary-color: gold;"></i> @endif


                            </td>
                            <td>@if(is_null($container->pickup_date))-- @else {{$container->pickup_date->format('m-d-Y')}} @endif</td>
                            <td>@if(is_null($container->drop_date))-- @else {{$container->drop_date->format('m-d-Y')}} @endif</td>
                            <td>@if(is_null($container->empty_date))-- @else {{$container->empty_date->format('m-d-Y')}} @endif</td>
                            <td>@if(is_null($container->returned_date))-- @else {{$container->returned_date->format('m-d-Y')}} @endif</td>
                            <td align="center">{{$container->camelot_number}}</td>
                            <td align="right"><a href="/containers/{{$container->id}}" target="_blank" class="btn btn-sm btn-info"><i class="fad fa-truck-moving" style="--fa-secondary-color: black;" title="Update"></i></a> </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @endif

@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="text/javascript">

        $(document).ready( function () {
            $('#containers').DataTable();
        } );

    </script>

@endsection
