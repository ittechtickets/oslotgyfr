{{--New file Template--}}

{{--Add Security for this page below--}}


@extends('layouts.app')
{{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')

    @if( Auth::user()->hasRole('Container') == FALSE)
        @include('layouts.unauthorized')

    @Else

        <div class="card mb-4">
            <div class="card-header" style="background-color: #91bcff;">
                <div class="row">
                    <div class="col-sm-6">
                        <span style="font-size:25px;font-weight: bold">Containers On Vessels</span>
                    </div>
                    <div class="col-sm-6">
                        <form method="post" action="/containers/search">
                            @csrf
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <a href="#" class="input-group-text btn btn-primary btn-sm" data-toggle="modal" data-target="#exampleModalCenter"><i class="fad fa-copy"></i></a>
                                    <div class="input-group-text"><i class="fad fa-search"></i></div>
                                </div>
                                <input type="text" class="form-control" id="inlineFormInputGroupUsername" name="search_bar" placeholder="Search...">
                                <div class="btn-group">
                                    <input type="submit" class="btn btn-dark" value="Search">
                                    <a href="/containers/all" class="btn btn-secondary float-right ">Back to All Containers</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="card-body">
                @if($containers->isEmpty())
                    <div class="alert alert-info">There are no containers on vessels.</div>
                @else
                <table class="table table-sm table-hover">
                    <thead>
                    <tr>
                        <td><b>Customer</b></td>
                        <td><b>Container</b></td>
                        <td><b>Ship ETA</b></td>

                        <td align="center"><b>on Vessel</b></td>

                        <td><b>BL #</b></td>
                        <td><b>DN #</b></td>
                        <td><b>LFD</b></td>
                        <td><b>Pickup</b></td>
                        <td><b>Drop</b></td>
                        <td><b>Empty</b></td>
                        <td align="center"><b>Camelot</b></td>
                        <td align="right"><a href="/containers/add" class="btn btn-sm btn-secondary float-right ">Add Container</a></td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($containers as $container)
                        <tr
                                @if(!is_null($container->estimated_lfd) AND \Carbon\Carbon::now()->diffInDays($container->estimated_lfd) < 2)
                                class="bg-danger text-white"
                                @endif
                        >
                            <td>{{$container->customer}}</td>
                            <td>{{$container->container_number}}</td>
                            <td>@if(is_null($container->estimated_arrival))-- @else {{$container->estimated_arrival->format('m-d-Y')}} @endif</td>
                            <td align="center">@if($container->on_vessel == 'Yes')<i class="fad fa-ship" style="--fa-primary-color: blue;" title="Container still on vessel"></i> @ENDIF</td>
                            <td>{{$container->bl_number}}</td>
                            <td>{{$container->dn_number}}</td>
                            <td>

                                @if(is_null($container->estimated_lfd))-- @else {{$container->estimated_lfd->format('m-d-Y')}} ({{\Carbon\Carbon::now()->diffInDays($container->estimated_lfd)}}) @endif
                                @if(!is_null($container->estimated_lfd) AND \Carbon\Carbon::now()->diffInDays($container->estimated_lfd) < 2) <i class="fad fa-exclamation-triangle" style="--fa-secondary-color: gold;"></i> @endif


                            </td>
                            <td>@if(is_null($container->pickup_date))-- @else {{$container->pickup_date->format('m-d-Y')}} @endif</td>
                            <td>@if(is_null($container->drop_date))-- @else {{$container->drop_date->format('m-d-Y')}} @endif</td>
                            <td>@if(is_null($container->empty_date))-- @else {{$container->empty_date->format('m-d-Y')}} @endif</td>
                            <td align="center">@if(is_null($container->camelot_number))<i class="fad fa-times-square" style="color:red" title="No Camelot Number"></i> @else <i class="fad fa-check-square" style="color: green"></i> @endif</td>
                            <td align="right"><a href="/containers/{{$container->id}}" class="btn btn-sm btn-info"><i class="fad fa-truck-container" title="Update"></i></a> </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                    @endif
            </div>
        </div>
    @endif


    <!-- Modal -->
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Copy Containers on Vessels</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <textarea class="form-control" rows="10">@foreach($containers as $containernumbers){{$containernumbers->container_number}}&#013;&#010;@endforeach</textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>



@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="text/javascript">

        $(document).ready( function () {
            $('#clients').DataTable();
        } );


    </script>

@endsection
