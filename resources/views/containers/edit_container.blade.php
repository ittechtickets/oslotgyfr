{{--New file Template--}}

{{--Add Security for this page below--}}


@extends('layouts.app')
{{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')

    @if( Auth::user()->hasRole('Container') == FALSE)
        @include('layouts.unauthorized')

    @Else


        <div class="card mb-4 border-dark">
            <div class="card-header" style="background-color: #91bcff;">
                <div class="row">
                    <div class="col-sm-4">
                        <span ><b>Modify Container</b> - {{$container->container_number}}</span> {{$container->container_status}}
                    </div>
                    <div class="col-sm-8">
                        <form method="post" action="/containers/search">
                            @csrf
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    @if($container->container_status == 'Flagged')
                                        <a href="#" class="btn btn-success float-right disabled" data-toggle="modal" data-target="#flagforreview">Clear Container Flag</a>
                                    @else
                                        <a href="#" class="btn btn-warning float-right " data-toggle="modal" data-target="#flagforreview">Flag Container for Review</a>
                                        @endif


                                    <div class="input-group-text"><i class="fad fa-search"></i></div>
                                </div>
                                <input type="text" class="form-control" id="inlineFormInputGroupUsername" name="search_bar" placeholder="Search...">
                                <div class="btn-group">
                                    <input type="submit" class="btn btn-dark" value="Search">
                                    <a href="/containers/all" class="btn btn-secondary float-right ">Back to All Containers</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <form method="post" action="/containers/update">
                    <input type="hidden" value="{{$container->id}}" name="id">
                    @csrf
                    <input type="hidden" name="created_by" value="{{$container->created_by}}">
                    <div class="form-row">
                        <div class="col-md-2 mb-3">
                            <label for="validationDefault01"><b>Customer</b></label>
                            <select class="custom-select mr-sm-2" id="inlineFormCustomSelect" name="customer" required>
                                <option selected value="{{$container->customer}}">{{$container->customer}}</option>
                                <option value="Kronos">Kronos</option>
                            </select>
                        </div>
                        <div class="col-md-2 mb-3">
                            <label for="validationDefault02"><b>Container #</b></label>
                            <input type="text" class="form-control" id="validationDefault02" name="container_number" placeholder="Container #" value="{{$container->container_number}}" required>
                        </div>
                        <div class="col-md-2 mb-3">
                            <label for="validationDefault01"><b>Container on Vessel?</b></label>
                            <select class="custom-select mr-sm-2" id="inlineFormCustomSelect" name="on_vessel" required>
                                <option selected value="{{$container->on_vessel}}">{{$container->on_vessel}}</option>
                                <option value="Yes">Yes</option>
                                <option value="No">No</option>
                            </select>
                        </div>
                        <div class="col-md-2 mb-3">
                            <label for="validationDefault02"><b>BL #</b></label>
                            <input type="text" class="form-control" id="validationDefault02" name="bl_number" placeholder="BL #" value="{{$container->bl_number}}">
                        </div>
                        <div class="col-md-2 mb-3">
                            <label for="validationDefault02"><b>DN #</b></label>
                            <input type="text" class="form-control" id="validationDefault02" name="dn_number" placeholder="DN #" value="{{$container->dn_number}}">
                        </div>
                        <div class="col-md-2 mb-3">
                            <label for="validationDefault02"><b>CH Robinson #</b></label>
                            <input type="text" class="form-control" id="validationDefault02" name="ch_number" placeholder="CH #" value="{{$container->ch_number}}">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-2 mb-3">
                            <label for="validationDefault02" title="When is the vessel estimated to be in the port?"><b>Vessel ETA</b></label>
                            <input type="date" class="form-control @if(is_null($container->estimated_arrival)) is-invalid @else is-valid @endif" id="validationDefault02" name="estimated_arrival" @if(!is_null($container->estimated_arrival))value="{{$container->estimated_arrival->format('yy-m-d')}}" @endif>
                        </div>
                        <div class="col-md-2 mb-3">
                            <label for="validationDefault02" title="When is the container available to be picked up from the port?"><b>Available at Port</b></label>
                            <input type="date" class="form-control @if(is_null($container->container_ava)) is-invalid @else is-valid @endif" id="validationDefault02" @if(!is_null($container->container_ava)) value="{{$container->container_ava->format('yy-m-d')}}" @endif name="container_ava">
                        </div>
                        <div class="col-md-2 mb-3">
                            <label for="validationDefault02" title="When is the estimated last free day for this container?"><b>Port LFD</b></label>
                            <input type="date" class="form-control @if(is_null($container->estimated_lfd)) is-invalid @else is-valid @endif" id="validationDefault02" name="estimated_lfd" @if(!is_null($container->estimated_lfd))value="{{$container->estimated_lfd->format('yy-m-d')}}" @endif>
                        </div>
                        <div class="col-md-2 mb-3">
                            <label for="validationDefault02" title="When was this container picked up from the port."><b>Out Gate</b></label>
                            <input type="date" class="form-control @if(is_null($container->pickup_date)) is-invalid @else is-valid @endif" id="validationDefault02"  @if(!is_null($container->pickup_date))value="{{$container->pickup_date->format('yy-m-d')}}" @endif name="pickup_date">
                        </div>
                        <div class="col-md-2 mb-3">
                            <label for="validationDefault02" title="When did this container get to the yard?"><b>Yard Arrival</b></label>
                            <input type="date" class="form-control @if(is_null($container->drop_date)) is-invalid @else is-valid @endif" id="validationDefault02" name="drop_date" @if(!is_null($container->drop_date))value="{{$container->drop_date->format('yy-m-d')}}" @endif>
                        </div>
                        <div class="col-md-2 mb-3">
                            <label for="validationDefault02" title="When does this containers free time expire?"><b>Container FTE</b></label>
                            <input type="date" class="form-control @if(is_null($container->container_fte)) is-invalid @else is-valid @endif" id="validationDefault02"   name="container_fte" @if(!is_null($container->container_fte))value="{{$container->container_fte->format('yy-m-d')}}" @endif>
                        </div>
                        <div class="col-md-2 mb-3">
                            <label for="validationDefault02" title="When was this container pulled to the dock door?"><b>Container to Door</b></label>
                            <input type="date" class="form-control @if(is_null($container->container_to_door)) is-invalid @else is-valid @endif" id="validationDefault02"   name="container_to_door" @if(!is_null($container->container_to_door))value="{{$container->container_to_door->format('yy-m-d')}}" @endif>
                        </div>
                        <div class="col-md-2 mb-3">
                            <label for="validationDefault02" title="When was this containers paperwork scanned to the CSR?"><b>Ppwk Scan</b></label>
                            <input type="date" class="form-control @if(is_null($container->scan_date)) is-invalid @else is-valid @endif" id="validationDefault02"  name="scan_date" @if(!is_null($container->scan_date))value="{{$container->scan_date->format('yy-m-d')}}" @endif>
                        </div>
                        <div class="col-md-2 mb-3">
                            <label for="validationDefault02" title="When were you notified that this container was empty?"><b>Empty Notify</b></label>
                            <input type="date" class="form-control @if(is_null($container->empty_date)) is-invalid @else is-valid @endif" id="validationDefault02"   name="empty_date" @if(!is_null($container->empty_date))value="{{$container->empty_date->format('yy-m-d')}}" @endif>
                        </div>
                        <div class="col-md-2 mb-3">
                            <label for="validationDefault02" title="When was this container returned to the port?"><b>In Gate</b></label>
                            <input type="date" class="form-control @if(is_null($container->returned_date)) is-invalid @else is-valid @endif" id="validationDefault02"  name="returned_date" @if(!is_null($container->returned_date))value="{{$container->returned_date->format('yy-m-d')}}" @endif>
                        </div>
                        <div class="col-md-2 mb-3">
                            <label for="validationDefault02"><b>In Camelot Date</b></label>
                            <input type="date" class="form-control" id="validationDefault02"  name="date_camelot" @if(!is_null($container->date_camelot))value="{{$container->date_camelot->format('yy-m-d')}}" @endif>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-2 mb-3">
                            <label for="validationDefault02"><b>Camelot Number</b></label>
                            <input type="text" class="form-control" id="validationDefault02" placeholder="Camelot Receipt Number"  name="camelot_number" value="{{$container->camelot_number}}">
                        </div>
                        <div class="col-md-2 mb-3">
                            <label for="validationDefault02"><b>Trans Inv Number</b></label>
                            <input type="text" class="form-control" id="validationDefault02" placeholder="Trans Inv Number"  name="transportation_inv_number" value="{{$container->transportation_inv_number}}">
                        </div>
                        <div class="col-md-2 mb-3">
                            <label for="validationDefault02"><b>Drop Location</b></label>
                            <input type="text" class="form-control" id="validationDefault02" placeholder="Drop Location" value="{{$container->drop_location}}" name="drop_location">
                        </div>
                        <div class="col-md-2 mb-3">
                            <label for="validationDefault02"><b>Commodity / Desc</b></label>
                            <input type="text" class="form-control" id="validationDefault02" placeholder="Commodity / Desc" value="{{$container->desc_01}}" name="desc_01">
                        </div>
                    </div>
                    <div class="btn-group">
                        <button class="btn btn-primary" type="submit">Update Container</button>
                    </div>
                    <div class="btn-group float-right">
                        @if ( Auth::user()->hasRole('Container Admin'))
                            <a href="/containers/delete/{{$container->id}}" class="btn btn-danger">Delete Container</a>
                        @endif
                    </div>

                </form>
            </div>
        </div>

        @if(!is_null($container->ch_number))
            <div class="card mt-3 border-dark">
                <div class="card-header" style="background-color: #91bcff;">
                    <span style="font-size:25px;font-weight: bold">CH Robinson Data</span>
                </div>
                <div class="card-body">
                    @if($container->chImport->isEmpty())
                        <div class="alert alert-info">There is no CH Robinson Accounts Receivable info for this Load.</div>
                    @else
                    <table class="table table-sm table-hover">
                        <thead>
                            <tr>
                                <td><b>Status</b></td>
                                <td><b>Load</b></td>
                                <td><b>Invoice</b></td>
                                <td><b>Pro</b></td>
                                <td><b>Trailer</b></td>
                                <td><b>Ship Date</b></td>
                                <td><b>Est Payment</b></td>
                                <td><b>Freight Rate</b></td>
                                <td><b>Adv</b></td>
                                <td><b>Adj</b></td>
                                <td><b>Bal</b></td>
                                <td><b>Total</b></td>
                                <td><b>Check#</b></td>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($container->chImport as $chInfo)
                            <tr>
                                <td>{{$chInfo->status}}</td>
                                <td>{{$chInfo->load_no}}</td>
                                <td>{{$chInfo->invoice}}</td>
                                <td>{{$chInfo->pro}}</td>
                                <td>{{$chInfo->trailer}}</td>
                                <td>@if(!is_null($chInfo->ship_date)) {{$chInfo->ship_date->format('m-d-Y')}} @endif</td>
                                <td>@if(!is_null($chInfo->est_payment_date)) {{$chInfo->est_payment_date->format('m-d-Y')}} @endif</td>
                                <td>${{$chInfo->freight_rate}}</td>
                                <td>${{$chInfo->advanced}}</td>
                                <td>${{$chInfo->adjustments}}</td>
                                <td>${{$chInfo->balance}}</td>
                                <td>${{$chInfo->total_paid}}</td>
                                <td>{{$chInfo->check_no}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                        @endif
                </div>
            </div>
        @endif

        <div class="card mt-3 mb-5 border-dark">
            <div class="card-header" style="background-color: #91bcff;">
                <div class="row">
                    <div class="col-sm-3">
                        <span style="font-size:25px;font-weight: bold">Notes</span>
                    </div>
                    <div class="col-sm-9">
                        <form method="post" action="/containers/notes/add">
                            @csrf
                            <input type="hidden" name="container_id" value="{{$container->id}}">
                            <input type="hidden" name="created_by" value="{{ Auth::user()->id}}">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text"><i class="fad fa-sticky-note"></i></div>
                                </div>
                                <input type="text" class="form-control" id="inlineFormInputGroupUsername" name="container_notes" placeholder="Add Note...">
                                <input type="submit" class="btn btn-dark" value="Add Note">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="card-body">

                @if($container->NotesContainer->isEmpty())
                    <div class="alert alert-info">No notes for this container</div>
                @else
                <table class="table table-sm">
                    <thead>
                        <tr>
                            <td><b>Note</b></td>
                            <td><b>Entered By</b></td>
                            <td><b>Date</b></td>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($container->NotesContainer as $notes)
                        <tr>
                            <td>{{$notes->container_notes}}</td>
                            <td>{{$notes->createdBY->first_name}} {{$notes->createdBY->last_name}}</td>
                            <td>{{$notes->created_at}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                @endif
            </div>
        </div>
    @endif


    @include('containers.modal.flag')
@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="text/javascript">

        $(document).ready( function () {
            $('#clients').DataTable();
        } );

    </script>

@endsection
